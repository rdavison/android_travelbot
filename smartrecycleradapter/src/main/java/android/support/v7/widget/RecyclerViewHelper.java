package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;

/**
 * Created by richard on 06/11/14.
 */
public class RecyclerViewHelper {
    public static int convertPreLayoutPositionToPostLayout(RecyclerView recyclerView, int position) {
        return recyclerView.mRecycler.convertPreLayoutPositionToPostLayout(position);
    }
}