package se.davison.smartrecycleradapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by richard on 11/11/14.
 */
public class TwoLineAdapterItem extends AdapterItem<TwoLineAdapterItem.ViewHolder> {

    private String textOne;
    private String textTwo;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtOne;
        TextView txtTwo;


        public ViewHolder(View itemView) {
            super(itemView);
            txtOne = (TextView) itemView.findViewById(android.R.id.text1);
            txtTwo = (TextView) itemView.findViewById(android.R.id.text2);
        }
    }

    public TwoLineAdapterItem(String textOne, String textTwo) {
        this.textOne = textOne;
        this.textTwo = textTwo;
    }


    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new TwoLineAdapterItem.ViewHolder(inflater.inflate(R.layout.two_line_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.txtOne.setText(textOne);
        viewHolder.txtTwo.setText(textTwo);
    }

}
