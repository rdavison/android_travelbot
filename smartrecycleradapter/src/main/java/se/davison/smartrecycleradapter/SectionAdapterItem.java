package se.davison.smartrecycleradapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by richard on 10/11/14.
 */
public class SectionAdapterItem extends AdapterItem<SectionAdapterItem.ViewHolder> {

    private String text;
    private boolean dividerVisible = false;

    private boolean autoDividers = true;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView titel;
        ImageView divider;

        public ViewHolder(View itemView) {
            super(itemView);
            titel = (TextView) itemView.findViewById(android.R.id.title);
            divider = (ImageView) itemView.findViewById(android.R.id.icon);
        }
    }

    public void setAutoDividers(boolean autoDividers) {
        this.autoDividers = autoDividers;
    }

    public void setDividerVisible(boolean dividerVisible) {
        this.dividerVisible = dividerVisible;
    }

    public boolean isDividerVisible() {
        return dividerVisible;
    }

    @Override
    public boolean isPinned() {
        return true;
    }

    @Override
    public boolean isHeader() {
        return true;
    }

    @Override
    public int headerId() {
        return text.hashCode();
    }

    public SectionAdapterItem(String text) {
        this.text = text;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.top_header, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.titel.setText(text);

        boolean visible = false;
        if(autoDividers && position>0){
            visible = true;
        }else if(dividerVisible){
            visible = true;
        }else{
            visible = false;
        }

        viewHolder.divider.setVisibility(visible?View.VISIBLE:View.GONE);
    }

}
