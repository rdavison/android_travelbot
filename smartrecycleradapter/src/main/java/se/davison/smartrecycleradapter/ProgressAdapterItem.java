package se.davison.smartrecycleradapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by richard on 18/11/14.
 */
public class ProgressAdapterItem extends AdapterItem<ProgressAdapterItem.ViewHolder> {

    private final int[] margin;
    private String text;
    private int minHeight;
    private Context context;
    private Integer textColor;
    private boolean loading = false;
    private ViewHolder holder;
    private boolean enabled;

    public ProgressAdapterItem(Context context, String text, Integer textColor) {
        this(context, text, textColor, 64, null);
    }

    public ProgressAdapterItem(Context context) {
        this(context, null, null, 64, null);
    }

    public ProgressAdapterItem(Context context, String text, Integer textColor, int minHeight, int[] margin) {

        if (margin != null && margin.length != 4) {
            throw new IllegalArgumentException("Margin must be an array of 4 values: left,top,right,bottom");
        }else if(margin == null){
            margin = new int[]{0,0,0,0};
        }

        this.text = text;
        this.minHeight = dpToPx(context, minHeight);
        this.context = context;
        this.textColor = textColor;
        this.margin = margin;

    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }


    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {

        ProgressBar progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyle);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(ProgressBar.GONE);
        progressBar.setId(android.R.id.progress);

        TextView textView = new TextView(context);
        textView.setId(android.R.id.text1);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        if (textColor != null) {
            textView.setTextColor(textColor);
        }

        RelativeLayout.LayoutParams centerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        centerParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        RelativeLayout ll = new RelativeLayout(context);
        ll.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ll.setMinimumHeight(minHeight);
        ll.addView(textView, centerParams);
        ll.addView(progressBar, centerParams);


        int[] attrs = new int[]{android.R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int backgroundResource = typedArray.getResourceId(0, 0);
        ll.setBackgroundResource(backgroundResource);
        typedArray.recycle();

        return new ViewHolder(ll);
    }

    public void setLoading(boolean value) {
        loading = value;
        if (holder != null) {
            if (value) {
                holder.progressBar.setVisibility(ProgressBar.VISIBLE);
                holder.textView.setVisibility(TextView.GONE);
            } else {
                holder.progressBar.setVisibility(ProgressBar.GONE);
                holder.textView.setVisibility(TextView.VISIBLE);
            }
        }
    }

    public boolean isLoading() {
        return loading;
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        holder = viewHolder;
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.itemView.getLayoutParams();
        params.setMargins(margin[0], margin[1], margin[2], margin[3]);

        holder.textView.setText(text);
        setLoading(loading);
    }

    private int dpToPx(Context c, float dp) {
        return (int) (c.getResources().getDisplayMetrics().density * dp);
    }

    public AdapterItem loading() {
        this.loading = true;
        return this;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(android.R.id.text1);
            progressBar = (ProgressBar) itemView.findViewById(android.R.id.progress);
        }
    }
}
