package se.davison.smartrecycleradapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

/**
 * Created by richard on 13/11/14.
 */
public class SectionItemDecoration extends RecyclerView.ItemDecoration {


    private static final int[] ATTRS = new int[]{
            android.R.attr.listDivider
    };

    public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;
    public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;
    private static final String TAG = SectionItemDecoration.class.getSimpleName();
    private Integer separatorColor;
    private int dividerHeight;
    private final RecyclerView.AdapterDataObserver adapterDataObserver;
    private SectionAdapter adapter;
    private RecyclerView recyclerView;

    private Drawable headerShadowDrawable;
    private int shadowHeight;
    private int separatorPaddingLeft;
    private int separatorPaddingRight;
    private Paint separatorPaint = new Paint(Paint.ANTI_ALIAS_FLAG);


    public void setSeparator(Integer separatorColor, int separatorPaddingLeft, int separatorPaddingRight) {
        this.separatorPaddingLeft = separatorPaddingLeft;
        this.separatorPaddingRight = separatorPaddingRight;
        this.separatorColor = separatorColor;
        this.separatorPaint.setColor(separatorColor);
    }

    public SectionItemDecoration(Context context, SectionAdapter adapter, RecyclerView recyclerView) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        float density = context.getResources().getDisplayMetrics().density;
        dividerHeight = Math.round(density);
        a.recycle();
        this.adapterDataObserver = new AdapterDataObserver();
        this.adapter = adapter;
        this.recyclerView = recyclerView;

        headerShadowDrawable =  ContextCompat.getDrawable(context, R.drawable.separator_shadow);
        shadowHeight = (int)density*2;

        //paddingRight = recyclerView.getPaddingRight();
        //paddingLeft = recyclerView.getPaddingLeft();
    }

    private int getOrientation(RecyclerView parent) {
        LinearLayoutManager layoutManager;
        try {
            layoutManager = (LinearLayoutManager) parent.getLayoutManager();
        } catch (ClassCastException e) {
            throw new IllegalStateException("DividerDecoration can only be used with a " +
                    "LinearLayoutManager.", e);
        }
        return layoutManager.getOrientation();
    }

    private RecyclerView.ViewHolder headerHolder = null;
    private Integer headerIndex = null;
    private int headerHeight = -1;

    public static void log(String message, Integer value){
        Log.d(TAG, message + (value == null ? "" : " = " + value));
    }

    public static void log(String message, Float value){
        Log.d(TAG, message + (value == null ? "" : " = " + value));
    }

    /*public static void log(String message){
       log(message, null);
    }*/

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        long time = System.currentTimeMillis();


        int childCount = parent.getChildCount();
        if(childCount>adapter.getItemCount()){
            childCount = adapter.getItemCount();
        }
        final RecyclerView.LayoutManager lm = parent.getLayoutManager();

        float headerTranslateY = 0;
        float paddingTranslation = 0;

        int headerChildIndex = -1;
        float headerTranslation = -1;

        boolean itemsChanged = adapter.hasItemsChanged();


        for (int i = childCount - 1; i >= 0; i--) {
            View child = parent.getChildAt(i);
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) child.getLayoutParams();
            RecyclerView.ViewHolder holder = parent.getChildViewHolder(child);
            int holderPosition = holder.getAdapterPosition();
            if(!lp.isItemRemoved() && holderPosition!=-1){
                AdapterItem item = adapter.getItem(holderPosition);

                if(item == null){
                    return;
                }

                boolean isHeader = item!=null&&item.isHeader();

                if(separatorColor!=null && !isHeader && !item.isLast()){
                    drawVertical(c, parent, child);
                }

                if(isHeader && headerIndex!=null && holderPosition!=headerIndex){
                    headerChildIndex = i;
                    float padding = ViewCompat.getTranslationY(child);
                    headerTranslation = getHeaderY(child, lm) + padding;
                }


                if(i==0){

                    //for clipToPadding
                    if(holderPosition==0&&isHeader) {
                        float padTransY = ViewCompat.getTranslationY(child);
                        paddingTranslation = getHeaderY(child, lm) + padTransY;
                    }

                    int sectionPos = adapter.getSectionPosition(holderPosition);



                    if(sectionPos == -1 || itemsChanged){
                        headerHolder = null;
                        headerIndex = null;
                    }

                    //added item.isPinned
                    if(sectionPos!=-1&&adapter.getItem(sectionPos).isPinned()&&(headerIndex==null||headerIndex!=sectionPos)){
                        //holder.
                        headerIndex=sectionPos;

                        int viewType = adapter.getItemViewType(sectionPos);
                        //log("view type")

                        if(!itemsChanged) {
                            headerHolder = recyclerView.getRecycledViewPool().getRecycledView(viewType);
                        }
                        if (headerHolder == null) {
                            adapter.setItemsChanged(false);
                            headerHolder = adapter.onCreateViewHolder(parent, viewType);
                        }
                        layoutHeader(headerHolder.itemView);
                        headerHeight = headerHolder.itemView.getMeasuredHeight();
                        adapter.onBindViewHolder(headerHolder, sectionPos);

                        headerChildIndex = 0;
                        headerTranslation = 0;
                    }

                    if(headerChildIndex>-1){
                        if((int)headerTranslation<=headerHeight){
                            headerTranslateY = headerTranslation-headerHeight;
                        }
                    }

                    //log("paddingTranslation",paddingTranslation);

                    if (headerHolder != null && paddingTranslation<=shadowHeight) {
                        c.save();
                        //Log.d(TAG, "headerTranslateY=" + headerTranslateY);


                        headerShadowDrawable.setBounds(0, headerHeight, recyclerView.getWidth(), headerHeight + (int) (paddingTranslation + headerTranslateY + shadowHeight));
                        headerShadowDrawable.draw(c);
                        c.translate(parent.getPaddingLeft(), paddingTranslation+headerTranslateY);
                        headerHolder.itemView.draw(c);
                        c.restore();
                    }
                }
            }
        }
    }

    private void layoutHeader(View header) {
        int widthSpec = android.view.View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth() - recyclerView.getPaddingRight() - recyclerView.getPaddingLeft(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        header.measure(widthSpec, heightSpec);
        header.layout(0, 0, header.getMeasuredWidth(), header.getMeasuredHeight());
    }





    public void drawVertical(Canvas c, RecyclerView parent, View child) {
        final int left = parent.getPaddingLeft()+ separatorPaddingLeft;
        final int right = parent.getWidth()-separatorPaddingRight-parent.getPaddingRight();
        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                .getLayoutParams();
        final int top = child.getBottom() + params.bottomMargin;
        final int bottom = top + dividerHeight;
        //final int bottom = top + mDivider.getIntrinsicHeight();
        c.drawRect(left, top, right, bottom, separatorPaint);
    }

    public void drawHorizontal(Canvas c, RecyclerView parent, View child) {
        final int top = parent.getPaddingTop()+ separatorPaddingLeft;
        final int bottom = parent.getHeight() - parent.getPaddingBottom();
        final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                .getLayoutParams();
        final int left = child.getRight() + params.rightMargin;
        final int right = left + dividerHeight;



        //final int right = left + mDivider.getIntrinsicHeight();
        c.drawRect(left,top,right,bottom,separatorPaint);
    }



    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
//        if (getOrientation(parent) == VERTICAL_LIST) {
//            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
//        } else {
//            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
//        }
    }

    public void registerAdapterDataObserver(SectionAdapter adapter) {
        adapter.registerAdapterDataObserver(adapterDataObserver);
    }

    private float getHeaderY(View item, RecyclerView.LayoutManager lm) {
        return  lm.getDecoratedTop(item) < 0 ? 0 : lm.getDecoratedTop(item);
    }


    private class AdapterDataObserver extends RecyclerView.AdapterDataObserver {

        public AdapterDataObserver() {
        }

        @Override
        public void onChanged() {

        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            //headerStore.onItemRangeRemoved(positionStart, itemCount);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            //headerStore.onItemRangeInserted(positionStart, itemCount);
        }
    }
}
