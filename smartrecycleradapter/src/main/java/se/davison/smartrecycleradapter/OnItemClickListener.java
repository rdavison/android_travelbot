package se.davison.smartrecycleradapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by richard on 21/10/15.
 */
public interface OnItemClickListener {
    void onItemClick(ViewGroup viewGroup, RecyclerView.ViewHolder viewHolder, View view, int position);
}
