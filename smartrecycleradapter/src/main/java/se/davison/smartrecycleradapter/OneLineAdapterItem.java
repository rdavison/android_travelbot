package se.davison.smartrecycleradapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by richard on 11/11/14.
 */
public class OneLineAdapterItem extends AdapterItem<OneLineAdapterItem.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;

        public ViewHolder(TextView itemView) {
            super(itemView);
            textView = itemView;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new OneLineAdapterItem.ViewHolder((TextView)inflater.inflate(R.layout.one_line_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.textView.setText(text);
        if(textColor!=null){
            viewHolder.textView.setTextColor(textColor);
        }

        if(textSize!=null){
            viewHolder.textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        }
    }

    public String getText() {
        return text;
    }

    private String text;
    private Integer textColor;
    private Integer textSize;

    public OneLineAdapterItem(String text, Integer textColor, Integer textSize) {
        this.text = text;
        this.textColor = textColor;
        this.textSize = textSize;
    }

    public OneLineAdapterItem(String text) {
        this.text = text;
    }
}
