package se.davison.smartrecycleradapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by richard on 10/11/14.
 */
public class SectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final String TAG = SectionAdapter.class.getSimpleName();
    private final LayoutInflater inflater;
    private SparseIntArray positionSection;
    private ArrayList<AdapterItem> items = new ArrayList<AdapterItem>();
    private List<Class<? extends AdapterItem>> itemTypes =  new ArrayList<Class<? extends AdapterItem>>(20);
    private List<AdapterItem> viewHolderTypes = new ArrayList<AdapterItem>(20);
    private boolean stableIdsSet = false;
    private OnItemClickListener itemClickListener;
    private SectionItemDecoration decoration;
    private Integer separatorColor = null;
    private int separatorPaddingLeft = 0;
    private boolean itemsChanged;
    private int separatorPaddingRight = 0;


    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public SectionAdapter(Context context, ArrayList<AdapterItem> items) {
        this.inflater = LayoutInflater.from(context);
        this.items = items;
        initList(items);
    }

    public SectionAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.items = new ArrayList<AdapterItem>();
        initList(items);
    }




    @Override
    public int getItemViewType(int position) {
        return itemTypes.indexOf(getItem(position).getClass());
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    private void initList(ArrayList<AdapterItem> items) {
        positionSection = new SparseIntArray(items.size());
        int count = 0;
        boolean setFirst = false;
        int sectionIndex = -1;
        for (AdapterItem item : items) {
            Class<? extends AdapterItem> currentClass = item.getClass();
            if (item.isHeader()) {
                setFirst = true;
                sectionIndex = count;
                if(count>0){
                    items.get(count-1).setLast(true);
                }
            }else if(setFirst){
                setFirst = false;
                item.setFirst(true);
            }
            if (!itemTypes.contains(currentClass)) {
                itemTypes.add(currentClass);
                viewHolderTypes.add(item);
            }
            positionSection.put(count, sectionIndex);
            if(count==items.size()-1){
                item.setLast(true);
            }
            count++;
        }
        itemsChanged = true;
        if(!stableIdsSet) {
            stableIdsSet = true;
            setHasStableIds(true);
        }
    }

    public int clearSection(int sectionIndex, boolean includeHeader){

        AdapterItem item = items.get(sectionIndex);
        if(!item.isHeader()){
            throw new IllegalArgumentException(item.getClass().getSimpleName()+ " @ " +sectionIndex+" is not a section!");
        }

        int size = items.size();
        int lastIndex = -1;
        for(int i = sectionIndex; i < size; i++){
            int sectionPos = positionSection.get(i);
            if(sectionPos!=sectionIndex){
                lastIndex = sectionPos;
                break;
            }
        }
        if(lastIndex==-1){
            lastIndex = size-1;
        }
        int itemsRemoved = 0;
        int removeIndex = sectionIndex+(includeHeader?0:1);
        for(int i = removeIndex; i < lastIndex+1;i++){
            items.remove(removeIndex);
            itemsRemoved++;
        }
        return itemsRemoved;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public synchronized void setItems(ArrayList<AdapterItem> items) {
        this.items = items;
        initList(items);
    }

    public ArrayList<AdapterItem> getItems() {
        return items;
    }

    public synchronized AdapterItem getItem(int position) {
        if(position<items.size() && position!=-1) {
            return items.get(position);
        }
        return null;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        if(viewType==-1){
            return null;
        }
        final AdapterItem adapterItem = viewHolderTypes.get(viewType);
        final RecyclerView.ViewHolder viewHolder = adapterItem.onCreateViewHolder(inflater, viewGroup);

        if(itemClickListener!=null&&adapterItem.isEnabled()){
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    itemClickListener.onItemClick(viewGroup,viewHolder,view,viewHolder.getAdapterPosition());
                }
            });
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        AdapterItem item = getItem(position);
        item.onBindViewHolder(viewHolder, position);
    }

    public void attachTo(RecyclerView recyclerView) {
        recyclerView.setAdapter(this);
        decoration = new SectionItemDecoration(recyclerView.getContext(), this, recyclerView);
        decoration.registerAdapterDataObserver(this);
        toggleSeparator();
        recyclerView.addItemDecoration(decoration);
    }

    private void toggleSeparator() {
        if(decoration!=null&&separatorColor!=null){
            decoration.setSeparator(separatorColor, separatorPaddingLeft,separatorPaddingRight);
        }
    }

    public void setSeparator(Integer color){
        separatorColor = color;
        toggleSeparator();
    }

    public void setSeparator(Integer color, int paddingLeft, int paddingRight){
        separatorColor = color;
        separatorPaddingLeft = paddingLeft;
        separatorPaddingRight = paddingRight;
        toggleSeparator();
    }



    public long getHeaderId(int itemPosition) {
        return 0;
    }

    public int getSectionPosition(int position) {
        return positionSection.get(position);
    }


    public boolean hasItemsChanged() {
        return itemsChanged;
    }

    public void setItemsChanged(boolean itemsChanged) {
        this.itemsChanged = itemsChanged;
    }
}
