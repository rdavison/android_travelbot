package se.davison.smartrecycleradapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by richard on 10/11/14.
 */
public abstract class AdapterItem<VH extends RecyclerView.ViewHolder> {

    private boolean last = false;
    public void setLast(boolean last) {
        this.last = last;
    }

    public int[] getMargin(){
        return new int[]{0,0,0,0};
    }


    private boolean first = false;

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public boolean isLast() {
        return last;
    }

    public boolean isHeader(){
        return false;
    }

    public boolean isPinned(){
        return false;
    }

    public int headerId(){
        return -1;
    }

    public abstract VH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent);

    public abstract void onBindViewHolder(VH viewHolder, int position);

    public boolean isEnabled(){return true; }

}
