package se.davison.travelbot;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import se.davison.travelbot.common.WearDeparture;


/**
 * Created by richard on 03/10/15.
 */
public class DepartureFragment extends Fragment implements View.OnApplyWindowInsetsListener {


    private static final String EXTRA_DEPARTURES = "extra_departures";
    private static final String EXTRA_TRACK = "extra_track";
    private static final String EXTRA_NAME = "extra_name";
    private int horizontalMargin;
    private WearableListView wearableListView;
    private DepartureAdapter adapter;
    private String strDeparturesFrom;

    private final List<DepartureAdapter.Action> actions = new ArrayList<>(10);


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        wearableListView = new WearableListView(getActivity());
        wearableListView.setGreedyTouchMode(true);


        wearableListView.setOnApplyWindowInsetsListener(this);

        wearableListView.setLayoutManager(new LinearLayoutManager(getActivity()));


        return wearableListView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        //strDeparturesFrom = getString(R.string.departures_from);

        actions.add(new DepartureAdapter.Action("refresh", getString(R.string.refresh), R.drawable.ic_refresh_24dp));
        //actions.add(new DepartureAdapter.Action(getString(R.string.open),R.drawable.ic_refresh));
    }

    @Override
    public WindowInsets onApplyWindowInsets(View view, WindowInsets insets) {

        horizontalMargin = getResources().getDimensionPixelSize(insets.isRound() ? R.dimen.activity_margin_round : R.dimen.activity_margin_square);

        view.setPadding(0, horizontalMargin*2, 0, horizontalMargin*2);

        Bundle args = getArguments();

        List<WearDeparture> departures = (ArrayList<WearDeparture>) args.getSerializable(EXTRA_DEPARTURES);
        String track = args.getString(EXTRA_TRACK);
        String name = args.getString(EXTRA_NAME);

        adapter = new DepartureAdapter(getActivity(), horizontalMargin, name + " " + track, departures, actions);
        adapter.setActionClickListener(new DepartureAdapter.ActionClickListener() {
            @Override
            public void onAction(String id) {
                ((MainActivity) getActivity()).refreshData();
            }
        });

        wearableListView.setAdapter(adapter);


        return insets;
    }

    public static DepartureFragment newInstance(String name, String track, List<WearDeparture> wearDepartures) {

        DepartureFragment fragment = new DepartureFragment();

        Bundle args = new Bundle(3);
        args.putString(EXTRA_NAME, name);
        args.putString(EXTRA_TRACK, track);
        args.putSerializable(EXTRA_DEPARTURES, (Serializable) wearDepartures);
        fragment.setArguments(args);

        return fragment;
    }

    public void setDepartures(List<WearDeparture>  departures) {

        adapter.setDepartures(departures);

    }
}
