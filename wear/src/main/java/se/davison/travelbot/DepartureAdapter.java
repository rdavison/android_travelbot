package se.davison.travelbot;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import se.davison.travelbot.common.WearDeparture;


/**
 * Created by richard on 05/10/15.
 */
public class DepartureAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final String strNow;

    public void setDepartures(List<WearDeparture> departures) {
        this.departures = departures;
        notifyDataSetChanged();
    }

    public interface ActionClickListener {
        public void onAction(String id);
    }



    private static final int VIEW_TYPE_DEPARTURE = 0;
    private static final int VIEW_TYPE_TITLE = 1;
    private static final int VIEW_TYPE_ACTION = 2;
    private final String title;

    private ActionClickListener actionClickListener;


    private List<Action> actions;
    private List<WearDeparture> departures;

    private LayoutInflater inflater;

    private int horizontalMargin = 0;

    public DepartureAdapter(Context context, int horizontalMargin, String title,List<WearDeparture> departures, List<Action> actions){
        inflater = LayoutInflater.from(context);

        strNow = context.getString(R.string.now);
        this.horizontalMargin = horizontalMargin;
        this.title = title;
        this.actions = actions;
        this.departures = departures;
    }

    public static class Action{

        public String text;
        public int iconResource;
        public String id;

        public Action(String id,String text, int iconResource) {
            this.id = id;
            this.text = text;
            this.iconResource = iconResource;
        }
    }

    public void setActionClickListener(ActionClickListener listener){
        this.actionClickListener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        switch (viewType){
            case VIEW_TYPE_ACTION:
                final RecyclerView.ViewHolder holder = new ActionViewHolder(inflater.inflate(R.layout.list_action,parent,false));
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Action action = actions.get(holder.getAdapterPosition()-1-departures.size());
                        actionClickListener.onAction(action.id);
                    }
                });
                return holder;
            case VIEW_TYPE_TITLE:
                return new TitleViewHolder(inflater.inflate(R.layout.list_title,parent,false));
            default:
                return new DepartureViewHolder(inflater.inflate(R.layout.list_departure,parent,false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        holder.itemView.setPadding(horizontalMargin,0,horizontalMargin,0);


        if(holder instanceof DepartureViewHolder){
            DepartureViewHolder departureViewHolder = (DepartureViewHolder)holder;

            WearDeparture departure = departures.get(position-1);

            departureViewHolder.txtDirection.setText(departure.direction);
            if(departure.via==null){
                departureViewHolder.txtDirectionSub.setVisibility(View.GONE);
            }else{
                departureViewHolder.txtDirectionSub.setVisibility(View.VISIBLE);
                departureViewHolder.txtDirectionSub.setText(departure.via);
            }

            departureViewHolder.txtLine.setText(departure.line);
            departureViewHolder.txtLine.setBackgroundColor(departure.bgColor);
            departureViewHolder.txtLine.setTextColor(departure.fgColor);
            //departureViewHolder.txtTrack.setText(departure.track);
            departureViewHolder.txtNextTrip.setText(departure.getNextTripAsString(strNow));
            departureViewHolder.txtThereafterTrip.setText(departure.getThereAfterAsString(strNow));


        }else if(holder instanceof ActionViewHolder){
            ActionViewHolder actionViewHolder = (ActionViewHolder)holder;

            Action action = actions.get(position-1-departures.size());
            actionViewHolder.text.setText(action.text);
            actionViewHolder.text.setCompoundDrawablesRelativeWithIntrinsicBounds(action.iconResource,0,0,0);



        }else {

            TitleViewHolder titleViewHolder = (TitleViewHolder)holder;
            titleViewHolder.title.setText(title);
        }

    }

    @Override
    public int getItemViewType(int position) {
        int departureCount = departures.size();
        if(position==0){
            return VIEW_TYPE_TITLE;
        }

        if(position<departureCount+1){
            return VIEW_TYPE_DEPARTURE;
        }

        return VIEW_TYPE_ACTION;
    }

    @Override
    public int getItemCount() {
        return 1+departures.size()+actions.size();
    }

    public static class DepartureViewHolder extends RecyclerView.ViewHolder {

        TextView txtLine;
        TextView txtDirection;
        //TextView txtTrack;
        TextView txtNextTrip;
        TextView txtThereafterTrip;
        TextView txtDirectionSub;

        public DepartureViewHolder(View convertView) {
            super(convertView);
            this.txtLine = (TextView) convertView.findViewById(R.id.txt_line);
            this.txtDirection = (TextView) convertView.findViewById(R.id.txt_direction);
            //this.txtTrack = (TextView) convertView.findViewById(R.id.txt_track);
            this.txtDirectionSub = (TextView) convertView.findViewById(R.id.txt_direction_sub);
            this.txtNextTrip = (TextView) convertView.findViewById(R.id.txt_next_trip);
            this.txtThereafterTrip = (TextView) convertView.findViewById(R.id.txt_thereafter_trip);


        }
    }

    public static class ActionViewHolder extends RecyclerView.ViewHolder {

        TextView text;


        public ActionViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(android.R.id.text1);

            int padding = (int) (text.getResources().getDisplayMetrics().density*16);

            text.setCompoundDrawablePadding(padding);
        }
    }

    public static class TitleViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public TitleViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(android.R.id.title);
        }
    }




}
