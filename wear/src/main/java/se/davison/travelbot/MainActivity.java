package se.davison.travelbot;

import android.app.Activity;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.Pair;
import android.support.wearable.view.CircularButton;
import android.support.wearable.view.DotsPageIndicator;
import android.support.wearable.view.GridViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import se.davison.travelbot.common.WearDeparture;


public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, MessageApi.MessageListener, DataApi.DataListener,LocationListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String PATH_DEPARTURES = "/departures";
    private static final String KEY_DEPARTURES = "se.davison.travelbot.departures";

    private static final int ERROR_SEND_MESSAGE = 0;
    private static final int ERROR_GET_DEPARTURES = 1;


    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private GridViewPager pager;
    private GridViewPagerFragmentAdapter adapter;
    private Set<String> lastKeys;
    private ProgressBar progressbar;
    private TextView txtMessage;
    private CircularButton btnRetry;
    private DotsPageIndicator indicator;
    private String lastStopId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //Connect the GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        pager = (GridViewPager) findViewById(R.id.grid_view_pager);
        progressbar = (ProgressBar)findViewById(R.id.prg_main);
        txtMessage = (TextView)findViewById(R.id.txt_message);
        btnRetry = (CircularButton)findViewById(R.id.btn_retry);
        indicator = (DotsPageIndicator) findViewById(R.id.indicator);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessageToNodes();
            }
        });
    }


    private String pickBestNodeId(List<Node> nodes) {
        String bestNodeId = null;
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {
            if (node.isNearby()) {
                return node.getId();
            }
            bestNodeId = node.getId();
        }
        return bestNodeId;
    }




    /*
     * Resolve the node = the connected device to send the message to
     */
    private void sendMessageToNodes() {
        progressbar.setVisibility(View.VISIBLE);
        txtMessage.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
            @Override
            public void onResult(NodeApi.GetConnectedNodesResult nodes) {
                String nodeId = pickBestNodeId(nodes.getNodes());
                Wearable.MessageApi.sendMessage(
                        mGoogleApiClient, nodeId, PATH_DEPARTURES, (String.valueOf(mLastLocation.getLatitude()) + "AAA" + String.valueOf(mLastLocation.getLongitude())).getBytes(Charset.forName("UTF-8"))).setResultCallback(
                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    showErrorMessage(ERROR_SEND_MESSAGE);
                                }
                            }
                        }
                );
            }
        });
    }


    @Override
    public void onConnected(Bundle bundle) {

        Log.d(TAG,"connected");


        Wearable.MessageApi.addListener(mGoogleApiClient, this);





       /* mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);*/

        if(mLastLocation==null){
            LocationRequest locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(2000)
                    .setFastestInterval(1000);

            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, locationRequest, this).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    if (status.getStatus().isSuccess()) {
                        if (Log.isLoggable(TAG, Log.DEBUG)) {
                            Log.d(TAG, "Successfully requested location updates");
                        }
                    } else {
                        Log.e(TAG,
                                "Failed in requesting location updates, "
                                        + "status code: "
                                        + status.getStatusCode()
                                        + ", message: "
                                        + status.getStatusMessage());
                    }
                }

            });
        }

        if (mLastLocation != null) {
            Log.d(TAG,"lastLocation!=null");
            sendMessageToNodes();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        //Improve your code

        Log.d(TAG, "connection susp");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //Improve your code
        Log.d(TAG, "connection failed");
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();

    }


    @Override
    protected void onPause() {
        super.onPause();
        //Wearable.DataApi.removeListener(mGoogleApiClient, this);
        Wearable.MessageApi.removeListener(mGoogleApiClient, this);
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);
        }
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                // DataItem changed
                DataItem item = event.getDataItem();
                if (item.getUri().getPath().compareTo(PATH_DEPARTURES) == 0) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    updateDepartures(dataMap.getString(KEY_DEPARTURES));
                }
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                // DataItem deleted
            }
        }
    }

    private void updateDepartures(String json) {

        boolean adapterCreated = false;
        if(adapter==null) {
            adapter = new GridViewPagerFragmentAdapter(getFragmentManager());
            adapterCreated = true;
        }



        if(json == null){
            showErrorMessage(ERROR_GET_DEPARTURES);
        }else {

            List<WearDeparture> departures = new Gson().fromJson(json, new TypeToken<ArrayList<WearDeparture>>() {
            }.getType());

            if(departures.size()>0&&!departures.get(0).stopId.equals(lastStopId)){
                lastKeys = null;
            }

            String stopName = "";

            ArrayMap<String, TreeSet<WearDeparture>> map = new ArrayMap<>(20);
            for (WearDeparture departure : departures) {
                String track = departure.track;
                lastStopId = departure.stopId;
                stopName = departure.stopName;
                if (!map.containsKey(track)) {
                    map.put(track, new TreeSet<WearDeparture>());
                }
                map.get(track).add(departure);
            }

            List<DepartureFragment> fragments = new ArrayList<>(20);

            if (lastKeys != null) {
                for (String lastKey : lastKeys) {

                    if (!map.containsKey(lastKey)) {
                        map.put(lastKey, null);
                    }
                }
            }

            int index = 0;
            for (String key : map.keySet()) {
                if (lastKeys != null && lastKeys.contains(key)) {
                    DepartureFragment oldFragment = (DepartureFragment) adapter.getFragment(0,index);
                    oldFragment.setDepartures(new ArrayList<WearDeparture>(map.get(key)));
                    fragments.add(index, oldFragment);
                } else {
                    DepartureFragment newFragment = DepartureFragment.newInstance(stopName,key, new ArrayList<WearDeparture>(map.get(key)));
                    if(index > fragments.size()){
                        fragments.add(newFragment);
                    }
                    else{
                        fragments.add(index, newFragment);
                        index++; //shift everything one position in add
                    }

                }
                index++;
            }

            lastKeys = map.keySet();
            adapter.setFragments(fragments);

            progressbar.setVisibility(View.GONE);

            if (adapterCreated) {
                pager.setAdapter(adapter);

                indicator.setVisibility(View.VISIBLE);
                indicator.setPager(pager);
            }
        }


    }

    private void showErrorMessage(int errorCode) {

        progressbar.setVisibility(View.GONE);
        txtMessage.setVisibility(View.VISIBLE);
        btnRetry.setVisibility(View.VISIBLE);

        int textId = 0;
        int iconId = R.drawable.ic_refresh_24dp;

        switch (errorCode){
            case ERROR_GET_DEPARTURES:
                textId = R.string.err_get_departures;
                break;
            case ERROR_SEND_MESSAGE:
                textId = R.string.err_send_message;
                break;
        }

        txtMessage.setText(textId);

        Toast.makeText(this,"ERROR",Toast.LENGTH_LONG).show();
    }

    public void refreshData() {

        progressbar.setVisibility(View.VISIBLE);

        sendMessageToNodes();
    }

    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;
        sendMessageToNodes();

        LocationServices.FusedLocationApi
                .removeLocationUpdates(mGoogleApiClient, this);

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.d(TAG, "====MESSAGE RECEIVED!!!===");

        updateDepartures(new String(messageEvent.getData()));


    }
}
