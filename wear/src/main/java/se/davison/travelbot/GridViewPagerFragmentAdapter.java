package se.davison.travelbot;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.wearable.view.FragmentGridPagerAdapter;

import java.util.List;

/**
 * Created by Tomasz Konieczny on 2015-05-19.
 */
public class GridViewPagerFragmentAdapter extends FragmentGridPagerAdapter {

    private List<DepartureFragment> fragments;

    public GridViewPagerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setFragments(List<DepartureFragment> fragments) {
        this.fragments = fragments;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getFragment(int row, int column) {
        return fragments.get(column);
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount(int i) {

        return fragments==null?0:fragments.size();

    }
}
