package se.davison.travelbot.common;

import java.io.Serializable;
import java.util.Date;

public class WearDeparture implements Serializable, Comparable<WearDeparture> {

    public String stopName;
    public String stopId;
    public String line;
    public int bgColor;
    public int fgColor;
    public String direction;
    public String via;
    public String track;
    public Long nextTrip = null;
    public Long thereAfterTrip = null;


    public WearDeparture(String stopName, String stopId, String line, int bgColor, int fgColor, String direction, String via, String track, long nextTrip, long thereAfterTrip) {
        this.stopName = stopName;
        this.stopId = stopId;
        this.line = line;
        this.bgColor = bgColor;
        this.fgColor = fgColor;
        this.direction = direction;
        this.via = via;
        this.track = track;
        this.nextTrip = nextTrip;
        this.thereAfterTrip = thereAfterTrip;
    }

    public boolean hasDeparture(){
        return this.nextTrip == null && this.thereAfterTrip == null;
    }

    public WearDeparture(String stopName, String stopId){
        this.stopName = stopName;
        this.stopId = stopId;
    }

    public String getNextTripAsString(String stringNow) {
        return (nextTrip == null) ? "" : isNow(nextTrip) ? stringNow : millisToString(nextTrip);
    }

    private boolean isNow(long nextTrip) {
        return nextTrip < 60000;
    }

    public String getThereAfterAsString(String stringNow) {
        return (thereAfterTrip == null) ? "" : isNow(thereAfterTrip) ? stringNow : millisToString(thereAfterTrip);
    }

    @Override
    public int compareTo(WearDeparture another) {
        return nextTrip.compareTo(another.nextTrip);
    }

    private static String millisToString(long l) {
        long h, m;
        h = l / 3600000;
        m = (l % 3600000) / 60000;
        if (h == 0) {
            return m + "m";
        } else {
            return h + "h " + m + "m";
        }
    }
}

