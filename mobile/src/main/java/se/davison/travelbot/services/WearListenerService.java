package se.davison.travelbot.services;

import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import se.davison.travelbot.common.WearDeparture;
import se.davison.travelbot.model.TimedDeparture;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.net.LocationResponse;
import se.davison.travelbot.net.NextTripResponse;
import se.davison.travelbot.net.RestClient;

/**
 * Created by richard on 05/10/15.
 */
public class WearListenerService extends WearableListenerService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = WearListenerService.class.getSimpleName();

    public static final String PATH_DEPARTURES = "/departures";
    private static final String KEY_DEPARTURES = "se.davison.travelbot.departures";


    private GoogleApiClient mGoogleApiClient;

    public final CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        latch.countDown();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {

        if (messageEvent.getPath().equals(PATH_DEPARTURES)) {
            final String[] latLng = new String(messageEvent.getData()).split("AAA");

            List<WearDeparture> wearDepartures = null;

            try {

                LocationResponse locationResponse = RestClient.service().nearbyStops(latLng[0], latLng[1], 20).toBlocking().first();


                Location myLocation = new Location("myLocation");
                myLocation.setLatitude(Double.valueOf(latLng[0]));
                myLocation.setLongitude(Double.valueOf(latLng[1]));

                List<Stop> stopListWithoutTrack = new ArrayList<Stop>(20);

                for (Stop stop : locationResponse.getStops()) {
                    if (stop.getTrack() == null) {
                        stopListWithoutTrack.add(stop);
                    }
                }

                float shortestDistance = Float.MAX_VALUE;
                Stop nearestStop = null;

                for (Stop stop : stopListWithoutTrack) {
                    Location newLocation = new Location("newlocation");
                    newLocation.setLatitude(Double.valueOf(stop.getLatitude()));
                    newLocation.setLongitude(Double.valueOf(stop.getLongitude()));

                    float stopDistance = newLocation.distanceTo(myLocation);
                    if (stopDistance < shortestDistance) {
                        shortestDistance = stopDistance;
                        nearestStop = stop;
                    }


                }

                NextTripResponse nextTripResponse = RestClient.service().nextTrip(nearestStop.getId()).toBlocking().first();

                wearDepartures = new ArrayList<>(nextTripResponse.getDepartures().size());

                for (TimedDeparture departure : nextTripResponse.getDepartures()) {
                    wearDepartures.add(new WearDeparture(nearestStop.getName(),nearestStop.getId(),departure.getLine(), departure.getBgColor(), departure.getFgColor(), departure.getDirection(), departure.getSubDirection(), departure.getTrack(), departure.getUntilNext().getTime(), departure.getUntilThereAfter().getTime()));
                }


            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                if(latch.getCount()>0) {
                    latch.await();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            NodeApi.GetConnectedNodesResult nodesResult = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
            for(Node n : nodesResult.getNodes()) {

                MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(mGoogleApiClient, n.getId(), PATH_DEPARTURES, new Gson().toJson(wearDepartures, new TypeToken<ArrayList<WearDeparture>>() {
                }.getType()).getBytes(Charset.forName("UTF-8"))).await();


                if(result.getStatus().isSuccess()){

                }
            }

        }

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
