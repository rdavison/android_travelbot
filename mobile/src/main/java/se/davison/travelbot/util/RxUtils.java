package se.davison.travelbot.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.observers.SafeSubscriber;

/**
 * Created by admin on 2015-08-16.
 */
public class RxUtils {


    public static final Func1<String, Boolean> STRING_IS_NOT_EMPTY = new Func1<String, Boolean>() {

        @Override
        public Boolean call(String string) {
            return !string.isEmpty();
        }
    };
    public static final Func1<CharSequence,String> CHARSEQUENCE_TO_STRING = new Func1<CharSequence, String>() {
        @Override
        public String call(CharSequence charSequence) {
            return charSequence.toString();
        }
    };


    public static Observable<String> createEditTextChangeObservable(final EditText editText){
        return Observable.create(new Observable.OnSubscribe<String>() {

            @Override
            public void call(final Subscriber<? super String> subscriber) {
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (subscriber.isUnsubscribed()) return;
                        subscriber.onNext(editText.getText().toString());
                    }
                });
            }
        });
    }

    public static class OperatorCountDownLatch<T> implements Observable.Operator<T, T> {


        private final CountDownLatch signal;

        public OperatorCountDownLatch(int count) {
            signal = new CountDownLatch(count);
        }


        public void countDown(){
            signal.countDown();
            synchronized (signal) {
                signal.notifyAll();
            }

        }

        public boolean isReleased(){
            return signal.getCount()==0;
        }


        @Override
        public Subscriber<? super T> call(final Subscriber<? super T> child) {
            
            return new SafeSubscriber<>(new Subscriber<T>() {
                @Override
                public void onCompleted() {
                    
                    //signal.countDown();
                    //signal.notifyAll();
                    child.onCompleted();
                }

                @Override
                public void onError(Throwable e) {
                    child.onError(e);
                }

                @Override
                public void onNext(T t) {
                    try {
                        signal.await(5, TimeUnit.DAYS);
                        if(!child.isUnsubscribed()) {
                            child.onNext(t);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        child.onError(e);
                    }

                }
            });
        }
    }
}
