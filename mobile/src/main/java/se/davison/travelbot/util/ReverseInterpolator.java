package se.davison.travelbot.util;

import android.animation.TimeInterpolator;
import android.view.animation.Interpolator;

/**
 * Created by admin on 2015-08-13.
 */
public class ReverseInterpolator implements Interpolator {

    private final Interpolator innerInterpolator;

    private ReverseInterpolator(){

        innerInterpolator = null;
    }

    public ReverseInterpolator(Interpolator interpolator){

        this.innerInterpolator = interpolator;

    }

    @Override
    public float getInterpolation(float input) {
        return 1-innerInterpolator.getInterpolation(input);
    }
}
