package se.davison.travelbot.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.security.Key;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import se.davison.travelbot.model.Stop;

/**
 * Created by admin on 2015-08-18.
 */
public class PreferenceHelper {

    private static final String TAG = PreferenceHelper.class.getSimpleName();

    public static <V> void set(SharedPreferences preferences,String key, V value) throws Exception{
        preferences.edit().putString(key,new Gson().toJson(value,value.getClass())).apply();
    }

    public static <T> T get(Context context, String key, T defaultValue){
        return get(PreferenceManager.getDefaultSharedPreferences(context),key,defaultValue);
    }

    public static <V> V get(SharedPreferences prefs, String key, V defaultValue) {
        String value = prefs.getString(key,null);
        try {

            if(value!=null){
                Class clazz = defaultValue.getClass();
                if(Collection.class.isAssignableFrom(clazz)){
                    return (V)new Gson().fromJson(value,new ArrayListParameterizedType(clazz));
                }
                if(AbstractCollection.class.isAssignableFrom(clazz)){
                    return (V)new Gson().fromJson(value,new LinkedListParameterizedType(clazz));
                }

                return (V)new Gson().fromJson(value,clazz);
            }
            return defaultValue;
        }catch (JsonSyntaxException e){

            Log.e(TAG,"JSON conversion failed for key '"+key+"'");
            Log.e(TAG,"JSONDATA=====:::"+value);
            e.printStackTrace();
        }
        return defaultValue;
    }

    private static class LinkedListParameterizedType implements ParameterizedType {

        private Type type;

        private LinkedListParameterizedType(Type type) {
            this.type = type;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[] {type};
        }

        @Override
        public Type getRawType() {
            return LinkedList.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }

    }

    private static class ArrayListParameterizedType implements ParameterizedType {

        private Type type;

        private ArrayListParameterizedType(Type type) {
            this.type = type;
        }

        @Override
        public Type[] getActualTypeArguments() {
            return new Type[] {type};
        }

        @Override
        public Type getRawType() {
            return ArrayList.class;
        }

        @Override
        public Type getOwnerType() {
            return null;
        }

    }
}
