package se.davison.travelbot.util;

import android.view.animation.DecelerateInterpolator;

/**
 * Created by admin on 2015-08-13.
 */
public class ReverseDecelerateInterpolator extends DecelerateInterpolator {

    @Override
    public float getInterpolation(float input) {
        return 1-super.getInterpolation(input);
    }
}
