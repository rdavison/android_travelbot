package se.davison.travelbot.util;

import android.os.Bundle;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

import me.tatarka.rxloader.SaveCallback;

/**
 * Created by richard on 30/07/16.
 */
public class SerializableSaveCallback<T> implements SaveCallback<T> {
    @Override
    public void onSave(String key, T value, Bundle outState) {


        outState.putSerializable(key, (Serializable) value);
    }

    @Override
    public T onRestore(String key, Bundle savedState) {
        return (T)savedState.getSerializable(key);
    }
}
