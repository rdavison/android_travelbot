package se.davison.travelbot.util;

import android.view.View;
import android.view.animation.Animation;

/**
 * Created by admin on 2015-08-14.
 */
public class HideViewAnimationListener implements Animation.AnimationListener {

    private int LayerType;
    private final View view;
    private final boolean show;

    public HideViewAnimationListener(View view, boolean show) {
        this.view = view;
        this.show = show;
    }

    @Override
    public void onAnimationStart(Animation animation) {

        if(show){
            view.setVisibility(View.VISIBLE);
        }
        animation.setFillAfter(show?true:false);
        animation.setFillEnabled(show?true:false);
        LayerType = view.getLayerType();
        view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        view.setLayerType(LayerType, null);

        if(!show){
            view.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
