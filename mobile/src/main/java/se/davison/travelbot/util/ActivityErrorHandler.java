package se.davison.travelbot.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by richard on 03/09/15.
 */
public class ActivityErrorHandler {

    private static final String STATE_ACTIVITY_ERROR_HANDLER_DATA = "state_activity_error_handler_data";
    private static final String TAG_ACTIVITY_ERROR_DIALOG_FRAGMENT = "tag_activity_error_dialog_fragment";


    private ArrayList<Request> data = new ArrayList<Request>();
    private FragmentManager fragmentManager;
    private boolean paused = false;

    public void error(int code, String errorString) {
        /*for (Request req : data) {
            if (req.code == code) {
                req.errorText = true;
                return;
            }
        }*/
        String tag = TAG_ACTIVITY_ERROR_DIALOG_FRAGMENT+code;
        AppCompatDialogFragment dialog = (AppCompatDialogFragment) fragmentManager.findFragmentByTag(tag);
        if(dialog == null){
            dialog = ErrorMessageDialog.newInstance(code,errorString);

        }
        if(!paused) {
            dialog.show(fragmentManager, tag);
        }

    }

    public static class Request implements Serializable {
        int code;
        public boolean error;

        public Request(int code) {
            this.code = code;
        }
    }

    public interface Callback {
        void onError(int errorCode);
    }

    public interface Action {
        void call();
    }

    public void onCreate(FragmentManager fragmentManager, Bundle savedInstanceState) {
        paused = false;
        this.fragmentManager = fragmentManager;
        if (savedInstanceState != null) {
            data = (ArrayList<Request>) savedInstanceState.getSerializable(STATE_ACTIVITY_ERROR_HANDLER_DATA);
        }
    }

    public void tryRun(int code, Action action) {
        paused = false;
        for (Request req : data) {
            if (req.code == code && req.error) {
                return;
            }
        }
        data.add(new Request(code));
        action.call();
    }


    public void onSaveInstanceState(Bundle outState) {
        paused = true;
        outState.putSerializable(STATE_ACTIVITY_ERROR_HANDLER_DATA, data);
    }

    public void onResume() {

    }

    public void onPause() {

    }

    public static class ErrorMessageDialog extends AppCompatDialogFragment {

        private static final String EXTRA_MESSAGE = "extra_message";
        private static final String EXTRA_TITLE = "extra_title";
        private static final String EXTRA_ERROR_CODE = "extra_error_code";

        public static ErrorMessageDialog newInstance(int errorCode, String message) {
            return newInstance(errorCode, message, null);
        }

        public static ErrorMessageDialog newInstance(int errorCode, String message, String title) {
            ErrorMessageDialog dialog = new ErrorMessageDialog();

            Bundle args = new Bundle();
            args.putInt(EXTRA_ERROR_CODE, errorCode);
            args.putString(EXTRA_MESSAGE, message);
            args.putString(EXTRA_TITLE, title);

            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            String message = getArguments().getString(EXTRA_MESSAGE, null);
            final int errorCode = getArguments().getInt(EXTRA_ERROR_CODE, -1);
            String title = getArguments().getString(EXTRA_TITLE, null);

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage(message);
            if (title != null) {
                builder.setTitle(title);
            }
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Activity activity = getActivity();
                    if (activity instanceof Callback) {
                        ((Callback) activity).onError(errorCode);
                    }
                }
            });
            builder.setCancelable(false);


            return builder.create();

        }
    }

}
