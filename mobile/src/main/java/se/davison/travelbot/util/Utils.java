package se.davison.travelbot.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import se.davison.travelbot.BuildConfig;
import se.davison.travelbot.R;
import se.davison.travelbot.ui.MainActivity;

/**
 * Created by admin on 2015-08-09.
 */
public class Utils {



    public static int dpToPx(Context context, float dp){
        return (int)(context.getResources().getDisplayMetrics().density*dp);
    }

    public static int getStatusBarHeight(Context context) {
        Resources resources = context.getResources();
        int result = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static boolean hasDatabase(Context context){
        return new File(context.getCacheDir().getAbsolutePath()+"/"+ Constants.DB_NAME).exists();
    }

    public static void removeDatabase(Context context){
        new File(context.getCacheDir().getAbsolutePath()+"/"+ Constants.DB_NAME).delete();
    }


    public static String exceptionToString(Context context,
                                           Exception exception) {
        String message;
        if (exception instanceof UnknownHostException) {
            message = context.getString(R.string.err_unknown_host);
        }else if (exception instanceof FileNotFoundException) {
            message = context.getString(R.string.err_file_not_found);
        } else if (exception instanceof IOException) {
            message = context.getString(R.string.err_message_io);
        } else {
            message = context.getString(R.string.err_common);
        }
        return message;
    }

    public static Location getRandomDebugLocation(){
        if(BuildConfig.DEBUG){

            //centralstation, gärdegatan, brunnsparken, pilegården
            Double[] coords = new Double[]{57.708939, 11.972407, 57.676438, 12.002821, 57.706539, 11.968457, 57.638801, 11.930005};
            int rand = new Random().nextInt(coords.length);
            rand = rand-(rand%2);
            Location loc = new Location("dummyprovider");
            loc.setLatitude(coords[rand]);
            loc.setLongitude(coords[rand+1]);
            return loc;
        }
        return null;
    }
    public static int parseColor(String value) {
        if (value == null) {
            return -1;
        } else {
            try {
                return Color.parseColor(value);
            } catch (IllegalArgumentException e) {
                return -1;
            }
        }
    }
    public static String millisToString(long l) {
        long h, m;
        h = l / 3600000;
        m = (l % 3600000) / 60000;
        if (h == 0) {
            return m + "m";
        } else {
            return h + "h " + m + "m";
        }
    }

    public static int getNavigationBarHeight(Context context){
        Resources resources = context.getResources();

        int id = resources.getIdentifier(
                resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape",
                "dimen", "android");
        if (id > 0) {
            return resources.getDimensionPixelSize(id);
        }
        return 0;
    }

    public static int getActionBarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        int actionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        return actionBarSize;
    }

    public static void restartApp(Context context) {

        Intent mStartActivity = new Intent(context, MainActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public static String dateToRelativeString(Date date, String strTomorrow, String strYesterday) {



        long today = new Date().getTime();
        today -= today%86400000;

        long due = date.getTime();
        due -= due%86400000;

        long diff = ((due-today)/86400000);


        String relative = "";

        if(diff == 1){
            relative = strTomorrow + " ";
        }else if(diff == -1){
            relative = strYesterday + " ";
        }else if(diff != 0){
            relative = Constants.DATE_FORMAT_DATE.format(due) + " ";
        }

        return relative;
    }

    public static Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * <p>Intent to show an applications details page in (Settings) com.android.settings</p>
     *
     * @param context       The context associated to the application
     * @param packageName   The package name of the application
     * @return the intent to open the application info screen.
     */
    public static Intent newAppDetailsIntent(Context context, String packageName) {

        if(packageName==null){
            packageName = context.getApplicationContext().getPackageName();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("package:" + packageName));
            return intent;
        } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.FROYO) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClassName("com.android.settings",
                    "com.android.settings.InstalledAppDetails");
            intent.putExtra("pkg", packageName);
            return intent;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName("com.android.settings",
                "com.android.settings.InstalledAppDetails");
        intent.putExtra("com.android.settings.ApplicationPkgName", packageName);
        return intent;
    }
}
