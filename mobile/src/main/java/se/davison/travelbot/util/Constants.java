package se.davison.travelbot.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by richard on 29/11/14.
 */
public class Constants {


    public static final String URL_DB = "http://cgilabs.se/travel/database.sqlite";
    public static final String DB_NAME = "database.sqlite";
    public static final String API_KEY = "a11e6bf4-0d91-4abd-b5e0-24fabb95cf6e";

    public final static DateFormat DATE_FORMAT_FULL = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public final static DateFormat DATE_FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd");
    public final static DateFormat DATE_FORMAT_TIME = new SimpleDateFormat("HH:mm");
}
