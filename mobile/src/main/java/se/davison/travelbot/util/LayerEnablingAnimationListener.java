package se.davison.travelbot.util;

import android.animation.Animator;
import android.view.View;
import android.view.animation.Animation;

/**
 * Created by admin on 2015-08-14.
 */
public class LayerEnablingAnimationListener implements Animation.AnimationListener {

    private final Callback callback;

    public static abstract class Callback{
        public void onAnimationEnd(Animation animation){

        }
        public void onAnimationStart(Animation animation){

        }
    }

    private final View mTargetView;
    private int mLayerType;

    public LayerEnablingAnimationListener(View mTargetView) {
        this.mTargetView = mTargetView;
        callback = null;
    }

    public LayerEnablingAnimationListener(View targetView, Callback callback){
        mTargetView = targetView;
        this.callback = callback;
    }

    @Override
    public void onAnimationStart(Animation animation) {
        mLayerType = mTargetView.getLayerType();
        mTargetView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        if(callback!=null){
            callback.onAnimationStart(animation);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        mTargetView.setLayerType(mLayerType, null);
        if(callback!=null){
            callback.onAnimationEnd(animation);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
