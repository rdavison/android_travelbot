package se.davison.travelbot.util;

import android.animation.Animator;
import android.view.View;

/**
 * Created by admin on 2015-08-07.
 */
public class LayerEnablingAnimatorListener implements Animator.AnimatorListener {

    private final Callback callback;

    public static abstract class Callback{
        public void onAnimationEnd(Animator animation){

        }
        public void onAnimationStart(Animator animation){

        }
    }

    private final View mTargetView;
    private int mLayerType;

    public LayerEnablingAnimatorListener(View targetView) {
        mTargetView = targetView;
        callback = null;
    }

    public LayerEnablingAnimatorListener(View targetView, Callback callback){
        mTargetView = targetView;
        this.callback = callback;
    }

    public View getTargetView() {
        return mTargetView;
    }

    @Override
    public void onAnimationStart(Animator animation) {
        mLayerType = mTargetView.getLayerType();
        mTargetView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        if(callback!=null){
            callback.onAnimationStart(animation);
        }
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        mTargetView.setLayerType(mLayerType, null);
        if(callback!=null){
            callback.onAnimationEnd(animation);
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
