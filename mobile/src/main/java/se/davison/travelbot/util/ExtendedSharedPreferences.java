package se.davison.travelbot.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import se.davison.travelbot.model.Stop;

/**
 * Created by admin on 2015-08-22.
 */
public class ExtendedSharedPreferences implements SharedPreferences {

    private final SharedPreferences sharedPreferences;
    private final ExtendedEditor editor;

    private ExtendedSharedPreferences() {
        editor = null;
        sharedPreferences = null;
    }

    private ExtendedSharedPreferences(SharedPreferences preferences) {
        this.sharedPreferences = preferences;
        editor = ExtendedEditor.from(sharedPreferences.edit());
    }

    public static ExtendedSharedPreferences from(Context context) {
        return new ExtendedSharedPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    @Override
    public Map<String, ?> getAll() {
        return sharedPreferences.getAll();
    }

    @Nullable
    @Override
    public String getString(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, Set<String> defValues) {
        return sharedPreferences.getStringSet(key, defValues);
    }

    @Override
    public int getInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

    @Override
    public long getLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return sharedPreferences.getFloat(key, defValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    public <T> List<T> getList(String key, List<T> defValue, Class<T> type) {



        Type classType = com.google.gson.internal.$Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, type);

        String value = sharedPreferences.getString(key, null);
        if (value == null) {
            return defValue;
        }

        return new Gson().fromJson(value, classType);



        /*String value = sharedPreferences.getString(key, null);

        return Arrays.asList(new Gson().fromJson(value, clazz));*/
    }


    @Override
    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    @Override
    public ExtendedEditor edit() {

        return editor;
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener);

    }

    public static class ExtendedEditor implements Editor {

        private final Editor editor;

        private ExtendedEditor(Editor editor) {
            this.editor = editor;
        }

        /*
        public <T> ExtendedEditor putArrayList(String key, ArrayList<T> value) {
            editor.putString(key,new Gson().toJson(value,new TypeToken<ArrayList<T>>(){}.getStopType()));
            return this;
        }*/

        public <T> ExtendedEditor putList(String key,List<T> value) {
            editor.putString(key,new Gson().toJson(value));
            return this;
        }

        @Override
        public ExtendedEditor putString(String key, String value) {
            editor.putString(key, value);
            return this;
        }

        @Override
        public ExtendedEditor putStringSet(String key, Set<String> values) {
            editor.putStringSet(key, values);
            return this;
        }

        @Override
        public ExtendedEditor putInt(String key, int value) {
            editor.putInt(key, value);
            return this;
        }

        @Override
        public ExtendedEditor putLong(String key, long value) {
            editor.putLong(key, value);
            return this;
        }

        @Override
        public ExtendedEditor putFloat(String key, float value) {
            editor.putFloat(key, value);
            return this;
        }

        @Override
        public ExtendedEditor putBoolean(String key, boolean value) {
            editor.putBoolean(key, value);
            return this;
        }

        @Override
        public ExtendedEditor remove(String key) {
            editor.remove(key);
            return this;
        }

        @Override
        public ExtendedEditor clear() {
            editor.clear();
            return this;
        }

        @Override
        public boolean commit() {
            return editor.commit();
        }

        @Override
        public void apply() {
            editor.apply();
        }

        private ExtendedEditor() {
            editor = null;
        }

        public static ExtendedEditor from(Editor editor) {
            return new ExtendedEditor(editor);
        }
    }
}
