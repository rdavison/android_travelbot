package se.davison.travelbot.net;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import se.davison.travelbot.model.Journey;

/**
 * Created by admin on 2015-08-15.
 */
public class JourneyResponse extends BaseResponse {

    @SerializedName("Trip")
    public List<Journey> journeys;

    public List<Journey> getJourneys() {
        return journeys;
    }

    public void setJourneys(List<Journey> journeys) {
        this.journeys = journeys;
    }
}
