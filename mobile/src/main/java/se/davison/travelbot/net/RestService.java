package se.davison.travelbot.net;

import java.util.Map;


import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by admin on 2015-08-14.
 */
public interface RestService {



    @GET("departureBoard"+RestClient.BASIC_PARAMS+"&maxDeparturesPerLine=2")
    Observable<NextTripResponse> nextTrip(@Query("id") String id);



    //http://api.vasttrafik.se/bin/rest.exe//trip?authKey=a11e6bf4-0d91-4abd-b5e0-24fabb95cf6e&format=json&date=2015-08-15&time=10:30&originId=9021014004090000&destId=9021014084961000&searchForArrival=1
    @GET("trip"+RestClient.BASIC_PARAMS)
    Observable<JourneyResponse> journey(@Query("date") String date, @Query("time") String time, @Query("searchForArrival") int searchForArrival, @QueryMap Map<String, String> options);

    //http://api.vasttrafik.se/bin/rest.exe/location.name?authKey=a11e6bf4-0d91-4abd-b5e0-24fabb95cf6e&format=json&input=Hults%20Gata
    @GET("location.name"+RestClient.BASIC_PARAMS)
    Observable<LocationResponse> locationName(@Query("input") String input);


    //http://api.vasttrafik.se/bin/rest.exe/v1/journeyDetail?ref=56973%2F33181%2F164712%2F63370%2F80%3Fdate%3D2016-03-11%26station_evaId%3D5283001%26station_type%3Ddep%26authKey%3Da11e6bf4-0d91-4abd-b5e0-24fabb95cf6e%26format%3Djson%26
    @GET("journeyDetail")
    Observable<JourneyDetailResponse> journeyDetail(@QueryMap Map<String,String> params);


    //http://api.vasttrafik.se/bin/rest.exe/location.nearbystops?authKey=a11e6bf4-0d91-4abd-b5e0-24fabb95cf6e&format=json&originCoordLat=57.676651&originCoordLong=12.002689&maxNo=10
    @GET("location.nearbystops"+RestClient.BASIC_PARAMS)
    Observable<LocationResponse> nearbyStops(@Query("originCoordLat") String lat, @Query("originCoordLong") String lng, @Query("maxNo") int maxCount);




}
