package se.davison.travelbot.net;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Stop;

/**
 * Created by admin on 2015-08-25.
 */
public class LocationResponse extends BaseResponse {

    @SerializedName("CoordLocation")
    private List<Stop> addresses;

    public List<Stop> getAddresses() {
        return addresses;
    }

    @SerializedName("StopLocation")
    private List<Stop> stops;

    public List<Stop> getStops() {
        return stops;
    }
}
