package se.davison.travelbot.net;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import se.davison.travelbot.model.TimedDeparture;

/**
 * Created by admin on 2015-08-15.
 */
public class NextTripResponse extends BaseResponse {

    @SerializedName("Departure")
    private List<TimedDeparture> departures;

    public NextTripResponse(TimedDeparture departure) {
        departures = new ArrayList<>();
        departures.add(departure);
    }

    public List<TimedDeparture> getDepartures() {
        return departures;
    }

    public void setDepartures(List<TimedDeparture> departures) {
        this.departures = departures;
    }


}
