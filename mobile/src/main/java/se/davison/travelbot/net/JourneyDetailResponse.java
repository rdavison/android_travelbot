package se.davison.travelbot.net;

import android.graphics.Color;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.model.TimedStop;

/**
 * Created by richard on 11/03/16.
 */
public class JourneyDetailResponse extends BaseResponse implements Serializable {

    @SerializedName("Stop")
    private List<Stop> stops;

    @SerializedName("Color")
    private ColorData colorData;

    public JourneyDetailResponse(Stop... stops) {
        super();
        this.stops = Arrays.asList(stops);
        this.colorData = new ColorData();
    }

    public void setLeg(Leg leg) {

        boolean included = false;

        int i = 0;
        for(Stop stop : stops){

            stop.setIncluded(included);

            if(stop.equals(leg.getOrigin()) || stop.equals(leg.getDestination())){
                included = !included;
                stop.setIncluded(true);
            }

            i++;
        }

    }

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public ColorData getColorData() {
        return colorData;
    }

    public Stop findById(String id) {
        for(Stop s : stops){
            if(s.getId().equals(id)){
                return s;
            }
        }
        return null;
    }

    public static class ColorData implements Serializable {
        private String fgColor = "#000000";
        private String bgColor = "#ffffff";
        private String stroke;

        public int getFgColor() {
            return Color.parseColor(fgColor);
        }

        public int getBgColor(){
            return Color.parseColor(bgColor);
        }
    }

    //private LineColor color;



}
