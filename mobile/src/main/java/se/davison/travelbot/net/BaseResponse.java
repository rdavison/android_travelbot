package se.davison.travelbot.net;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 2015-08-15.
 */
public abstract class BaseResponse {
    @SerializedName("servertime")
    public String serverTime;

    @SerializedName("serverdate")
    public String serverDate;

    @SerializedName("errorText")
    public String errorText;

    @SerializedName("error")
    public String errorCode;

    public boolean isSuccessful(){
        return errorText ==null;
    }



}
