package se.davison.travelbot.net;

import android.support.v4.util.Pair;
import android.util.Log;
import android.util.Xml;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.functions.FuncN;
import se.davison.travelbot.model.TimedDeparture;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.model.TimedStop;
import se.davison.travelbot.model.TransportMode;
import se.davison.travelbot.util.Constants;

/**
 * Created by admin on 2015-08-14.
 */
public class RestClient {
    public static final String BASIC_PARAMS = "?authKey=a11e6bf4-0d91-4abd-b5e0-24fabb95cf6e&format=json";
    private static final String TAG = RestClient.class.getSimpleName();
    public static final String BASE_URL = "http://api.vasttrafik.se/bin/rest.exe/";

    private RestService service;

    private static abstract class NestedDeserializer<T>  implements JsonDeserializer<T>{

        private final Pair<Type, JsonDeserializer>[] innerSerializers;
        protected final Gson gson;

        public NestedDeserializer(Pair<Type, JsonDeserializer>... innerSerializers) {
            this.innerSerializers = innerSerializers;
            GsonBuilder builder = new GsonBuilder();
            for (Pair<Type, JsonDeserializer> serializer :
                    innerSerializers) {
                builder.registerTypeAdapter(serializer.first, serializer.second);
            }
            gson = builder.create();
        }

        @Override
        public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return (T) gson.fromJson(json, typeOfT);
        }


    }


    private static class HafasDeserializer<T extends BaseResponse> extends InnerDeserializer<BaseResponse>{

        public HafasDeserializer(String wrapper) {
            super(wrapper);
        }

        public HafasDeserializer(String wrapper, Pair<Type, JsonDeserializer>... innerSerializers) {
            super(wrapper, innerSerializers);
        }

        @Override
        public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            BaseResponse response = super.deserialize(json, typeOfT, context);

            RestClient.raiseExceptionIRequestError(response);

            return (T) response;
        }
    }

    private static class InnerDeserializer<T> extends NestedDeserializer<T> {


        private static final String TAG = InnerDeserializer.class.getSimpleName();
        private final String wrapper;

        public InnerDeserializer(String wrapper) {
            super();
            this.wrapper = wrapper;
        }

        public InnerDeserializer(String wrapper, Pair<Type, JsonDeserializer>... innerSerializers) {
            super(innerSerializers);
            this.wrapper = wrapper;
        }

        public String getWrapper() {
            return wrapper;
        }

        @Override
        public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject rootObj = json.getAsJsonObject();
            json = rootObj.getAsJsonObject(wrapper);
            if(json!=null){
                Log.d(TAG, json.toString());
            }
            return super.deserialize(json,typeOfT,context);
        }


    }






    private static class NextTripDeserializer extends HafasDeserializer<NextTripResponse> {


        public static final String DEPARTURE = "Departure";

        public NextTripDeserializer(Pair<Type, JsonDeserializer>... innerSerializers) {
            super("DepartureBoard", innerSerializers);
        }

        @Override
        public NextTripResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {


            JsonObject baseElement = json.getAsJsonObject().getAsJsonObject(getWrapper());
            JsonElement departureElement = baseElement.get(DEPARTURE);

            List<TimedDeparture> departures = new ArrayList<>(100);

            if(!departureElement.isJsonArray()){
                baseElement.remove(DEPARTURE);

                TimedDeparture departure =gson.fromJson(departureElement,TimedDeparture.class);
                departures.add(departure);
            }

            NextTripResponse response = super.deserialize(json, typeOfT, context);
            if(response.getDepartures()==null){
                response.setDepartures(departures);
            }
            departures = TimedDeparture.merge(response.getDepartures());

            Date serverDateTime;
            try {
                serverDateTime = Constants.DATE_FORMAT_FULL.parse(String.format("%s %s", response.serverDate, response.serverTime));
            } catch (ParseException e) {
                serverDateTime = new Date();
            }


            for (TimedDeparture dep : departures) {
                dep.fix(serverDateTime);
            }
            Collections.sort(departures);
            response.setDepartures(departures);




            return response;
        }
    }

    private static void raiseExceptionIRequestError(BaseResponse response) throws ServiceException {
        if(!response.isSuccessful()){
            response.errorCode.replace(" hafasError","");
            throw new ServiceException(response.errorCode,response.errorText);
        }
    }

    public static class ServiceException extends JsonParseException {

        public String errorCode;

        public ServiceException(String errorCode, String errorText) {
            super(errorText);
            this.errorCode = errorCode;
        }
    }

    private static class JourneyDeserializer extends NestedDeserializer<Journey>{


        public static final String JSON_KEY_LEG = "Leg";



        public JourneyDeserializer(Pair<Type, JsonDeserializer>... innerSerializers) {
            super(innerSerializers);
        }

        @Override
        public Journey deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

            JsonElement legElement = json.getAsJsonObject().get(JSON_KEY_LEG);

            Leg oneLeg = null;

            if (legElement.isJsonObject()) {

                oneLeg = gson.fromJson(legElement,Leg.class);
                json.getAsJsonObject().remove(JSON_KEY_LEG);
            }

            Journey journey = super.deserialize(json, typeOfT, context);

            if(oneLeg != null){
                List<Leg> legs = new ArrayList<Leg>(1);
                legs.add(oneLeg);
                journey.setLegs(legs);
            }

            //what happens here
            List<Leg> fixedLegs = new ArrayList<>(journey.getLegs().size());
            for (Leg leg :
                    journey.getLegs()) {
                if(!leg.getOrigin().getName().equals(leg.getDestination().getName())){
                    fixedLegs.add(leg);
                }
            }

            journey.setLegs(fixedLegs);

            return journey;


        }

    }


    private class LegDeserializer extends NestedDeserializer<Leg> {


        public LegDeserializer(Pair<Type, JsonDeserializer> innerStopSerializer) {
            super(innerStopSerializer);
        }

        @Override
        public Leg deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {


            Leg leg = super.deserialize(json, typeOfT, context);
            JsonObject object = json.getAsJsonObject();
            if(object.has("JourneyDetailRef")){
                String detailRef = object.getAsJsonObject("JourneyDetailRef").get("ref").getAsString();
                leg.setJourneyDetailReference(detailRef);
            }
            leg.fix();
            return leg;
        }
    }


    public static class StopDeserializer extends NestedDeserializer<Stop> {

        public static final String JSON_KEY_TYPE = "type";
        public static final String JSON_KEY_ADDRESS = "ADR";

        @Override
        public Stop deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Stop stop = super.deserialize(json,typeOfT,context);
            if (json.getAsJsonObject().has(JSON_KEY_TYPE) && json.getAsJsonObject().get(JSON_KEY_TYPE).getAsString().equals(JSON_KEY_ADDRESS)) {
                stop.setStopType(Stop.TYPE_ADDRESS);
            }
            stop.fixName();
            return stop;
        }
    }


    private static class Holder {
        static final RestClient INSTANCE = new RestClient();
    }

    private RestClient() {




        StopDeserializer stopDeserializer = new StopDeserializer();
        Pair<Type, JsonDeserializer> innerStopSerializer = new Pair(Stop.class, stopDeserializer);
        Pair<Type, JsonDeserializer> innerTimedStopSerializer = new Pair(TimedStop.class, stopDeserializer);
        LegDeserializer legDeserializer = new LegDeserializer(innerTimedStopSerializer);
        Pair<Type, JsonDeserializer> innerLegSerializer = new Pair(Leg.class,legDeserializer);
        JourneyDeserializer journeyDeserializer = new JourneyDeserializer(innerLegSerializer,innerStopSerializer);
        Pair<Type, JsonDeserializer> innerJourneySerializer = new Pair(Journey.class,journeyDeserializer);



        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Stop.class, stopDeserializer)
                .registerTypeAdapter(TimedStop.class, stopDeserializer)
                .registerTypeAdapter(Leg.class, legDeserializer)
                .registerTypeAdapter(Journey.class, journeyDeserializer)
                .registerTypeAdapter(JourneyDetailResponse.class, new HafasDeserializer<JourneyDetailResponse>("JourneyDetail",innerStopSerializer))
                .registerTypeAdapter(NextTripResponse.class, new NextTripDeserializer(innerStopSerializer))
                .registerTypeAdapter(JourneyResponse.class, new HafasDeserializer<JourneyResponse>("TripList",innerJourneySerializer,innerLegSerializer,innerTimedStopSerializer))
                .registerTypeAdapter(LocationResponse.class, new InnerDeserializer<LocationResponse>("LocationList", innerStopSerializer))
                .create();


        OkHttpClient httpClient = new OkHttpClient.Builder().addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Log.d(TAG,"REQUEST URL === "+chain.request().url().toString());

                return chain.proceed(chain.request());
            }
        }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();


        service = retrofit.create(RestService.class);



    }


    private Map<String, String> stopOptions(Stop stop, boolean isOrigin) {
        Map<String, String> options = new HashMap<String, String>(3);
        String prefix = isOrigin ? "origin" : "dest";
        if(stop.getStopType()==Stop.TYPE_ADDRESS){
            options.put(prefix + "CoordLong", stop.getLongitude());
            options.put(prefix + "CoordLat", stop.getLatitude());
            options.put(prefix + "CoordName", stop.getName());
        }else{
            options.put(prefix + "Id", stop.getId());
        }
        return options;
    }

    public Observable<JourneyResponse> journey(final Stop origin, final Stop destination, Date dateAndTime, boolean searchForArrival) {

        Map<String, String> options = new HashMap<String, String>(6);
        options.putAll(stopOptions(origin,true));
        options.putAll(stopOptions(destination,false));
        return service.journey(Constants.DATE_FORMAT_DATE.format(dateAndTime), Constants.DATE_FORMAT_TIME.format(dateAndTime), searchForArrival ? 1 : 0, options).map(new Func1<JourneyResponse, JourneyResponse>() {
            @Override
            public JourneyResponse call(JourneyResponse journeyResponse) {

                for (Journey journey : journeyResponse.getJourneys()) {

                    Leg firstLeg = journey.getLegs().get(0);
                    if(firstLeg.getTransportMode() == TransportMode.WALK){
                        Stop firstStop = firstLeg.getOrigin();
                        firstStop.setLatitude(origin.getLatitude());
                        firstStop.setLongitude(origin.getLongitude());
                    }

                    Leg lastLeg = journey.getLegs().get(journey.getLegs().size()-1);

                    if(lastLeg.getTransportMode() == TransportMode.WALK) {
                        Stop stop2 = lastLeg.getDestination();
                        stop2.setLatitude(destination.getLatitude());
                        stop2.setLongitude(destination.getLongitude());
                    }
                }

                return journeyResponse;
            }
        });
    }

    public Observable<ArrayList<JourneyDetailResponse>> journeyDetails(final Journey journey) {

        List<Observable<JourneyDetailResponse>> journeyDetails = new ArrayList<>(10);

        int i = 0;
        for(final Leg leg : journey.getLegs()){
            if(leg.getJourneyDetailReference()!=null) {


                try {
                    final int finalIndex = i;
                    journeyDetails.add(service.journeyDetail(splitQuery(new URL(leg.getJourneyDetailReference()))).map(new Func1<JourneyDetailResponse, JourneyDetailResponse>() {
                        @Override
                        public JourneyDetailResponse call(JourneyDetailResponse journeyDetailResponse) {
                            Leg prevLeg;
                            if(finalIndex > 0 && (prevLeg = journey.getLegs().get(finalIndex -1)).getTransportMode() == TransportMode.WALK){
                                Stop prevDestination = prevLeg.getJourneyDetail().getStops().get(1);
                                Stop stop = journeyDetailResponse.findById(prevDestination.getId());
                                prevDestination.setLatitude(stop.getLatitude());
                                prevDestination.setLongitude(stop.getLongitude());
                            }
                            leg.setJourneyDetail(journeyDetailResponse);
                            journeyDetailResponse.setLeg(leg);
                            return journeyDetailResponse;
                        }
                    }));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }else{
                TimedStop origin = new TimedStop(leg.getOrigin());
                origin.setIncluded(true);
                TimedStop destination = new TimedStop(leg.getDestination());
                destination.setIncluded(true);
                leg.setJourneyDetail(new JourneyDetailResponse(origin,destination));
            }
            i++;
        }

        return Observable.zip(journeyDetails, new FuncN<ArrayList<JourneyDetailResponse>>(){

            @Override
            public ArrayList<JourneyDetailResponse> call(Object... args) {
                JourneyDetailResponse[] array = new JourneyDetailResponse[args.length];
                System.arraycopy(args,0,array,0,args.length);

                int i = 0;

                for(Leg leg : journey.getLegs()){
                    Leg nextLeg = null;
                    if(i < journey.getLegs().size()-1 && (nextLeg = journey.getLegs().get(i+1)).getTransportMode() == TransportMode.WALK){
                        Stop nextOrigin = nextLeg.getJourneyDetail().getStops().get(0);
                        Stop stop = leg.getJourneyDetail().findById(nextOrigin.getId());
                        nextOrigin.setLatitude(stop.getLatitude());
                        nextOrigin.setLongitude(stop.getLongitude());
                    }
                    i++;
                }



                return new ArrayList(Arrays.asList(array));
            }
        });
    }

    public static RestClient getInstance() {
        return Holder.INSTANCE;
    }

    public static RestService service() {
        return Holder.INSTANCE.service;
    }

    public Observable<Integer> downloadFile(final String url, final File path, final String name) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {

                InputStream input = null;
                OutputStream output = null;
                HttpURLConnection connection = null;
                int progress = 0;
                try {
                    URL theUrl = new URL(url);
                    connection = (HttpURLConnection) theUrl.openConnection();
                    connection.connect();

                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        throw new IOException("Server returned HTTP " + connection.getResponseCode()
                                + " " + connection.getResponseMessage());
                    }
                    int fileLength = connection.getContentLength();

                    // download the file
                    input = connection.getInputStream();
                    output = new FileOutputStream(new File(path, name));

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1 && !subscriber.isUnsubscribed()) {
                        total += count;
                        if (fileLength > 0) {
                            int newProgress = (int) (total * 100 / fileLength);
                            if (newProgress != progress) {
                                progress = newProgress;
                                subscriber.onNext((int) (total * 100 / fileLength));
                            }
                        }
                        output.write(data, 0, count);
                    }
                } catch (Exception e) {
                    subscriber.onError(e);
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                    if (connection != null)
                        connection.disconnect();

                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onCompleted();
                    }
                }

            }
        });
    }

    public static Map<String, String> splitQuery(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    public static class ArrayAdapter<T> extends TypeAdapter<List<T>> {
        private Class<T> adapterclass;

        public ArrayAdapter(Class<T> adapterclass) {

            this.adapterclass = adapterclass;
        }

        public List<T> read(JsonReader reader) throws IOException {


            List<T> list = new ArrayList<T>();

            Gson gson = new Gson();

            if (reader.peek() == JsonToken.BEGIN_OBJECT) {

                T inning = (T) gson.fromJson(reader, adapterclass);
                list.add(inning);

            } else if (reader.peek() == JsonToken.BEGIN_ARRAY) {

                reader.beginArray();
                while (reader.hasNext()) {
                    T inning = (T) gson.fromJson(reader, adapterclass);
                    list.add(inning);
                }
                reader.endArray();

            } else {
                reader.skipValue();
            }

            return list;
        }

        public void write(JsonWriter writer, List<T> value) throws IOException {

        }

    }

    public static class ArrayAdapterFactory implements TypeAdapterFactory {

        @SuppressWarnings({ "unchecked" })
        @Override
        public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> type) {

            ArrayAdapter typeAdapter = null;
            try {
                if (type.getRawType() == List.class)
                {

                    typeAdapter = new ArrayAdapter(
                            (Class) ((ParameterizedType) type.getType())
                                    .getActualTypeArguments()[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return typeAdapter;
        }

    }

}
