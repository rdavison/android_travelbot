package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;

/**
 * Created by admin on 2015-08-16.
 */
public class SearchResultHeaderAdapterItem extends AdapterItem<SearchResultHeaderAdapterItem.ViewHolder> {

    final private String title;
    public SearchResultHeaderAdapterItem(String title) {
        this.title = title;

    }

    @Override
    public boolean isEnabled() {
        return false;
    }



    @Override
    public boolean isHeader() {
        return true;
    }

    @Override
    public boolean isPinned() {
        return false;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.list_search_result_title, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.title.setText(title);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(android.R.id.title);
        }
    }
}
