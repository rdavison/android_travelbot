package se.davison.travelbot.adapters.adapteritems;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;

/**
 * Created by richard on 16/09/15.
 */
public class MapTileAdapterItem extends AdapterItem<MapTileAdapterItem.ViewHolder> {


    private static final String GOOGLE_MAP_TILE_STRING = "http://maps.googleapis.com/maps/api/staticmap?center=%s,%s&zoom=%d&size=%dx%d&maptype=roadmap&sensor=false";
    private final Rect margins;
    private final Rect paddings;

    private String latitude;
    private String longitude;
    private int zoom = 15;
    private int height;
    private int width;
    private int placeholderResource;
    private int pushpinResource;

    public MapTileAdapterItem(String latitude, String longitude, int zoom, int height, int width, int placeholderResource, int pushpinResource,Rect margins, Rect paddings) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.zoom = zoom;
        this.height = height;
        this.width = width;
        this.placeholderResource = placeholderResource;
        this.margins = margins;
        this.paddings = paddings;
        this.pushpinResource = pushpinResource;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {

        FrameLayout root = new FrameLayout(inflater.getContext());
        root.setClickable(true);

        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,height);
        if(margins!=null) {
            layoutParams.leftMargin = margins.left;
            layoutParams.topMargin = margins.top;
            layoutParams.rightMargin = margins.right;
            layoutParams.bottomMargin = margins.bottom;
        }
        root.setLayoutParams(layoutParams);

        ImageView imageView = new ImageView(inflater.getContext());
        imageView.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
        imageView.setImageResource(pushpinResource);
        if(paddings!=null) {
            imageView.setPadding(paddings.left, paddings.top, paddings.right, paddings.bottom);
        }

        root.addView(imageView);

        return new ViewHolder(root,imageView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        Picasso.with(viewHolder.imageView.getContext()).load(String.format(GOOGLE_MAP_TILE_STRING, latitude, longitude, zoom, width, height)).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                BitmapDrawable drawable = new BitmapDrawable(viewHolder.itemView.getContext().getResources(), bitmap);

                if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.itemView.setBackgroundDrawable(drawable);
                } else {
                    viewHolder.itemView.setBackground(drawable);
                }
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    @Override
    public boolean isHeader() {
        return true;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        FrameLayout rootView;

        public ViewHolder(FrameLayout rootView,ImageView imageView) {
            super(rootView);


            this.rootView = rootView;
            this.imageView = imageView;

            this.imageView.setScaleType(ImageView.ScaleType.CENTER);

            TypedValue outValue = new TypedValue();
            imageView.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
            this.rootView.setForeground(ContextCompat.getDrawable(rootView.getContext(), outValue.resourceId));
        }
    }
}
