package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import se.davison.travelbot.R;
import se.davison.travelbot.model.TimedDeparture;

/**
 * Created by richard on 05/09/15.
 */
public class DepartureAdapterItem extends CardAdapterItem<DepartureAdapterItem.ViewHolder> {




    private final TimedDeparture departure;
    private final String strNow;

    public DepartureAdapterItem(TimedDeparture departure, String now) {
        this.departure = departure;
        this.strNow = now;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private int dp8 = 0;


        TextView txtLine;
        TextView txtDirection;
        TextView txtTrack;
        TextView txtNextTrip;
        TextView txtThereafterTrip;
        TextView txtDirectionSub;

        public ViewHolder(View convertView) {
            super(convertView);

            this.txtLine = (TextView) convertView.findViewById(R.id.txt_line);
            this.txtDirection = (TextView) convertView.findViewById(R.id.txt_direction);
            this.txtTrack = (TextView) convertView.findViewById(R.id.txt_track);
            this.txtDirectionSub = (TextView) convertView.findViewById(R.id.txt_direction_sub);
            this.txtNextTrip = (TextView) convertView.findViewById(R.id.txt_next_trip);
            this.txtThereafterTrip = (TextView) convertView.findViewById(R.id.txt_thereafter_trip);
        }
    }


    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.list_departure, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder,position);

        String subDirection = departure.getSubDirection();
        if(!departure.isNumeric()){
            holder.txtLine.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
        }
        holder.txtLine.setText(departure.getLine());
        holder.txtLine.setBackgroundColor(departure.getFgColor());
        holder.txtLine.setTextColor(departure.getBgColor());
        holder.txtDirection.setText(departure.getDirection());
        if (subDirection != null) {
            holder.txtDirectionSub.setText(subDirection);
            holder.txtDirectionSub.setVisibility(View.VISIBLE);
        } else {
            holder.txtDirectionSub.setVisibility(View.GONE);
        }
        String track = departure.getTrack();
        if(departure.showTrack()){
            holder.txtTrack.setText(track);
            holder.txtTrack.setVisibility(View.VISIBLE);
        }else{
            holder.txtTrack.setVisibility(View.GONE);
        }
        holder.txtNextTrip.setText(departure.getTimeNext(strNow));
        holder.txtThereafterTrip.setText(departure.getTimeThereAfter(strNow));
    }

}
