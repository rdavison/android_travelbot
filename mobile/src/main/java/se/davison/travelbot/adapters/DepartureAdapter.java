package se.davison.travelbot.adapters;

/**
 * Created by admin on 2015-08-29.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import se.davison.travelbot.R;
import se.davison.travelbot.model.TimedDeparture;

/**
 * Created by richard on 10/01/15.
 */
public class DepartureAdapter extends RecyclerView.Adapter<DepartureAdapter.ViewHolder> {

    private List<TimedDeparture> departures = new ArrayList<TimedDeparture>();
    private LayoutInflater inflater;
    private String strNow;

    public DepartureAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.strNow = context.getString(R.string.now);
    }

    public void setDepartures(List<TimedDeparture> departures) {
        this.departures = departures;
        notifyItemRangeInserted(0,departures.size());
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView txtLine;
        TextView tatDirection;
        TextView txtTrack;
        TextView txtNextTrip;
        TextView txtThereafterTrip;
        TextView txtDirectionSub;

        public ViewHolder(View itemView) {
            super(itemView);


            this.txtLine  = (TextView) itemView.findViewById(R.id.txt_line);
            this.tatDirection = (TextView) itemView.findViewById(R.id.txt_direction);
            this.txtTrack = (TextView) itemView.findViewById(R.id.txt_track);
            this.txtDirectionSub = (TextView) itemView.findViewById(R.id.txt_direction_sub);
            this.txtNextTrip = (TextView) itemView.findViewById(R.id.txt_next_trip);
            this.txtThereafterTrip = (TextView) itemView.findViewById(R.id.txt_thereafter_trip);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View convertView = inflater.inflate(R.layout.list_departure, parent, false);

        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        int backgroundResource = R.drawable.card_background_middle;

        if(position==0){
            backgroundResource = R.drawable.card_background_first;
        }else if(position==getItemCount()-1){
            backgroundResource = R.drawable.card_background_last;
        }

        holder.itemView.setBackgroundResource(backgroundResource);

        TimedDeparture departure = departures.get(position);
        String subDirection = departure.getSubDirection();
        if(!departure.isNumeric()){
            holder.txtLine.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
        }
        holder.txtLine.setText(departure.getLine());
        holder.txtLine.setBackgroundColor(departure.getFgColor());
        holder.txtLine.setTextColor(departure.getBgColor());
        holder.tatDirection.setText(departure.getDirection());
        if (subDirection != null) {
            holder.txtDirectionSub.setText(subDirection);
            holder.txtDirectionSub.setVisibility(View.VISIBLE);
        } else {
            holder.txtDirectionSub.setVisibility(View.GONE);
        }
        String track = departure.getTrack();
        if(track!=null){
            holder.txtTrack.setText(track);
        }else{
            holder.txtTrack.setText("");
        }
        holder.txtNextTrip.setText(departure.getTimeNext(strNow));
        holder.txtThereafterTrip.setText(departure.getTimeThereAfter(strNow));
    }

    @Override
    public int getItemCount() {
        return departures==null?0:departures.size();
    }
}
