package se.davison.travelbot.adapters;

/**
 * Created by richard on 05/01/15.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import se.davison.travelbot.R;
import se.davison.travelbot.db.StopProvider;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.net.RestClient;

public class StopCursorAdapter extends CursorAdapter {

    private Context context;
    private LayoutInflater inflater;
    private AutoCompleteTextView autoCompleteTextView;
    private List<Stop> extraLocations;

    public static interface OnItemClickListener {
        public void onItemClicked(Stop stop);
    }

    private static class ViewHolder {
        TextView lineOne;
        TextView lineTwo;

        public ViewHolder(TextView lineOne, TextView lineTwo) {
            this.lineOne = lineOne;
            this.lineTwo = lineTwo;
        }
    }



    @Override
    public int getCount() {
        return extraLocations.size() + super.getCount();
    }

    public StopCursorAdapter(final AutoCompleteTextView autoCompleteTextView, final Context context, final OnItemClickListener itemClickListener) {
        super(context, null, false);
        this.context = context;
        this.autoCompleteTextView = autoCompleteTextView;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        extraLocations = new ArrayList<Stop>();




        this.autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Stop stop;
                if(extraLocations.isEmpty()){
                    stop = new Stop(getCursor().getString(1), getCursor().getString(2), getCursor().getString(3), getCursor().getString(4), getCursor().getString(5));
                }else{
                    stop = extraLocations.get(position);
                }
                autoCompleteTextView.setText(stop.getName());
                if(itemClickListener!=null){
                    itemClickListener.onItemClicked(stop);
                }

                View focusableView = autoCompleteTextView.focusSearch(View.FOCUS_DOWN);
                if(focusableView != null) focusableView.requestFocus();

                InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
            }
        });

        this.autoCompleteTextView.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                autoCompleteTextView.setTag(null);
                return false;
            }
        });
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = inflater.inflate(R.layout.list_two_line_padded, parent, false);
        return v;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (extraLocations.isEmpty() && getCursor().getCount()>0) {
            return super.getView(position, convertView, parent);
        } else if(!extraLocations.isEmpty()) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = newView(context, null, parent);
                holder = new ViewHolder((TextView) convertView.findViewById(android.R.id.text1), (TextView) convertView.findViewById(android.R.id.text2));
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            try{ //this must be done due to threading
                Stop stop = extraLocations.get(position);
                holder.lineOne.setTextColor(Color.BLACK);
                holder.lineOne.setText(stop.getName());
                holder.lineTwo.setText(stop.getDistrict());
            }catch(Exception e){

            }
            return convertView;
        }else{
            return convertView;
        }
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if (holder == null) {
            holder = new ViewHolder((TextView) view.findViewById(android.R.id.text1), (TextView) view.findViewById(android.R.id.text2));
            view.setTag(holder);
        }
        holder.lineOne.setTextColor(Color.BLACK);
        holder.lineOne.setText(cursor.getString(2));
        holder.lineTwo.setText(cursor.getString(3));
    }

    @Override
    public CharSequence convertToString(Cursor cursor) {
        if (cursor.getCount() > 0) {
            return cursor.getString(2);
        }
        return "";
    }

    @SuppressLint("DefaultLocale")
    @Override
    public Cursor runQueryOnBackgroundThread(CharSequence constraint) {
        extraLocations.clear();

        String filter = "";
        if (constraint == null) {
            filter = "";
        } else {
            filter = constraint.toString().toLowerCase();
        }

        Cursor cursor = StopProvider.getStopCursor(context.getContentResolver(), filter);


        if (cursor!=null && cursor.getCount() == 0) {
            try {
                /*RestClient.service().locationName(filter).subscribe(new Observer<List<Stop>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Stop> stops) {
                        extraLocations = stops;

                    }
                });*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return cursor;
    }
}
