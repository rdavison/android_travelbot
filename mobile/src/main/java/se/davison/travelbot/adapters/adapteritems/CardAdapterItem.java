package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;

/**
 * Created by admin on 2015-08-18.
 */
public abstract class CardAdapterItem<VH extends RecyclerView.ViewHolder> extends AdapterItem<VH>{

    private static final int CARD_BACKGROUND_MIDDLE = R.drawable.card_background_middle;
    private static final int CARD_BACKGROUND_LAST = R.drawable.card_background_last;
    private static final int CARD_BACKGROUND_FIRST = R.drawable.card_background_first;

    private static final int CARD_BACKGROUND_FULL = R.drawable.card_background;


    @Override
    public abstract VH onCreateViewHolder(LayoutInflater inflater, ViewGroup parent);

    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        int background = CARD_BACKGROUND_MIDDLE;
        if(isFirst()&&isLast()){
            background = CARD_BACKGROUND_FULL;
        } else if(isFirst()){
            background = CARD_BACKGROUND_FIRST;
        }else if(isLast()){
            background = CARD_BACKGROUND_LAST;
        }

        viewHolder.itemView.setBackgroundResource(background);
    }


}
