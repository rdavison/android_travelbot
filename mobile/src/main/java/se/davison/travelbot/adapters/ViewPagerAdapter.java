package se.davison.travelbot.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.Pair;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import se.davison.travelbot.ui.fragments.NextTripFragment;

/**
 * Created by admin on 2015-08-07.
 */
public abstract class ViewPagerAdapter extends FragmentStatePagerAdapter {
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount(){
        return registeredFragments.size();
    }

    @Override
    public abstract Fragment getItem(int position);

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public abstract CharSequence getPageTitle(int position);

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getFragment(int position) {
        return registeredFragments.get(position);
    }


    public Fragment[] getFragmentsWithClass(Class<? extends Fragment> clazz) {
        List<Fragment> fragments = new ArrayList<Fragment>();
        for(int i = 0; i < registeredFragments.size(); i++) {
            int key = registeredFragments.keyAt(i);
            Fragment fragment = registeredFragments.get(key);
            if(clazz.isAssignableFrom(fragment.getClass())){
                fragments.add(fragment);
            }
        }
        return fragments.toArray(new Fragment[]{});
    }

    public interface OnPageSelectedListener{
        void onPageSelected();
    }
}
