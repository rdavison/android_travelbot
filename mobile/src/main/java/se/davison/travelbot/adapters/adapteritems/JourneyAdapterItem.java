package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.ui.widget.JourneyInfoView;
import se.davison.travelbot.util.Constants;

/**
 * Created by richard on 08/09/15.
 */
public class JourneyAdapterItem extends AdapterItem<JourneyAdapterItem.ViewHolder> {

    private Journey journey;
    private String strFrom;

    public JourneyAdapterItem(Journey journey, String fromText) {
        this.journey = journey;
        this.strFrom = fromText;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.list_journey,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.viewJourneyInfo.setJourney(journey);

        int[] durationHM = journey.getDurationHoursMinutes();

        viewHolder.txtDuration.setText((durationHM[0]>0?durationHM[0]+"h ":"")+durationHM[1]+"m");
        viewHolder.txtTime.setText(infoText(journey.getLegs()));

    }

    private Spanned infoText(List<Leg> legs) {

        String fromTime = Constants.DATE_FORMAT_TIME.format(legs.get(0).getOrigin().getDate());
        String toTime = Constants.DATE_FORMAT_TIME.format(legs.get(legs.size()-1).getDestination().getDate());
        Stop origin = legs.get(0).getOrigin();

        int timeDiff = legs.get(0).getOrigin().getTimeDiff();

        String diff = "";
        timeDiff = (timeDiff%60==0?0:timeDiff);
        if(timeDiff!=0){
            diff = "<font color=\"#ff0000\"> "+(timeDiff<0?"":"+")+timeDiff+"</font>";
        }

        return Html.fromHtml(fromTime + diff + " - " + toTime + "<font color=\"#757575\"> - " + strFrom + " " + origin.getName() + ", " + origin.getDistrict()+"</font>");

    }

    public Journey getJourney() {
        return journey;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtDuration;
        public TextView txtTime;
        public JourneyInfoView viewJourneyInfo;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtDuration = (TextView) itemView.findViewById(R.id.txt_duration);
            this.txtTime = (TextView) itemView.findViewById(R.id.txt_time);
            this.viewJourneyInfo = (JourneyInfoView) itemView.findViewById(R.id.view_journey_info);
        }

    }
}
