package se.davison.travelbot.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import se.davison.travelbot.R;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.TransportMode;
import se.davison.travelbot.ui.widget.JourneyPathView;
import se.davison.travelbot.util.Constants;

/**
 * Created by richard on 02/08/16.
 */
public class JourneyDetailAdapter extends RecyclerView.Adapter<JourneyDetailAdapter.ViewHolder> {



    private final LayoutInflater inflater;
    private final Leg finalLeg;
    private Journey journey;
    private SparseArray<BitmapDrawable> icons;
    private SparseBooleanArray expanded;

    private String strWalk;
    private String strHour;
    private String strMinutes;
    private BitmapDrawable icCollapse;
    private BitmapDrawable icExpand;


    public JourneyDetailAdapter(Context context, Journey journey) {
        this.journey = journey;

        List<Leg> legs = journey.getLegs();

        expanded = new SparseBooleanArray(legs.size());

        Leg lastLeg = legs.get(legs.size()-1);

        finalLeg = new Leg();
        finalLeg.setTransportMode(lastLeg.getTransportMode());
        finalLeg.setBgColor(lastLeg.getBgColor());
        finalLeg.setFgColor(lastLeg.getFgColor());
        finalLeg.setOrigin(lastLeg.getDestination());

        //legs.add(finalLeg);

        inflater = LayoutInflater.from(context);

        strWalk = context.getString(R.string.walkFor);
        strHour = context.getString(R.string.hour);
        strMinutes = context.getString(R.string.minute);
        icCollapse = new BitmapDrawable(context.getResources(),BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_collapse));
        icExpand = new BitmapDrawable(context.getResources(),BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_expand));


        SparseArray<Bitmap> bitmaps = TransportMode.bitmaps(context);
        icons = new SparseArray<>(bitmaps.size());

        for(int i = 0; i < bitmaps.size(); i++){
            int key = bitmaps.keyAt(i);
            icons.put(key,new BitmapDrawable(context.getResources(),bitmaps.valueAt(i)));
        }


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View convertView = inflater.inflate(R.layout.list_journey_detail, parent, false);

        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        boolean lastPosition = position == getItemCount()-1;

        Leg leg = lastPosition? finalLeg :journey.getLegs().get(position);
        holder.txtTime.setText(Constants.DATE_FORMAT_TIME.format(leg.getOrigin().getDate()));
        holder.txtDirection.setText(leg.getDirection());
        holder.txtStop.setText(leg.getOrigin().getName());

        holder.txtLine.setText(leg.getLine());
        holder.txtLine.setBackgroundColor(leg.getFgColor());
        holder.txtLine.setTextColor(leg.getBgColor());


        boolean isExpanded = expanded.get(position,false);


        if(lastPosition){
            holder.txtExpand.setVisibility(View.GONE);
            holder.txtLine.setVisibility(View.GONE);
            holder.journeyPath.setLeg(leg,true);
            holder.txtTime.setCompoundDrawablesWithIntrinsicBounds(null,null,null,null);
            holder.txtDirection.setVisibility(View.GONE);
        }else{
            BitmapDrawable icon = icons.get(leg.getTransportMode().toResourceId());
            holder.journeyPath.setLeg(leg);
            holder.txtExpand.setVisibility(View.VISIBLE);
            if(leg.getTransportMode()==TransportMode.WALK){
                holder.txtLine.setVisibility(View.GONE);
                holder.txtDirection.setVisibility(View.GONE);
                holder.txtExpand.setCompoundDrawablesWithIntrinsicBounds(icon,null,null,null);
                int[] hoursMinutes = leg.getDurationHoursMinutes();
                String text = strWalk;
                if(hoursMinutes[0]>0){
                    text += " "+hoursMinutes[0] + strHour + " & ";
                }
                text+=" "+hoursMinutes[1] + " " + strMinutes;
                holder.txtExpand.setText(text);
            }else{

                holder.txtExpand.setCompoundDrawablesWithIntrinsicBounds(isExpanded?icCollapse:icExpand,null,null,null);
                holder.txtLine.setVisibility(View.VISIBLE);
                holder.txtDirection.setVisibility(View.VISIBLE);
                holder.txtTime.setCompoundDrawablesWithIntrinsicBounds(null,icon,null,null);
            }


        }

        holder.journeyPath.setPreviousLeg(position>0?journey.getLegs().get(position-1):null);
    }

    @Override
    public int getItemCount() {
        return journey.getLegs().size()+1;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView txtLine;
        public final TextView txtStop;
        public final TextView txtExpand;
        public final TextView txtDirection;
        public final JourneyPathView journeyPath;
        public final TextView txtTime;

        public ViewHolder(View itemView) {
            super(itemView);

            this.txtTime = (TextView) itemView.findViewById(R.id.txt_time);
            this.journeyPath = (JourneyPathView)itemView.findViewById(R.id.view_stroke);
            this.txtDirection = (TextView) itemView.findViewById(R.id.txt_direction);
            this.txtLine = (TextView) itemView.findViewById(R.id.txt_line);
            this.txtStop = (TextView) itemView.findViewById(R.id.txt_stop);
            this.txtExpand = (TextView) itemView.findViewById(R.id.txt_expand);





        }
    }
}
