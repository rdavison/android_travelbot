package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;

/**
 * Created by richard on 05/09/15.
 */
public class DepartureHeaderAdapterItem extends AdapterItem<DepartureHeaderAdapterItem.ViewHolder> {

    private final boolean hideTrack;
    private String title;

    public DepartureHeaderAdapterItem(String title) {
        this.title = title;
        hideTrack = false;
    }

    public DepartureHeaderAdapterItem(String title, boolean hideTrack) {
        this.title = title;
        this.hideTrack = hideTrack;
    }

    @Override
    public boolean isPinned() {
        return true;
    }

    @Override
    public boolean isHeader() {
        return true;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.list_departure_header,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.title.setText(title);
        viewHolder.track.setVisibility(hideTrack?View.GONE:View.VISIBLE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView track;


        public ViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(android.R.id.title);
            this.track = (TextView)itemView.findViewById(R.id.txt_track);
        }
    }

}
