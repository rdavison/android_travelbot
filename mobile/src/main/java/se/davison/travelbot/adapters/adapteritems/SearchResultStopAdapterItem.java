package se.davison.travelbot.adapters.adapteritems;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import se.davison.travelbot.R;
import se.davison.travelbot.model.Stop;

/**
 * Created by admin on 2015-08-16.
 */
public class SearchResultStopAdapterItem extends CardAdapterItem<SearchResultStopAdapterItem.ViewHolder> {

    public static final int STOP = 0;
    public static final int HISTORY = 2;
    public static final int FAVORITE = 3;

    private static final int ICON_STOP = R.drawable.ic_stop_24dp;
    private static final int ICON_ADDRESS = R.drawable.ic_address_24dp;
    private static final int ICON_LOCATION = R.drawable.ic_location_24dp;

    private int type = STOP;
    private final Stop stop;

    public SearchResultStopAdapterItem(Stop stop, int type) {
        this.stop = stop;
        this.type = type;
    }

    @Override
    public boolean isHeader() {
        return false;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        Context context = inflater.getContext();
        return new ViewHolder(inflater.inflate(R.layout.list_search_result, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder,position);

        viewHolder.txtDistrict.setText(stop.getDistrict());
        viewHolder.txtName.setText(stop.getName());
        int iconResource = ICON_STOP;
        switch (stop.getStopType()){
            case Stop.TYPE_ADDRESS:iconResource= ICON_ADDRESS;break;
            case Stop.TYPE_OTHER:iconResource= ICON_LOCATION;break;
        }
        viewHolder.imgIcon.setImageResource(iconResource);
    }

    public Stop getStop() {
        return stop;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName;
        public TextView txtDistrict;
        public ImageView imgIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(android.R.id.text1);
            txtDistrict = (TextView)itemView.findViewById(android.R.id.text2);
            imgIcon = (ImageView)itemView.findViewById(android.R.id.icon);
        }
    }
}
