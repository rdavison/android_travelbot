package se.davison.travelbot.adapters.adapteritems;

import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.ui.widget.FromToView;

/**
 * Created by richard on 08/09/15.
 */
public class JourneyFavoriteAdapterItem extends AdapterItem<JourneyFavoriteAdapterItem.ViewHolder> {

    private Stop origin;
    private Stop destination;
    private String strStop;
    private String strAddress;

    public JourneyFavoriteAdapterItem(Stop origin, Stop destination, String strStop, String strAddress) {
        this.origin = origin;
        this.destination = destination;
        this.strStop = strStop;
        this.strAddress = strAddress;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {


        FromToView fromToView = new FromToView(inflater.getContext());
        fromToView.setStopDrawableResource(R.drawable.ic_stop_dark);
        fromToView.setAddressDrawableResource(R.drawable.ic_address_dark);

        return new ViewHolder(fromToView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        viewHolder.fromToView.setStops(origin,destination);
    }

    private Spanned infoText(Stop stop) {
        return Html.fromHtml(stop.getDistrict()+"<font color=\"#757575\"> - "+(stop.getStopType()==Stop.TYPE_STOP?strStop:strAddress)+"</font>");
    }

    public Stop getOrigin() {
        return origin;
    }

    public Stop getDestination() {
        return destination;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        FromToView fromToView;

        public ViewHolder(FromToView itemView) {
            super(itemView);

            this.fromToView = itemView;

            int[] attrsBg = new int[]{R.attr.selectableItemBackground};
            TypedArray typedArray = fromToView.getContext().obtainStyledAttributes(attrsBg);
            int backgroundResource = typedArray.getResourceId(0, 0);
            fromToView.setBackgroundResource(backgroundResource);
            typedArray.recycle();

        }

    }
}
