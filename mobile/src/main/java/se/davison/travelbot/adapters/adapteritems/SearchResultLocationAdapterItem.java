package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import se.davison.travelbot.R;

/**
 * Created by admin on 2015-08-18.
 */
public class SearchResultLocationAdapterItem extends CardAdapterItem<SearchResultLocationAdapterItem.ViewHolder> {

    public static final int MY_LOCATION = 0;
    public static final int CHOOSE_FROM_MAP = 1;

    public int getType() {
        return type;
    }

    private static final int ICON_MY_LOCATION = R.drawable.ic_my_location_24dp;
    private static final int ICON_CHOOSE_FROM_MAP = R.drawable.ic_location_24dp;

    private final String title;
    private final int type;

    public SearchResultLocationAdapterItem(String title, int type) {
        this.title = title;
        this.type = type;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new SearchResultLocationAdapterItem.ViewHolder(inflater.inflate(R.layout.list_search_result_location,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder,position);
        viewHolder.txtName.setText(title);
        viewHolder.imgIcon.setImageResource(type==MY_LOCATION?ICON_MY_LOCATION:ICON_CHOOSE_FROM_MAP);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        ImageView imgIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(android.R.id.text1);
            imgIcon = (ImageView)itemView.findViewById(android.R.id.icon);
        }
    }
}
