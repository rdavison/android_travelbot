package se.davison.travelbot.adapters.adapteritems;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.travelbot.R;
import se.davison.travelbot.model.Stop;

/**
 * Created by richard on 18/11/14.
 */
public class StopAdapterItem extends AdapterItem<StopAdapterItem.ViewHolder> {

    private final Stop stop;
    private final AdapterView.OnItemClickListener overflowClickListener;

    public StopAdapterItem(Stop stop) {
        this.stop = stop;
        this.overflowClickListener = null;
    }

    public StopAdapterItem(Stop stop, AdapterView.OnItemClickListener overflowClickListener) {
        this.stop = stop;
        this.overflowClickListener = overflowClickListener;
    }

    @Override
    public boolean isHeader() {
        return false;
    }

    @Override
    public ViewHolder onCreateViewHolder(LayoutInflater inflater, ViewGroup parent) {
        return new ViewHolder(inflater.inflate(R.layout.list_stop, parent, false),overflowClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.txtDistrict.setText(stop.getDistrict());
        viewHolder.txtName.setText(stop.getName());
        viewHolder.imgOverflow.setTag(position);

    }

    public Stop getStop() {
        return stop;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgOverflow;
        public TextView txtName;
        public TextView txtDistrict;

        public ViewHolder(View itemView,final AdapterView.OnItemClickListener overflowClickListener) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(R.id.txt_name);
            txtDistrict = (TextView)itemView.findViewById(R.id.txt_district);
            imgOverflow = (ImageView)itemView.findViewById(R.id.img_overflow);
            if(overflowClickListener!=null) {
                imgOverflow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = (int) v.getTag();
                        overflowClickListener.onItemClick(null,v,position,position);
                    }
                });
            }
        }

    }
}
