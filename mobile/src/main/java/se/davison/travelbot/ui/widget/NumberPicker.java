package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import java.util.Arrays;

import se.davison.travelbot.R;

/**
 * Created by admin on 2015-08-26.
 */
public class NumberPicker extends android.widget.NumberPicker {

    private static final String NAMESPACE = "http://schemas.android.com/apk/res/android";

    private int textSize;
    private int textColor;
    private int dividerColor;



    public NumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.NumberPicker,
                0, 0);

        textColor = a.getColor(R.styleable.NumberPicker_textColor,Color.BLACK);
        dividerColor = a.getColor(R.styleable.NumberPicker_dividerColor,-1);
        textSize = a.getDimensionPixelSize(R.styleable.NumberPicker_textSize,(int) (getResources().getDisplayMetrics().scaledDensity * 14));
        a.recycle();

        if(dividerColor!=-1) {
            setDividerColor(this, dividerColor);
        }
    }

    public void setDividerColor(int dividerColor) {
        this.dividerColor = dividerColor;
        setDividerColor(this, dividerColor);
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public static void setDividerColor(android.widget.NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = android.widget.NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }


    private int indexOf(int id, int[] ids) {
        for (int i = 0; i < ids.length; i++) {
            if (ids[i] == id) {
                return i;
            }
        }
        throw new RuntimeException("id " + id +  " not in ids");
    }

    @Override
    public void addView(View child) {
        super.addView(child);
        updateView(child);
    }

    @Override
    public void addView(View child, int index, android.view.ViewGroup.LayoutParams params) {
        super.addView(child, index, params);
        updateView(child);
    }

    @Override
    public void addView(View child, android.view.ViewGroup.LayoutParams params) {
        super.addView(child, params);
        updateView(child);
    }

    private void updateView(View view) {
        if(view instanceof EditText){
            //((EditText) view).setTextSize(textSize);
            //((EditText) view).setTextColor(textColor);
        }
    }

}
