package se.davison.travelbot.ui.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.*;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;

import se.davison.travelbot.R;


public class ProgressDialogFragment extends BaseDialogFragment {

    public static final String FRAGMENT_TAG = "progress_dialog_fragment_tag";
    private static final String EXTRA_CANCEL_TEXT = "extra_cancel_text";
    private static final String EXTRA_CANCELABLE = "extra_cancelable";
    private static final String EXTRA_MAX_VALUE = "extra_max";

    private ProgressDialog progressDialog;
    private Integer max;

    public static class ProgressEvent {
        public float progress;
        public String tag;

        public ProgressEvent(float progress){
            this.progress = progress;
        }
    }

    public static class CanceledEvent extends DialogEvent {

        public CanceledEvent(String tag, int result) {
            super(tag, result);
        }
    }

    public static class Builder extends BaseDialogFragment.Builder {

        private boolean cancelable = false;
        private String cancelText;
        private Integer max;


        public Builder(DialogCallback callback) {
            super(callback);
        }


        public Builder cancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }

        public Builder cancelText(String cancelText) {
            this.cancelText = cancelText;
            return this;
        }

        public Builder max(Integer max) {
            this.max = max;
            return this;
        }

        public ProgressDialogFragment show() {
            String dialogTag = tag == null ? FRAGMENT_TAG : tag;
            ProgressDialogFragment dialog = ProgressDialogFragment.newInstance(dialogTag, message, title, cancelable, cancelText, max);
            if(fragment!=null){
                dialog.setTargetFragment(fragment,0);
            }
            dialog.show(fm);
            return dialog;
        }
    }


    public static ProgressDialogFragment newInstance(String tag, String message, String title, boolean cancelable, String cancelText, Integer max) {
        ProgressDialogFragment dialogFragment = new ProgressDialogFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_CANCEL_TEXT, cancelText);
        args.putBoolean(EXTRA_CANCELABLE, cancelable);
        if (max == null) {
            max = -1;
        }
        args.putInt(EXTRA_MAX_VALUE, max);
        dialogFragment.setArguments(tag, message, title, args);
        return dialogFragment;
    }

    public void setProgress(float progressPercent){
        if (progressDialog != null && max != null) {
            int progress = (int) ((float) max * progressPercent);
            progressDialog.setProgress(progress);
        }
    }

    static int resolveDialogTheme(Context context, int resid) {
        if (resid >= 0x01000000) {   // start of real resource IDs.
            return resid;
        } else {
            TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.alertDialogTheme, outValue, true);
            return outValue.resourceId;
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();
        String title = args.getString(EXTRA_TITLE);
        String message = args.getString(EXTRA_MESSAGE);
        String cancelText = args.getString(EXTRA_CANCEL_TEXT);

        boolean cancelable = args.getBoolean(EXTRA_CANCELABLE, true);

        max = args.getInt(EXTRA_MAX_VALUE);
        if (max == -1) {
            max = null;
        }

        progressDialog = new ProgressDialog(getActivity(),resolveDialogTheme(getContext(),0));

        if (title != null) {
            progressDialog.setTitle(title);
        }

        progressDialog.setMessage(message);
        if (max != null) {
            progressDialog.setProgress(0);
            progressDialog.setMax(max);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setIndeterminate(false);

        } else {
            progressDialog.setIndeterminate(true);

        }

        setCancelable(cancelable);
        if (cancelText != null) {
            progressDialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    postEvent(new CanceledEvent(getFragmentTag(),0));

                }
            });
            progressDialog.setButton(ProgressDialog.BUTTON_NEGATIVE, cancelText, new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    postEvent(new CanceledEvent(getFragmentTag(),0));
                }
            });
        }
        return progressDialog;
    }

}
