package se.davison.travelbot.ui;

import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.CallSuper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;

import se.davison.travelbot.R;

/**
 * Created by admin on 2015-08-07.
 */
public class ToolbarActivity extends AppCompatActivity {


    private Toolbar toolbarView;
    private int selectableItemBackground;
    private boolean fixEnabled = false;


    @Override
    @CallSuper
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setup();
    }

    protected void fixToolbarRipple(){

        TypedValue outValue = new TypedValue();
        getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        selectableItemBackground = outValue.resourceId;
        if(toolbarView!=null){


            toolbarView.post(new Runnable() {
                @Override
                public void run() {

                    if(isFinishing()){
                        return;
                    }

                    int childCount = toolbarView.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View view = toolbarView.getChildAt(i);
                        if (view instanceof ImageButton) {
                            view.setBackgroundResource(selectableItemBackground);
                        }
                        if (view instanceof ViewGroup) {
                            ViewGroup actionItems = (ViewGroup) view;
                            int actionCount = actionItems.getChildCount();
                            for (int ii = 0; ii < actionCount; ii++) {
                                actionItems.getChildAt(ii).setBackgroundResource(selectableItemBackground);
                            }
                        }
                    }
                }
            });
        }
    }

    private void setup() {

        toolbarView = (Toolbar)findViewById(R.id.toolbar);
        if (toolbarView == null)
        {

            throw new IllegalStateException ("have you forgot to include the toolbar in your layout? Add a view with id toolbar");
        }

        setSupportActionBar(toolbarView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



    public Toolbar getToolbar(){
        return toolbarView;
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        setup();

    }
}
