package se.davison.travelbot.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import se.davison.travelbot.R;

/**
 * Created by richard on 06/11/15.
 */
public class MoreFragment extends Fragment {

    private static class MenuItem{
        public String title;
        public Class clazz;

        //FIXME change to extends Activity
        public MenuItem(String title, Class<? extends Integer> clazz) {
            this.title = title;
            this.clazz = clazz;
        }
    }

    private List<MenuItem> menuItems = new ArrayList<>(10);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);


        menuItems.add(new MenuItem(getString(R.string.vicinity), Integer.class));
        menuItems.add(new MenuItem(getString(R.string.traffic_situation),Integer.class));
        menuItems.add(new MenuItem(getString(R.string.settings),Integer.class));
        menuItems.add(new MenuItem(getString(R.string.about),Integer.class));



        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
