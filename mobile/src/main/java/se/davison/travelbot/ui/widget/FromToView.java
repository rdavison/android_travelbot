package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import se.davison.travelbot.R;
import se.davison.travelbot.model.Stop;

/**
 * Created by richard on 25/02/16.
 */
public class FromToView extends LinearLayout {

    private int arrowDrawableResource;
    private int textColorSecondary;
    private int textColor;
    private ViewHolder left;
    private ViewHolder right;
    private ImageView imgArrow;

    private int stopDrawableResource;
    private int addressDrawableResource;

    private Stop from;
    private Stop to;



    public void setStops(Stop from, Stop to){
        this.from = from;
        this.to = to;

        setStop(left,from);
        setStop(right,to);
    }

    private void setStop(ViewHolder holder, Stop stop) {
        holder.stop.setText(stop.getName());

        int drawable = stop.getStopType() == Stop.TYPE_STOP ? stopDrawableResource : addressDrawableResource;


        holder.info.setCompoundDrawablesWithIntrinsicBounds(
                drawable, //left
                0, //top
                0, //right
                0);//bottom

        holder.info.setText(stop.getDistrict());
    }


    public FromToView(Context context){
        this(context,null);
    }

    public FromToView(Context context, AttributeSet attrs){
        this(context, attrs,0);
    }

    public FromToView(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs,0);

        inflate(context, R.layout.view_from_to,this);

        imgArrow = (ImageView)findViewById(R.id.img_arrow);



        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.FromToView,
                0, 0);


        try {
            textColor = a.getColor(R.styleable.FromToView_textColor, 0xFF000000);
            textColorSecondary = a.getColor(R.styleable.FromToView_textColorSecondary, 0x8A000000);
            arrowDrawableResource = a.getResourceId(R.styleable.FromToView_srcArrow,0);
            stopDrawableResource = a.getResourceId(R.styleable.FromToView_srcStop,0);
            addressDrawableResource = a.getResourceId(R.styleable.FromToView_srcAddress,0);
        } finally {
            a.recycle();
        }

        left = new ViewHolder(findViewById(R.id.cnt_left));
        left.title.setText(getResources().getString(R.string.from));
        right = new ViewHolder(findViewById(R.id.cnt_right));
        right.title.setText(getResources().getString(R.string.to));

        setArrowDrawableResource(arrowDrawableResource);


        setTextColors(textColor,textColorSecondary);

        int dp88 = (int) (getResources().getDisplayMetrics().density*88);

        setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,dp88));


    }

    public void setArrowDrawableResource(int arrowDrawableResource) {
        this.arrowDrawableResource = arrowDrawableResource;
        imgArrow.setImageResource(arrowDrawableResource);
    }

    public void setStopDrawableResource(int stopDrawableResource) {
        this.stopDrawableResource = stopDrawableResource;
    }

    public void setAddressDrawableResource(int addressDrawableResource) {
        this.addressDrawableResource = addressDrawableResource;
    }

    public void setTextColors(int textColor, int textColorSecondary) {
        this.textColor = textColor;
        this.textColorSecondary = textColorSecondary;

        setTextColor(left, textColor, textColorSecondary);
        setTextColor(right, textColor, textColorSecondary);

    }

    private void setTextColor(ViewHolder holder, int textColor, int textColorSecondary) {
        holder.title.setTextColor(textColorSecondary);
        holder.info.setTextColor(textColor);
        holder.stop.setTextColor(textColor);
    }

    private static class ViewHolder{

        TextView title;
        TextView info;
        TextView stop;

        public ViewHolder(View view){
            this.title = (TextView)view.findViewById(R.id.txt_title);
            this.info = (TextView)view.findViewById(R.id.txt_info);
            this.stop = (TextView)view.findViewById(R.id.txt_stop);
        }
    }
}
