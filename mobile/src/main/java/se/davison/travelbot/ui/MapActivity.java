package se.davison.travelbot.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import me.tatarka.rxloader.RxLoaderManager;
import me.tatarka.rxloader.RxLoaderObserver;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import se.davison.travelbot.R;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.model.TransportMode;
import se.davison.travelbot.net.JourneyDetailResponse;
import se.davison.travelbot.net.RestClient;
import se.davison.travelbot.ui.fragments.JourneyDetailFragment;
import se.davison.travelbot.ui.widget.SnappingBottomSheetBehavior;
import se.davison.travelbot.util.LUtils;
import se.davison.travelbot.util.MapAdjust;
import se.davison.travelbot.util.SerializableSaveCallback;
import se.davison.travelbot.util.RxUtils;
import se.davison.travelbot.util.Utils;


/**
 * Created by richard on 06/03/16.
 */
public class MapActivity extends ToolbarActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {
	private static final String EXTRA_JOURNEY = "extra_journey";
	private static final String TAG = MapActivity.class.getSimpleName();
	private static final String TAG_JOURNEY_DETAIL = "tag_journey_detail";
	
	
	private int peekHeight;
	private View bottomSheet;
	private SnappingBottomSheetBehavior<View> behavior;
	private int snappedHeight;
	private CoordinatorLayout coordinatorLayout;
	private GoogleMap map;
	private RxLoaderManager loaderManager;
	private Observable<?> observable;
	private RxLoaderObserver<?> observer;
	private RxUtils.OperatorCountDownLatch delayOperator = new RxUtils.OperatorCountDownLatch(1);
	private int lineWidth;
	private LatLngBounds bounds;
	private int screenWidth;
	private int screenHeight;
	private int mapInitHeight;
	private int statusBarHeight;
	private int dp16;
	
	private Interpolator fastOutSlowInInterpolator;
	private JourneyDetailFragment journeyDetailFragment;
	private boolean clickedMap = false;
	private Bitmap walkMarkerIcon;
	private Bitmap stopMarkerIcon;
	private LinkedList<Marker> markers = new LinkedList<>();
	private float scaledDensity;
	
	public static void startActivity(Context context, Journey journey) {
		
		Intent intent = new Intent(context, MapActivity.class);
		intent.putExtra(EXTRA_JOURNEY, journey);
		context.startActivity(intent);
		
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		loaderManager = RxLoaderManager.get(this);
		
		fastOutSlowInInterpolator = LUtils.loadInterpolatorWithFallback(this, android.R.interpolator.fast_out_slow_in, android.R.interpolator.decelerate_cubic);
		
		
		setContentView(R.layout.activity_map);
		
		coordinatorLayout = (CoordinatorLayout) findViewById(R.id.cnt_root);
		
		
		//AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.cnt_app_bar);
		
		
		final Toolbar toolbar = getToolbar();
		//noinspection RestrictedApi
		getSupportActionBar().setShowHideAnimationEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setTitle(R.string.route);
		
		statusBarHeight = Utils.getStatusBarHeight(this);
		int navigationBarHeight = Utils.getNavigationBarHeight(this);
		
		
		//ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toolbar.getLayoutParams();
		//params.setMargin = statusBarHeight;
		//toolbar.setLayoutParams(params);
		toolbar.setPadding(toolbar.getPaddingLeft(), toolbar.getPaddingTop() + statusBarHeight, toolbar.getPaddingRight(), toolbar.getPaddingBottom());
		
		
		final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		
		scaledDensity = metrics.scaledDensity;
		
		dp16 = (int) (metrics.density * 16f);
		
		screenWidth = metrics.widthPixels;
		screenHeight = metrics.heightPixels;
		mapInitHeight = (int) (((float) screenWidth) * 0.5625f);
		
		snappedHeight = mapInitHeight;
		peekHeight = navigationBarHeight + (int) (metrics.density * 56f);
		
		walkMarkerIcon = createMarkerIcon(walkMarkerIcon, R.drawable.ic_black_dot);
		stopMarkerIcon = createMarkerIcon(stopMarkerIcon, R.drawable.ic_stop_marker);
		
		
		bottomSheet = findViewById(R.id.cnt_bottom_sheet);
		bottomSheet.setClickable(true);
		behavior = SnappingBottomSheetBehavior.from(bottomSheet);
		behavior.setBottomSheetCallback(new SnappingBottomSheetBehavior.BottomSheetCallback() {
			
			private int settlingCount = 0;
			
			@Override
			public void onStateChanged(@NonNull View bottomSheet, int newState) {
				Log.d(TAG, "STATE = " + newState);
				
				map.getUiSettings().setAllGesturesEnabled(newState == SnappingBottomSheetBehavior.STATE_COLLAPSED);
				
				switch (newState) {
					case SnappingBottomSheetBehavior.STATE_COLLAPSED:
					case SnappingBottomSheetBehavior.STATE_EXPANDED:
					case SnappingBottomSheetBehavior.STATE_SNAPPED:
						
						break;
					
				}
				
				switch (newState) {
					case SnappingBottomSheetBehavior.STATE_SNAPPED:
						bottomSheet.setOnClickListener(null);
						zoomToRoute(true);
						toggleToolbar(true);
						break;
					case SnappingBottomSheetBehavior.STATE_COLLAPSED:
						toggleToolbar(true);
						if (!clickedMap) {
							zoomToRoute(false);
						}
						break;
					case SnappingBottomSheetBehavior.STATE_EXPANDED:
						toggleToolbar(false);
						break;
					
				}
				clickedMap = false;
			}
			
			@Override
			public void onSlide(@NonNull View bottomSheet, float slideOffset) {
				if (journeyDetailFragment != null) {
					journeyDetailFragment.setMargin(behavior.getCurrentOffset());
				}
			}
		});
		
		//16:9 aspect ratio
		behavior.setPeekHeight(peekHeight);
		behavior.setSnapHeight(snappedHeight);
		behavior.setState(SnappingBottomSheetBehavior.STATE_SNAPPED);
		
		lineWidth = (int) (8f * metrics.density);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			
			if (extras.containsKey(EXTRA_JOURNEY)) {
				
				final Journey journey = (Journey) extras.getSerializable(EXTRA_JOURNEY);
				
				
				if (savedInstanceState == null) {
					journeyDetailFragment = JourneyDetailFragment.newInstance(journey);
					getSupportFragmentManager().beginTransaction().add(R.id.cnt_bottom_sheet, journeyDetailFragment, TAG_JOURNEY_DETAIL).commit();
				} else {
					journeyDetailFragment = (JourneyDetailFragment) getSupportFragmentManager().findFragmentByTag(TAG_JOURNEY_DETAIL);
				}
				
				
				handleIntentData(RestClient.getInstance().journeyDetails(journey).lift(delayOperator), new RxLoaderObserver<ArrayList<JourneyDetailResponse>>() {
					
					@Override
					public void onStarted() {
						int hejsan = 0;
					}
					
					@Override
					public void onNext(ArrayList<JourneyDetailResponse> responses) {
						
					}
					
					@Override
					public void onCompleted() {
						LatLngBounds.Builder builder = new LatLngBounds.Builder();
						
						
						for (Leg leg : journey.getLegs()) {
							JourneyDetailResponse journeyDetail = leg.getJourneyDetail();
							if (journeyDetail != null) {
								
								List<Stop> stops = journeyDetail.getStops();
								
								if (leg.getTransportMode() != TransportMode.WALK) {
									
									PolylineOptions rectLine = new PolylineOptions().width(lineWidth).color(journeyDetail.getColorData().getFgColor()).zIndex(0);
									
									
									for (Stop stop : stops) {
										if (!stop.isIncluded()) {
											continue;
										}
										
										Log.d(TAG, stop.getName());
										LatLng point = stop.getLocation();
										builder.include(point);
										rectLine.add(point);
										
										//LatLng offsetPoint  = new LatLng(point.latitude-0.001,point.longitude-0.001);
										
										
										markers.add(map.addMarker(new MarkerOptions().zIndex(10).anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromBitmap(stopMarkerIcon)).position(point).snippet(stop.getName())));
									}
									Polyline polyline = map.addPolyline(rectLine);
								} else {
									
									int stopsPerDistance = 16;
									
									
									LatLng origin = stops.get(0).getLocation();
									LatLng destination = stops.get(stops.size() - 1).getLocation();
									
									
									double distance = Math.sqrt(Math.pow(origin.latitude - origin.longitude, 2) + Math.pow(destination.latitude - destination.longitude, 2));
									
									Log.d(TAG, "ORIGIN = " + origin.toString());
									Log.d(TAG, "DESTINATION = " + destination.toString());
									Log.d(TAG, "DISTANCE = " + distance);
									
									double latDiff = origin.latitude - destination.latitude;
									double lngDiff = origin.longitude - destination.longitude;
									
									int count = (int) Math.ceil(distance / stopsPerDistance);
									
									for (int i = 1; i < count; i++) {
										
										
										LatLng position = new LatLng(origin.latitude - ((latDiff / count) * i), origin.longitude - ((lngDiff / count) * i));
										
										map.addMarker(new MarkerOptions()
												.position(position)
												.icon(BitmapDescriptorFactory.fromBitmap(walkMarkerIcon)));
										
										//map.addCircle(new CircleOptions().zIndex(1).center(position).radius(lineWidth/8).fillColor(journeyDetail.getColorData().getFgColor()));
										
									}
									
									
								}
							}
						}
						
						bounds = builder.build();
						
						Location loc1 = new Location("dummy");
						loc1.setLatitude(bounds.northeast.latitude);
						loc1.setLongitude(bounds.northeast.longitude);
						
						Location loc2 = new Location("dummy");
						loc2.setLatitude(bounds.southwest.latitude);
						loc2.setLongitude(bounds.southwest.longitude);
						
						float bearing = loc2.bearingTo(loc1);
						
						zoomToRoute(true);
						
						
					}
					
					@Override
					public void onError(Throwable e) {
						e.printStackTrace();
					}
				});
			}
		}
		
		
	}
	
	private Bitmap createMarkerIcon(Bitmap bitmap, int res) {
		Drawable drawable = ContextCompat.getDrawable(this, res);
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
		bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		
		Canvas canvas = new Canvas(bitmap);
		drawable.draw(canvas);
		
		return bitmap;
	}
	
	public void setPeekHeight(int peekHeight) {
		this.peekHeight = peekHeight;
		behavior.setPeekHeight(peekHeight);
	}
	
	private void toggleToolbar(boolean show) {
		Toolbar toolbar = getToolbar();
		toolbar.animate().translationY(show ? 0 : -toolbar.getHeight()).alpha(show ? 1 : 0).setInterpolator(fastOutSlowInInterpolator).setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).start();
	}
	
	private LatLng offsetLatLongByPixels(LatLng target, float zoom, int offsetX, int offsetY) {
		
		float oldZoom = map.getCameraPosition().zoom;
		LatLng oldTarget = map.getCameraPosition().target;
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(target, zoom));
		
		double scale = Math.pow(2, zoom);
		
		Point point = map.getProjection().toScreenLocation(target);
		
		point.x += offsetX;
		point.y += offsetY;
		
		LatLng newLatLng = map.getProjection().fromScreenLocation(point);
		
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(oldTarget, oldZoom));
		
		return newLatLng;
	}
	
	private <T extends Serializable> void handleIntentData(Observable<T> observable, RxLoaderObserver<T> rxLoaderObserver) {
		loaderManager.create(observable.subscribeOn(Schedulers.newThread())
				.observeOn(AndroidSchedulers.mainThread()), rxLoaderObserver).save(new SerializableSaveCallback<T>()).start();
	}
	
	@Override
	public void onMapReady(GoogleMap map) {
		delayOperator.countDown();
		this.map = map;
		
		map.setOnMapClickListener(this);
		map.setOnMarkerClickListener(this
		);
		map.getUiSettings().setAllGesturesEnabled(false);
	}
	
	private void zoomToRoute(boolean collapsed) {
		
		
		
		if (delayOperator.isReleased()) {
			
			int viewHeight = collapsed ? mapInitHeight-statusBarHeight : (int) ((screenHeight - (peekHeight - statusBarHeight)));
			viewHeight -= (dp16 * 2);
			
			int offsetY = ((screenHeight - viewHeight) / 2)-dp16;
			
			CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, screenWidth - (dp16 * 2), viewHeight,0);
			
			float oldZoom = map.getCameraPosition().zoom;
			LatLng oldTarget = map.getCameraPosition().target;
			
			map.moveCamera(cu);
			
			float zoom = map.getCameraPosition().zoom;
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(oldTarget, oldZoom));
			
			LatLng target = offsetLatLongByPixels(bounds.getCenter(), zoom, 0, offsetY);
			CameraPosition cp = new CameraPosition.Builder()
					.target(target)
					.zoom(zoom)
					.build();
			
			
			map.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
		}
		
	}
	
	@Override
	public void onMapClick(LatLng latLng) {
		
		
		behavior.setState(SnappingBottomSheetBehavior.STATE_COLLAPSED);
		
		bottomSheet.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				behavior.setState(SnappingBottomSheetBehavior.STATE_SNAPPED);
			}
		});
		
		clickedMap = true;
		
		
		map.getUiSettings().setAllGesturesEnabled(true);
		zoomToRoute(false);
	}
	
	@Override
	public boolean onMarkerClick(Marker marker) {
		for (Marker m : markers) {
			if (m != marker) {
				marker.hideInfoWindow();
			}
		}
		marker.showInfoWindow();
		return true;
	}
}
