package se.davison.travelbot.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import se.davison.travelbot.R;

/**
 * Created by richard on 01/08/16.
 */
public class JourneyView extends View {

    protected final float dp;


    public JourneyView(Context context) {
        this(context,null);
    }

    public JourneyView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public JourneyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        dp = displayMetrics.density;
        init(context,attrs,defStyleAttr,0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public JourneyView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        dp = displayMetrics.density;
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {



    }
}
