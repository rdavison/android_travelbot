package se.davison.travelbot.ui.fragments;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.smartrecycleradapter.OnItemClickListener;
import se.davison.smartrecycleradapter.OneLineAdapterItem;
import se.davison.smartrecycleradapter.ProgressAdapterItem;
import se.davison.smartrecycleradapter.SectionAdapter;
import se.davison.smartrecycleradapter.SectionAdapterItem;
import se.davison.travelbot.R;
import se.davison.travelbot.adapters.ViewPagerAdapter;
import se.davison.travelbot.adapters.adapteritems.StopAdapterItem;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.net.LocationResponse;
import se.davison.travelbot.net.RestClient;
import se.davison.travelbot.ui.DepartureBoardActivity;
import se.davison.travelbot.ui.MainActivity;
import se.davison.travelbot.util.ExtendedSharedPreferences;
import se.davison.travelbot.util.LocationException;
import se.davison.travelbot.util.Utils;



/**
 * Created by richard on 04/11/14.
 */
public class NextTripFragment extends RecyclerViewFragment<LocationResponse> implements PopupMenu.OnMenuItemClickListener,ViewPagerAdapter.OnPageSelectedListener {


    private static final String TAG = NextTripFragment.class.getSimpleName();
    private static final int MAX_STOP_COUNT = 10;

    private ArrayList<Stop> favorites;
    private SectionAdapter adapter;
    private int nearbySectionIndex;
    private String lat;
    private String lng;

    private AdapterView.OnItemClickListener overflowClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PopupMenu popup = new PopupMenu(view.getContext(), view);
            MenuInflater inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.actions_stop, popup.getMenu());
            popup.setOnMenuItemClickListener(NextTripFragment.this);
            popup.show();
        }
    };
    private String errorString;

    @Override
    public void onPause() {
        super.onPause();
    }

    public static NextTripFragment newInstance(){
        return new NextTripFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setRetainInstance(true);



        super.onCreate(savedInstanceState);
    }



    @Override
    protected void setup(View container, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout) {

        int horizontalMargin = getResources().getDimensionPixelSize(R.dimen.card_wrap_horizontal_margin);




        container.setPadding(0, Utils.dpToPx(getContext(), 56 + 48) + Utils.getStatusBarHeight(getContext()), 0, 0);
        recyclerView.setBackgroundResource(R.drawable.card_inset);







        adapter = new SectionAdapter(getContext());

        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup viewGroup, RecyclerView.ViewHolder viewHolder, View view, int position) {
                DepartureBoardActivity.startActivity(getActivity(), ((StopAdapterItem) adapter.getItem(position)).getStop(),((StopAdapterItem.ViewHolder)viewHolder).txtName);
            }

        });


        int dp8 = Utils.dpToPx(getContext(), 8);

        recyclerView.setPadding(horizontalMargin, dp8, horizontalMargin, Utils.getNavigationBarHeight(getContext()) + dp8);



        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.attachTo(recyclerView);
    }

    @Override
    public void onResume() {
        super.onResume();



        favorites = new ArrayList<Stop>(ExtendedSharedPreferences.from(getContext()).getList(MainActivity.PREF_KEY_FAVORITE_STOPS, new ArrayList<Stop>(20), Stop.class));
        ArrayList<AdapterItem> adapterItems = new ArrayList<>(100);
        if(favorites.size()>0) {
            adapterItems.add(new SectionAdapterItem(getString(R.string.favorites)));

            for (Stop s : favorites) {
                adapterItems.add(new StopAdapterItem(s,overflowClickListener));
            }
        }


        adapterItems.add(new SectionAdapterItem(getString(R.string.nearby)));

        adapterItems.add(new ProgressAdapterItem(getContext()).loading());
        adapter.setItems(adapterItems);
        adapter.notifyDataSetChanged();
        nearbySectionIndex = adapter.getSectionPosition(adapterItems.size()-1);

        if(errorString != null){
            showErrorItem(errorString);
        }

    }

    @Override
    protected boolean showFirstLoadingIndicator() {
        return false;
    }

    @Override
    protected void onDataRefresh() {
        Activity activity = getActivity();
        if(activity instanceof MainActivity){
            ((MainActivity)activity).updateLocation();
        }
    }

    @Override
    public boolean onError(Throwable ex) {
        errorString = getString(R.string.err_nearby_stops);
        if (ex instanceof LocationException){
            errorString = getString(R.string.err_location_not_found);
        }else if(ex instanceof SecurityException){
            errorString = getString(R.string.err_permission_denied);
        }

        if(isResumed()){
            showErrorItem(errorString);
        }

        return false;
    }

    public void showErrorItem(String message) {

        ArrayList<AdapterItem> adapterItems = adapter.getItems();
        adapter.clearSection(nearbySectionIndex, false);

        adapterItems.add(new OneLineAdapterItem(message, ContextCompat.getColor(getContext(), R.color.text_color_secondary), null));
        adapter.setItems(adapterItems);
        adapter.notifyDataSetChanged();

    }

    @Override
    public String getErrorString(Throwable ex) {
        return "Error";
    }

    @Override
    public String getEmptyString() {
        return "Empty string";
    }

    @Override
    public Observable<LocationResponse> mainObservable() {
        return RestClient.service().nearbyStops(lat,lng, MAX_STOP_COUNT);
    }



    @Override
    protected void onComplete(LocationResponse locationResponse) {
        errorString = null;
        ArrayList<AdapterItem> adapterItems =  adapter.getItems();
        adapter.clearSection(nearbySectionIndex, false);

        for(Stop s : locationResponse.getStops()){
            if(s.getTrack() == null){
                adapterItems.add(new StopAdapterItem(s,overflowClickListener));
            }
        }

        adapter.setItems(adapterItems);
        adapter.notifyDataSetChanged();
    }

    public void setLocation(Location location) {
        this.lat = String.valueOf(location.getLatitude());
        this.lng =  String.valueOf(location.getLongitude());
        loadData();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public void onPageSelected() {
        onDataRefresh();
    }
}
