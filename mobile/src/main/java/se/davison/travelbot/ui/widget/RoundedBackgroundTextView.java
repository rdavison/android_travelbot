package se.davison.travelbot.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import se.davison.travelbot.R;

/**
 * Created by richard on 02/08/16.
 */
public class RoundedBackgroundTextView extends TextView {
    private int radius;
    private int color;

    public RoundedBackgroundTextView(Context context) {
        this(context,null);
    }

    public RoundedBackgroundTextView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RoundedBackgroundTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs,defStyleAttr,0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public RoundedBackgroundTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs,defStyleAttr,defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {

        paint.setStyle(Paint.Style.FILL);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.RoundedBackgroundTextView,
                0, 0);
        radius = a.getDimensionPixelSize(R.styleable.RoundedBackgroundTextView_radius, 0);

        color = ((ColorDrawable)getBackground()).getColor();
        super.setBackgroundColor(0);
        paint.setColor(color);

    }

    @Override
    public void setBackgroundColor(int color) {
        super.setBackgroundColor(0);
        this.color = color;
        paint.setColor(color);

    }

    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private RectF rect = new RectF();

    @Override
    protected void onDraw(Canvas canvas) {
        rect.right = getWidth();
        rect.bottom = getHeight();
        canvas.drawRoundRect(rect,radius,radius,paint);
        super.onDraw(canvas);

    }
}
