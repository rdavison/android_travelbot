package se.davison.travelbot.ui.fragments;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.TreeSet;

import rx.Observable;
import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.smartrecycleradapter.OnItemClickListener;
import se.davison.smartrecycleradapter.SectionAdapter;
import se.davison.travelbot.R;
import se.davison.travelbot.adapters.adapteritems.DepartureHeaderAdapterItem;
import se.davison.travelbot.adapters.adapteritems.DepartureAdapterItem;
import se.davison.travelbot.adapters.adapteritems.MapTileAdapterItem;
import se.davison.travelbot.model.TimedDeparture;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.net.NextTripResponse;
import se.davison.travelbot.net.RestClient;
import se.davison.travelbot.util.Utils;

/**
 * Created by admin on 2015-08-23.
 */
public class DepartureBoardFragment extends RecyclerViewFragment<NextTripResponse> {

    private static final String EXTRA_STOP = "extra_stop";
    private static final String TAG = DepartureBoardFragment.class.getSimpleName();
    private Stop stop;
    //private DepartureAdapter departureAdapter;
    private SectionAdapter adapter;
    private String strAllDepartures;
    private String strDeparturesFrom;
    private String strDepartures;

    private String strNow;

    private ArrayMap<String, TreeSet<TimedDeparture>> groups;
    private boolean departuresGrouped;
    private NextTripResponse nextTripResponse;
    private int width;
    private int dpMapHeight;
    private int dp16;
    private int navigationBarHeight;
    private int dp8;
    private int dp64;


    public static DepartureBoardFragment newInstance(Stop stop) {
        DepartureBoardFragment fragment = new DepartureBoardFragment();
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_STOP, stop);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void setup(View container, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout) {

        DisplayMetrics metrics = getResources().getDisplayMetrics();

        dpMapHeight = getResources().getDimensionPixelSize(R.dimen.map_item_height);
        int horizontalMargin = getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
        navigationBarHeight = Utils.getNavigationBarHeight(getContext());
        dp16 = (int)(metrics.density*16);
        dp8= (int)(metrics.density*8);
        dp64 = (int)(metrics.density*64);

        width = metrics.widthPixels;

        recyclerView.setPadding(0, dp8, 0, 0);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setClipToPadding(false);

        adapter = new SectionAdapter(getContext());
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup viewGroup, RecyclerView.ViewHolder viewHolder, View view, int position) {
                AdapterItem item = adapter.getItem(position);
                Log.d(TAG, "we are in here!");
                if(item instanceof MapTileAdapterItem){

                }
                if(item instanceof DepartureAdapterItem){

                }
            }
        });

        adapter.setSeparator(0x1f000000, horizontalMargin+dp64, horizontalMargin);
        adapter.attachTo(recyclerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stop = (Stop) getArguments().getSerializable(EXTRA_STOP);

        strAllDepartures = getString(R.string.all_departures);
        strDepartures = getString(R.string.departures);
        strDeparturesFrom = getString(R.string.departures_from);
        strNow = getString(R.string.now);
    }

    @Override
    public String getErrorString(Throwable ex) {
        if(ex instanceof RestClient.ServiceException){
            return ex.getMessage();
        }
        return getString(R.string.err_fetch_stops);
    }

    @Override
    public String getEmptyString() {
        return getString(R.string.no_departures);
    }

    @Override
    public Observable<NextTripResponse> mainObservable() {
        return RestClient.service().nextTrip(stop.getId());
    }

    @Override
    public boolean refreshOnScroll() {
        return false;
    }

    @Override
    protected void onCompleteBackground(NextTripResponse nextTripResponse) {


        groups = TimedDeparture.group(nextTripResponse.getDepartures());
        this.nextTripResponse = nextTripResponse;

    }

    @Override
    protected void onComplete(NextTripResponse nextTripResponse) {
        if(nextTripResponse.getDepartures().size()==0){
            toggleList(false);
        }else{


            ArrayList<AdapterItem> adapterItems = new ArrayList<>(100);

            if(departuresGrouped) {

                for (int i = 0; i < groups.size(); i++) {
                    String track = groups.keyAt(i);
                    TreeSet<TimedDeparture> departures = groups.get(track);
                    String header = track!=null?strDeparturesFrom + " " + track:strDepartures;
                    adapterItems.add(new DepartureHeaderAdapterItem(header, true));
                    for (TimedDeparture departure : departures) {
                        departure.showTrack(false);
                        adapterItems.add(new DepartureAdapterItem(departure, strNow));
                    }
                }

            }else{
                adapterItems.add(new DepartureHeaderAdapterItem(strAllDepartures));


                for (TimedDeparture departure : nextTripResponse.getDepartures()){
                    departure.showTrack(true);
                    adapterItems.add(new DepartureAdapterItem(departure, strNow));
                }
            }

            adapterItems.add(new MapTileAdapterItem(stop.getLatitude(), stop.getLongitude(), 15, dpMapHeight, width, 0, R.drawable.pushpin_stop,new Rect(0, dp16, 0, 0),null));

            adapter.setItems(adapterItems);
            //departureAdapter.setDepartures(nextTripResponse.getDepartures());
            adapter.notifyDataSetChanged();
        }


    }

    public void setDeparturesGrouped(boolean departuresGrouped, boolean refresh) {
        this.departuresGrouped = departuresGrouped;
        if(refresh&&nextTripResponse!=null) {
            onComplete(nextTripResponse);
        }
    }

    public boolean isDeparturesGrouped() {
        return departuresGrouped;
    }
}
