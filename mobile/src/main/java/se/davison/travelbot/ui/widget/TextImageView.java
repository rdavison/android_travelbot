package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import java.util.Arrays;

/**
 * Created by admin on 2015-08-13.
 */
public class TextImageView extends ImageView {
    private int paddingRight;
    private int paddingLeft;
    private Paint paint;
    private String text;

    public TextImageView(Context context) {
        super(context);
    }

    private int indexOf(int id, int[] ids) {
        for (int i = 0; i < ids.length; i++) {
            if (ids[i] == id) {
                return i;
            }
        }
        throw new RuntimeException("id " + id +  " not in ids");
    }


    public TextImageView(Context context, AttributeSet attrs) {
        super(context, attrs);


        int[] ids = { android.R.attr.text,  android.R.attr.textColor, android.R.attr.textSize};
        Arrays.sort(ids); // just sort the array

        TypedArray a = context.obtainStyledAttributes(attrs, ids);

        text = a.getString(indexOf(android.R.attr.text, ids));
        int textSize = a.getDimensionPixelSize(indexOf(android.R.attr.textSize,ids),(int) (getResources().getDisplayMetrics().scaledDensity*14));
        int textColor = a.getColor(indexOf(android.R.attr.textColor,ids),Color.WHITE);

        a.recycle();

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        paddingRight = 0;
        paddingLeft = 0;


    }

    public void setTextPadding(int paddingLeft, int paddingRight){
        this.paddingLeft = paddingLeft;
        this.paddingRight = paddingRight;
    }




    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(text!=null&&!text.isEmpty()){

            canvas.drawText(text,getPaddingLeft()+paddingLeft,getHeight()/2 - ((paint.descent() + paint.ascent()) / 2),paint);
        }

    }
}
