package se.davison.travelbot.ui.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;


public class ErrorDialog extends BaseDialogFragment {

    public static final int ALERT = 0;
    public static final int TOAST = 1;

    public static final String FRAGMENT_TAG = "error_dialog_fragment_tag";
    private static final String EXTRA_POST_CALLBACK = "extra_post_callback";


    public static class Builder extends BaseDialogFragment.Builder {

        private int level = ErrorDialog.ALERT;
        private Exception exception;

        public Builder(DialogCallback callback, int level) {
            super(callback);
            this.level = level;
        }

        public Builder exception(Exception e) {
            this.exception = e;
            return this;
        }

        public ErrorDialog show() {
            if (exception != null && message == null) {
                this.message = "";//Utils.exceptionToString(context, exception);
            }
            if (level == ALERT) {

                Log.d("ASFASfASf", "FINISHING??!?!" + Boolean.valueOf(((Activity) context).isFinishing()));
                String dialogTag = tag == null ? FRAGMENT_TAG : tag;
                ErrorDialog dialog = ErrorDialog.newInstance(dialogTag, message, title);
                if(fragment!=null){
                    dialog.setTargetFragment(fragment,0);
                }
                dialog.show(fm);
                return dialog;

            } else if (level == TOAST) {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                return null;
            } else {
                throw new IllegalArgumentException(
                        "level must be ErrorDialog.ALERT or ErrorDialog.TOAST");
            }
        }
    }

    private static ErrorDialog newInstance(String tag, String message, String title) {
        ErrorDialog dialogFragment = new ErrorDialog();
        Bundle args = new Bundle();
        dialogFragment.setArguments(tag, message, title, args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = getDefaultBuilder();

        builder.setPositiveButton("Ok", new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                postEvent(new DialogEvent(getFragmentTag(), 0));
            }
        });
        builder.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                postEvent(new DialogEvent(getFragmentTag(), 0));
            }
        });
        return builder.create();
    }
}
