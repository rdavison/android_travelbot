package se.davison.travelbot.ui.fragments.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by admin on 2015-08-28.
 */
public class AlertDialog extends BaseDialogFragment {

    public static final String FRAGMENT_TAG = "confirm_dialog_fragment_tag";
    private static final String EXTRA_CANCELABLE = "extra_cancelable";

    public static class Builder extends BaseDialogFragment.Builder{

        private boolean cancelable = true;

        public Builder(DialogCallback callback) {
            super(callback);
        }

        public Builder cancelable(boolean cancelable){
            this.cancelable = cancelable;
            return this;
        }

        @Override
        public DialogFragment show() {
            String dialogTag = tag == null ? FRAGMENT_TAG : tag;
            AlertDialog dialog = AlertDialog.newInstance(dialogTag,message, title,cancelable);
            if(fragment!=null){
                dialog.setTargetFragment(fragment,0);
            }
            dialog.show(fm);
            return dialog;
        }
    }

    private static AlertDialog newInstance(String tag, String message, String title, boolean cancelable) {
        AlertDialog dialogFragment = new AlertDialog();
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_CANCELABLE,cancelable);
        dialogFragment.setArguments(tag, message, title, args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        boolean cancelable = getArguments().getBoolean(EXTRA_CANCELABLE,true);

        android.support.v7.app.AlertDialog.Builder builder = getDefaultBuilder();
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                postEvent(new DialogEvent(getFragmentTag(), 1));
            }
        }).setCancelable(cancelable).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                postEvent(new DialogEvent(getFragmentTag(), 0));
            }
        });
        return builder.create();
    }
}
