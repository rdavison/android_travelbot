package se.davison.travelbot.ui;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.transition.TransitionInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import se.davison.travelbot.R;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.transition.ChangeText;
import se.davison.travelbot.ui.fragments.DepartureBoardFragment;
import se.davison.travelbot.util.ExtendedSharedPreferences;
import se.davison.travelbot.util.LUtils;
import se.davison.travelbot.util.ReverseInterpolator;
import se.davison.travelbot.util.Utils;

/**
 * Created by admin on 2015-08-23.
 */
public class DepartureBoardActivity extends ToolbarActivity implements AppBarLayout.OnOffsetChangedListener{

    private static final String TAG = DepartureBoardActivity.class.getSimpleName();

    private static final String PREF_KEY_GROUP_DEPARTURES = "pref_key_group_departures";
    private static final String EXTRA_STOP = "extra_stop";
    private static final String TAG_FRAGMENT = "tag_fragment";
    private static final String STATE_HERO_ID = "state_hero_id";
    private static final int[] HERO_IMAGES = new int[] {R.drawable.hero_departure_board_1,R.drawable.hero_departure_board_2,R.drawable.hero_departure_board_3,R.drawable.hero_departure_board_4};
    public static final int ANIMATION_DURATION = 200;


    private int heroId;
    private AppBarLayout appBarLayout;
    private DepartureBoardFragment fragment;
    private int index = 0;
    private ExtendedSharedPreferences prefs;
    private ArrayList<Stop> favorites;
    private FloatingActionButton fab;
    private boolean isFavorite = false;
    private boolean groupDepartures;
    private MenuItem groupMenuItem;
    private Interpolator fastOutSlowInInterpolator;
    private Interpolator reverseFastOutSlowInInterpolator;
    private AnimatedVectorDrawable drawableCompat;

    public static void startActivity(Activity activity, Stop stop, View sharedView){

        Intent intent = new Intent(activity, DepartureBoardActivity.class);
        intent.putExtra(EXTRA_STOP, (Serializable) stop);

        Bundle bundle  = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            //activity.getWindow().setExitTransition(new Explode());
            bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, sharedView, sharedView.getTransitionName()).toBundle();
            ChangeText.intent(intent, sharedView, sharedView.getTransitionName());
            //activity.getWindow().setEnterTransition(new Explode());
        }

        ActivityCompat.startActivity(activity, intent, bundle);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_HERO_ID, heroId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_departure_board);
        fixToolbarRipple();

        fastOutSlowInInterpolator = LUtils.loadInterpolatorWithFallback(this, android.R.interpolator.fast_out_slow_in, android.R.interpolator.decelerate_cubic);
        reverseFastOutSlowInInterpolator = new ReverseInterpolator(fastOutSlowInInterpolator);

        final Stop stop = (Stop) getIntent().getExtras().getSerializable(EXTRA_STOP);



        Toolbar toolbar = getToolbar();


        ImageView imgHero = (ImageView)findViewById(R.id.img_hero);
        appBarLayout = (AppBarLayout) findViewById(R.id.cnt_app_bar);
        fab = (FloatingActionButton)findViewById(R.id.fab);
        final TextView txtSharedTitle = (TextView)findViewById(R.id.txt_shared_title);
        txtSharedTitle.setText(stop.getName());
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) txtSharedTitle.getLayoutParams();
        params.topMargin -= Utils.getStatusBarHeight(this);
        txtSharedTitle.setLayoutParams(params);

        prefs = ExtendedSharedPreferences.from(this);
        groupDepartures = prefs.getBoolean(PREF_KEY_GROUP_DEPARTURES,true);
        favorites =  new ArrayList<Stop>(prefs.getList(MainActivity.PREF_KEY_FAVORITE_STOPS, new ArrayList<Stop>(20),Stop.class));

        if(favorites.contains(stop)){
            isFavorite = true;
            //fab.setImageResource(R.drawable.ic_star_border_24dp);
        }


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            drawableCompat = (AnimatedVectorDrawable) getDrawable(R.drawable.star_vector);
            fab.setImageDrawable(drawableCompat);
        }



        fab.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (isFavorite) {
                    favorites.remove(stop);
                } else {
                    favorites.add(stop);
                }

                Toast.makeText(DepartureBoardActivity.this, isFavorite ? R.string.favorite_removed : R.string.favorite_added, Toast.LENGTH_SHORT).show();

                if(drawableCompat!=null) {
                    drawableCompat.stop();
                    drawableCompat.start();
                }

                //Drawable animatable = ContextCompat.getDrawable(DepartureBoardActivity.this, isFavorite ? R.drawable.animated_vector_star_border : R.drawable.animated_vector_star);

                isFavorite = !isFavorite;

                //fab.getImage

                //fab.setImageDrawable(drawableCompat);
                //drawableCompat.stop();
                //drawableCompat.start();
                //((Animatable) animatable).start();

                prefs.edit().putList(MainActivity.PREF_KEY_FAVORITE_STOPS, favorites).apply();
            }
        });

        fab.post(new Runnable() {
            @Override
            public void run() {
                fab.setVisibility(View.GONE);
            }
        });


        //getSupportActionBar().setTitle(stop.getName());
        //getSupportActionBar().setSubtitle(stop.getDistrict());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_toolbar_back));


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent upIntent = NavUtils.getParentActivityIntent(DepartureBoardActivity.this);
                if (NavUtils.shouldUpRecreateTask(DepartureBoardActivity.this, upIntent)) {
                    TaskStackBuilder.create(DepartureBoardActivity.this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    NavUtils.navigateUpTo(DepartureBoardActivity.this, upIntent);
                }
            }
        });





        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(stop.getName());
        //collapsingToolbar.setExpandedTitleColor(0xFFFFFF);
        //collapsingToolbar.setCollapsedTitleTextColor(0xFFFFFF);





        FragmentManager fm = getSupportFragmentManager();

        if(savedInstanceState==null){

            if (fm.findFragmentByTag(TAG_FRAGMENT) == null) {
                fragment = DepartureBoardFragment.newInstance(stop);
                fm.beginTransaction().add(R.id.cnt_fragment, fragment, TAG_FRAGMENT).commit();
            }
            heroId = HERO_IMAGES[new Random().nextInt(HERO_IMAGES.length-1)];
        }else{
            heroId = savedInstanceState.getInt(STATE_HERO_ID);
            fragment = (DepartureBoardFragment) fm.findFragmentByTag(TAG_FRAGMENT);
        }

        fragment.setDeparturesGrouped(groupDepartures, false);

        imgHero.setImageResource(heroId);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(TransitionInflater.from(this)
                    .inflateTransition(R.transition.curve));
            getWindow().setSharedElementReturnTransition(TransitionInflater.from(this)
                    .inflateTransition(R.transition.curve));
            ChangeText.setup(this);
            getWindow().getReturnTransition().addListener(new LUtils.TransitionListenerAdapter() {
                /*@Override
                public void onTransitionStart(Transition transition) {
                    new LUtils.CircularRevealBuilder(appBarLayout).duration(400).withLayer().interpolator(reverseFastOutSlowInInterpolator).createAndStart();
                }*/
            });

            getWindow().getEnterTransition().addListener(new LUtils.TransitionListenerAdapter(){
                @Override
                public void onTransitionStart(Transition transition) {
                    appBarLayout.setVisibility(View.INVISIBLE);
                }

                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onTransitionEnd(Transition transition) {
                    appBarLayout.setVisibility(View.VISIBLE);
                    fab.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            fab.show();
                        }
                    },ANIMATION_DURATION);
                    getWindow().getEnterTransition().removeListener(this);
                    new LUtils.CircularRevealBuilder(appBarLayout).listener(new LUtils.CircularRevealListener() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            txtSharedTitle.setTextColor(0x00000000);
                        }
                    }).duration(ANIMATION_DURATION*2).withLayer().interpolator(fastOutSlowInInterpolator).createAndStart();
                }
            });
        } else {
            appBarLayout.setVisibility(View.VISIBLE);
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        appBarLayout.removeOnOffsetChangedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.departure_board, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        groupMenuItem = menu.findItem(R.id.menu_group_ungroup);
        if(groupDepartures){
            groupMenuItem.setIcon(R.drawable.ic_toolbar_ungroup);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_group_ungroup:
                if(groupDepartures) {
                    groupMenuItem.setIcon(R.drawable.ic_toolbar_group);
                }else{
                    groupMenuItem.setIcon(R.drawable.ic_toolbar_ungroup);
                }
                groupDepartures = !groupDepartures;
                prefs.edit().putBoolean(PREF_KEY_GROUP_DEPARTURES,groupDepartures).apply();
                fragment.setDeparturesGrouped(groupDepartures,true);
                break;
            case R.id.menu_refresh:
                fragment.loadData();
                break;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        index = i;

        if (i == 0) {
            fragment.setRefreshEnabled(true);
        } else {
            fragment.setRefreshEnabled(false);
        }

    }
}
