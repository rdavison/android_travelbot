package se.davison.travelbot.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.WindowInsets;
import android.widget.RelativeLayout;

/**
 * Created by richard on 19/05/16.
 */
public class InsetsRelativeLayout extends RelativeLayout {

    private boolean zeroInset = true;

    public InsetsRelativeLayout(Context context) {
        super(context);
    }

    public InsetsRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InsetsRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public InsetsRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setZeroInset(boolean zeroInset) {
        this.zeroInset = zeroInset;
    }

    @Override
    public WindowInsets onApplyWindowInsets(WindowInsets insets) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            return super.onApplyWindowInsets(insets.replaceSystemWindowInsets(0, 0, 0,zeroInset?0:insets.getSystemWindowInsetBottom()));
        } else {
            return insets;
        }
    }
}
