package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

import se.davison.travelbot.R;

/**
 * Created by admin on 2015-08-17.
 */
public class RadiusCardView extends CardView {

    public static final int ROUND_NONE = 0;
    public static final int ROUND_TOP = 1;
    public static final int ROUND_BOTTOM = 2;
    public static final int ROUND_BOTH = 3;




    private Paint paint;
    private Integer color;
    private float radius;
    private int cornerRadiusPolicy = ROUND_BOTH;

    public void setCornerRadiusPolicy(int cornerRadiusPolicy) {
        this.cornerRadiusPolicy = cornerRadiusPolicy;
    }

    public RadiusCardView(Context context) {
        super(context);
        init(context, null, 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        paint = new Paint();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CardView, defStyleAttr,
                R.style.CardView_Light);
        int backgroundColor = a.getColor(R.styleable.CardView_cardBackgroundColor, 0);
        radius = a.getDimension(R.styleable.CardView_cardCornerRadius, 0);
        paint.setColor(Color.RED);
        a.recycle();

    }

    public RadiusCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public RadiusCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        /*
        if(cornerRadiusPolicy!=ROUND_BOTH) {

            boolean hideTop = cornerRadiusPolicy == ROUND_BOTTOM || cornerRadiusPolicy == ROUND_NONE;
            boolean hideBottom =  cornerRadiusPolicy == ROUND_TOP || cornerRadiusPolicy == ROUND_NONE;


            if (true) {
                canvas.drawRect(0, 0, getWidth(), -radius, paint);
            }
            if (true) {
                canvas.drawRect(0, getHeight() - radius, getWidth(), getHeight(), paint);
            }
        }*/
    }
}
