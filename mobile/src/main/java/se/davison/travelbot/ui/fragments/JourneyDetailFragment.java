package se.davison.travelbot.ui.fragments;

import android.animation.ValueAnimator;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.Space;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;

import se.davison.travelbot.R;
import se.davison.travelbot.adapters.JourneyDetailAdapter;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.ui.MapActivity;
import se.davison.travelbot.ui.widget.JourneyInfoView;
import se.davison.travelbot.util.LUtils;
import se.davison.travelbot.util.Utils;

/**
 * Created by richard on 14/03/16.
 */
public class JourneyDetailFragment extends Fragment {
	private static final String EXTRA_JOURNEY = "extra_journey";
	private static final String TAG = JourneyDetailFragment.class.getSimpleName();
	private Journey journey;
	private JourneyInfoView journeyInfoView;
	private float statusBarHeight;
	private TextView txtDuration;
	private View spacer;
	private ViewGroup root;
	
	private Interpolator fastOutSlowInInterpolator;
	private RecyclerView recyclerView;
	private JourneyDetailAdapter adapter;
	
	public static JourneyDetailFragment newInstance(Journey journey) {
		
		JourneyDetailFragment fragment = new JourneyDetailFragment();
		
		Bundle args = new Bundle(1);
		args.putSerializable(EXTRA_JOURNEY, journey);
		fragment.setArguments(args);
		return fragment;
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		
		LUtils.loadInterpolatorWithFallback(getContext(), android.R.interpolator.fast_out_slow_in, android.R.interpolator.decelerate_cubic);
		
		journey = (Journey) getArguments().getSerializable(EXTRA_JOURNEY);
	}
	
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		
		root = (ViewGroup) inflater.inflate(R.layout.fragment_journey_detail, container, false);
		statusBarHeight = Utils.getStatusBarHeight(getContext());
		
		journeyInfoView = (JourneyInfoView) root.findViewById(R.id.view_journey_info);
		journeyInfoView.setJourney(journey);
		
		
		recyclerView = (RecyclerView) root.findViewById(R.id.lst_recycler);
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		
		int[] durationHM = journey.getDurationHoursMinutes();
		txtDuration = (TextView) root.findViewById(R.id.txt_duration);
		txtDuration.setText((durationHM[0] > 0 ? durationHM[0] + "h " : "") + durationHM[1] + "m");
		
		adapter = new JourneyDetailAdapter(getContext(), journey);
		
		recyclerView.setPadding(0, Utils.dpToPx(getContext(), 16), 0, Utils.getNavigationBarHeight(getContext()) + (int) statusBarHeight);
		recyclerView.setAdapter(adapter);
		//adapter.notifyDataSetChanged();
		
		journeyInfoView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
					journeyInfoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				} else {
					journeyInfoView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}
				
				((MapActivity) getActivity()).setPeekHeight(journeyInfoView.getMeasuredHeight()+Utils.dpToPx(getContext(),32) + Utils.getNavigationBarHeight(getContext()));
			}
		});
		
		return root;
	}
	
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}
	
	public void setMargin(float offset) {
		final int height = offset > 1 ? (int) (((offset) - 1f) * statusBarHeight) : 0;
		root.layout(0, height, root.getWidth(), root.getBottom());
	}
}
