package se.davison.travelbot.ui.fragments.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;


public class ConfirmDialog extends BaseDialogFragment {

    private static final String EXTRA_YES = "extra_yes";
    private static final String EXTRA_NO = "extra_no";
    public static final String FRAGMENT_TAG = "confirm_dialog_fragment_tag";

    public static class ConfirmDialogEvent extends DialogEvent {
        public ConfirmDialogEvent(String tag, int result) {
            super(tag, result);
        }

        public static final int YES = 0;
        public static final int NO = 1;
    }


    public static class Builder extends BaseDialogFragment.Builder {

        private String yesText;
        private String noText;

        public Builder(DialogCallback callback, String yesText, String noText) {
            super(callback);
            this.yesText = yesText;
            this.noText = noText;
        }




        @Override
        public ConfirmDialog show() {
            Log.d("ASFASfASf", "FINISHING??!?!" + Boolean.valueOf(((Activity) context).isFinishing()));
            String dialogTag = tag == null ? FRAGMENT_TAG : tag;
            ConfirmDialog dialog = ConfirmDialog.newInstance(dialogTag, yesText, noText, message, title);
            if(fragment!=null){
                dialog.setTargetFragment(fragment,0);
            }
            dialog.show(fm);
            return dialog;
        }
    }

    public static ConfirmDialog newInstance(String tag, String yesText, String noText,
                                            String message, String title) {
        ConfirmDialog dialogFragment = new ConfirmDialog();
        Bundle args = new Bundle();
        args.putString(EXTRA_YES, yesText);
        args.putString(EXTRA_NO, noText);
        dialogFragment.setArguments(tag, message, title, args);
        return dialogFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle args = getArguments();

        String yes = args.getString(EXTRA_YES);
        String no = args.getString(EXTRA_NO);



        AlertDialog.Builder builder = getDefaultBuilder();
        builder.setPositiveButton(yes, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                postEvent(new ConfirmDialogEvent(getFragmentTag(), ConfirmDialogEvent.YES));
            }
        });
        builder.setNegativeButton(no, new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                postEvent(new ConfirmDialogEvent(getFragmentTag(), ConfirmDialogEvent.YES));
            }
        });

        return builder.create();
    }


    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

}
