package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.TypedValue;

import se.davison.travelbot.R;


/**
 * Created by admin on 2015-08-07.
 */
public class StopSearchView extends CardView {

    private String hint;
    private Paint paint;
    private Paint fadePaint;
    private float dp;
    private float sp;
    private Bitmap iconBitmap;

    public StopSearchView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fadePaint = new Paint();



        dp = context.getResources().getDisplayMetrics().density;
        sp = context.getResources().getDisplayMetrics().scaledDensity;

        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.textColorSecondary, typedValue, true);

        //paint.setColor(getResources().getColor(typedValue.resourceId));
        paint.setTextSize(14 * sp);

        setCoverOffset(0);


    }

    public void setCoverOffset(float offset){
        fadePaint.setColor(Color.argb((int)(offset*255f),255,255,255));
    }

    public void setup(String hint, Bitmap iconBitmap){
        this.hint = hint;
        this.iconBitmap = iconBitmap;
    }

    public StopSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public StopSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawText(hint, dp * 64, getHeight() / 2+sp*6, paint);
        canvas.drawBitmap(iconBitmap,16*dp,getHeight() / 2- iconBitmap.getHeight()/2, paint);
        canvas.drawRect(0,0,getWidth(),getHeight(),fadePaint);

    }
}
