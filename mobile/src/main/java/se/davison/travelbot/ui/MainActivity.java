package se.davison.travelbot.ui;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.util.Pair;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import se.davison.travelbot.adapters.ViewPagerAdapter;
import se.davison.travelbot.db.Database;
import se.davison.travelbot.db.StopProvider;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.net.LocationResponse;
import se.davison.travelbot.net.RestClient;
import se.davison.travelbot.ui.fragments.NextTripFragment;
import se.davison.travelbot.ui.fragments.PlaceHolderFragment;
import se.davison.travelbot.R;
import se.davison.travelbot.ui.fragments.SearchResultFragment;
import se.davison.travelbot.ui.fragments.JourneyHistoryFragment;
import se.davison.travelbot.ui.fragments.dialogs.AlertDialog;
import se.davison.travelbot.ui.fragments.dialogs.BaseDialogFragment;
import se.davison.travelbot.ui.fragments.dialogs.ProgressDialogFragment;
import se.davison.travelbot.ui.fragments.dialogs.TimePickerDialogFragment;
import se.davison.travelbot.ui.widget.ExtendedSnackbar;
import se.davison.travelbot.ui.widget.InsetsRelativeLayout;
import se.davison.travelbot.util.ActivityErrorHandler;
import se.davison.travelbot.util.Constants;
import se.davison.travelbot.util.EventBus;
import se.davison.travelbot.util.HideViewAnimationListener;
import se.davison.travelbot.util.LUtils;
import se.davison.travelbot.util.LayerEnablingAnimationListener;
import se.davison.travelbot.util.LayerEnablingAnimatorListener;
import se.davison.travelbot.util.LocationException;
import se.davison.travelbot.util.ReverseInterpolator;
import se.davison.travelbot.util.RxUtils;
import se.davison.travelbot.util.Utils;

public class MainActivity extends RxAppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BaseDialogFragment.DialogCallback, TimePickerDialogFragment.Callback, ActivityErrorHandler.Callback {


    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int SLIDE_ANIMATION_DURATION = 1000 * 3;
    private static final int ANIMATION_DURATION = 200;
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    private static final String EXTRA_PLAY_SERVICES_ERROR_CODE = "extra_play_services_error_code";
    private static final String STATE_INPUT_OPEN = "state_input_open";
    private static final String STATE_ORIGIN = "state_origin";
    private static final String STATE_DESTINATION = "state_destination";
    private static final String STATE_TIME = "state_time";
    private static final String STATE_SEARCH_FOR_DEPARTURE = "state_search_for_departure";
    private static final String STATE_RESOLVING_ERROR = "state_resolving_error";
    private static final String TAG_SEARCH_RESULT_FRAGMENT = "tag_search_result_fragment";
    private static final String TAG_PLAY_ERROR_DIALOG = "tag_play_error_dialog";
    private static final String TAG_DOWNLOAD_DATABASE_DIALOG = "tag_download_database_dialog";
    private static final String TAG_DOWNLOAD_DATABASE_PROGRESS_DIALOG = "tag_download_database_progress_dialog";
    private static final String TAG_TIME_PICKER_DIALOG = "tag_time_picker_dialog";
    private static final String TAG_DATABASE_ERROR_DIALOG = "tag_database_error_dialog";
    private static final String PREF_KEY_DATABASE_DOWNLOADED = "pref_key_database_downloaded";

    public static final String PREF_KEY_FAVORITE_STOPS = "pref_key_favorite_stops";
    public static final String PREF_KEY_FAVORITE_JOURNEYS = "pref_key_favorite_journeys";
    public static final String PREF_KEY_HISTORY_JOURNEYS = "pref_key_history_journeys";

    public static final int RESULT_STOP = 0;
    public static final int RESULT_MY_LOCATION = 1;
    public static final int RESULT_CHOSE_FROM_MAP = 2;
    private static final String MY_LOCATION_ID = "my_location_id";
    private static final int TRY_DOWNLOAD_DATABASE = 0;
    public static final int REQUEST_CODE_PERMISSIONS = 99;
    private static final int REQUEST_CODE_SHOW_SETTINGS = 101;



    private Bitmap iconSearch;
    private Bitmap iconAddress;
    private Bitmap iconStop;
    private Bitmap iconLocation;


    private ValueAnimator optionsAnimation = ValueAnimator.ofInt(0, 1);
    private TextView txtFrom;
    private TextView txtTo;
    private Toolbar toolbar;
    private AlphaAnimation searchResultFragmentAnimation;
    private EditText txtInput;
    private InputMethodManager inputMethodManager;
    private ViewGroup cntFragment;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;


    //private Subscription downloadSubscription;
    private ProgressDialogFragment progressDialog;


    private Stop origin;
    private Stop destination;
    private SharedPreferences prefs;
    private SearchResultFragment searchStopResultFragment;
    private ActivityErrorHandler activityErrorHandler = new ActivityErrorHandler();
    private ViewGroup cntInput;

    private boolean requestFrom = false;
    private boolean showInputLocation = false;
    private int textColorSelected = 0xFFFFFFFF;
    private int textColorUnselected = 0xb3FFFFFF;
    private String strMyLocation;
    private String strFromStopOrAddress;
    private String strToStopOrAddress;
    private String strDepartAt;
    private String strArriveBy;
    private String strToday;
    private String strTomorrow;
    private String strYesterday;
    private boolean databaseDownloaded = false;
    private boolean optionsExpanded = false;
    private int rootOffset = 0;
    private boolean inputOpen = false;
    private int statusBarHeight;
    private boolean resolvingError = false;
    private int[] revealXY = new int[2];
    private ImageView imgFrom;
    private ImageView imgTo;
    private Bitmap iconFrom;
    private FloatingActionButton fab;
    private TextView txtOptions;
    private TimePickerDialogFragment timePickerDialog;
    private Date optionTime;
    private boolean optionDeparture = true;
    private ViewPagerAdapter adapter;
    private int databaseDownloadErrorCount = 0;
    private LayerEnablingAnimatorListener rootAnimationListener;
    private View viewFabReveal;
    private InsetsRelativeLayout rootLayout;
    private NextTripFragment nextTripFragment;
    private JourneyHistoryFragment journeyHistoryFragment;

    private Interpolator fastOutSlowInInterpolator;
    private Interpolator reverseFastOutSlowInInterpolator;
    private ViewPager pager;
    private boolean hasErrors = false;




    public void setResult(int resultCode, Stop result, View sharedView) {

        if (resultCode == RESULT_CHOSE_FROM_MAP) {
            //TODO: show chose from map
        } else {
            addStop(resultCode, result, requestFrom, true, sharedView);
        }
    }

    public void addStop(int resultCode, Stop result, boolean isFrom, boolean pressBack, View sharedView) {
        TextView target = isFrom ? txtFrom : txtTo;
        ImageView imageTarget = isFrom ? imgFrom : imgTo;
        Bitmap bitmapIcon;

        Stop stop = result;
        if (resultCode == RESULT_MY_LOCATION) {
            stop = new Stop(Stop.TYPE_ADDRESS, strMyLocation, "", null, null);
            stop.setId(MY_LOCATION_ID);
            bitmapIcon = iconLocation;
        } else {
            bitmapIcon = stop.getStopType() == Stop.TYPE_STOP ? iconStop : iconAddress;
        }

        if (isFrom) {
            iconFrom = bitmapIcon;
            origin = stop;
        } else {
            destination = stop;
        }

        target.setTextColor(textColorSelected);
        target.setText(stop.getName());
        imageTarget.setImageBitmap(bitmapIcon);


        if (resultCode == RESULT_STOP && !showInputLocation) {
            imgFrom.setImageBitmap(iconSearch);
            DepartureBoardActivity.startActivity(this, stop, sharedView);
        }
        if (showInputLocation) {
            if (pressBack) {
                onBackPressed();
            }



            if(origin.equals(destination)){
                txtFrom.setError("ERROR");
                txtTo.setError("ERROR");
                hasErrors = true;
            }else{
                txtFrom.setError(null);
                txtTo.setError(null);
                hasErrors = false;
            }

            if (origin != null && destination != null && txtFrom.getError()==null && txtTo.getError()==null) {
                onTimeSelected(new Date(), true);
                if (!optionsExpanded) {
                    optionsExpanded = true;
                    optionsAnimation.setInterpolator(fastOutSlowInInterpolator);
                    optionsAnimation.setStartDelay(pressBack ? ANIMATION_DURATION * 2 : 0);
                    optionsAnimation.start();
                }
            }

            if(optionsExpanded && hasErrors){
                optionsExpanded = false;
                optionsAnimation.setInterpolator(reverseFastOutSlowInInterpolator);
                optionsAnimation.setStartDelay(pressBack ? ANIMATION_DURATION * 2 : 0);
                optionsAnimation.start();
            }
        }


    }

    public void updateLocation() {





        if (nextTripFragment == null) {
            nextTripFragment = (NextTripFragment) adapter.getFragmentsWithClass(NextTripFragment.class)[0];
        }


        //lastLocation = Utils.getRandomDebugLocation();
        if (lastLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSIONS);
                return;
            }
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    googleApiClient);
        }
        if (lastLocation != null) {
            nextTripFragment.setLocation(lastLocation);
        } else {
            nextTripFragment.onError(new LocationException());
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    updateLocation();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    nextTripFragment.onError(new SecurityException());
                    showPermissionDeniedSnackbar();

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showPermissionDeniedSnackbar() {
        ExtendedSnackbar.extend(Snackbar
                .make(rootLayout, Html.fromHtml(String.format("<font color=\"#ffffff\">%s</font>",getString(R.string.enable_location_permission))), Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.settings), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivityForResult(Utils.newAppDetailsIntent(MainActivity.this,null),REQUEST_CODE_SHOW_SETTINGS);
                    }
                }),Utils.getNavigationBarHeight(this)).show();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        updateLocation();
    }

    @Override
    public void onConnectionSuspended(int cause) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (resolvingError) {
            return;
        } else if (result.hasResolution()) {
            try {
                resolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                googleApiClient.connect();
            }
        } else {
            showErrorDialog(result.getErrorCode());
            resolvingError = true;
        }
    }

    private void showErrorDialog(int errorCode) {
        PlayServicesErrorDialogFragment dialogFragment = new PlayServicesErrorDialogFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_PLAY_SERVICES_ERROR_CODE, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), TAG_PLAY_ERROR_DIALOG);
    }

    public void onDialogDismissed() {
        resolvingError = false;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!resolvingError) {  // more about this later
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            resolvingError = false;
            if (resultCode == RESULT_OK) {
                if (!googleApiClient.isConnecting() &&
                        !googleApiClient.isConnected()) {
                    googleApiClient.connect();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_INPUT_OPEN, inputOpen);
        outState.putBoolean(STATE_RESOLVING_ERROR, resolvingError);
        outState.putSerializable(STATE_ORIGIN, origin);
        outState.putSerializable(STATE_DESTINATION, destination);
        outState.putSerializable(STATE_TIME, optionTime);
        outState.putBoolean(STATE_SEARCH_FOR_DEPARTURE, optionDeparture);

        activityErrorHandler.onSaveInstanceState(outState);

        super.onSaveInstanceState(outState);
    }

    public static float clamp(float value, float max, float min) {
        return Math.min(Math.max(value, max), min);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        buildGoogleApiClient();

        fastOutSlowInInterpolator = LUtils.loadInterpolatorWithFallback(this,android.R.interpolator.fast_out_slow_in,android.R.interpolator.decelerate_cubic);
        reverseFastOutSlowInInterpolator = new ReverseInterpolator(fastOutSlowInInterpolator);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        databaseDownloaded = prefs.getBoolean(PREF_KEY_DATABASE_DOWNLOADED, false);

        activityErrorHandler.onCreate(getSupportFragmentManager(), savedInstanceState);

        if (databaseDownloaded) {
            EventBus.getInstance().observeEvents(Database.DatabaseEvent.class).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread()).compose(this.<Database.DatabaseEvent>bindToLifecycle()).subscribe(new Action1<Database.DatabaseEvent>() {
                @Override
                public void call(Database.DatabaseEvent databaseEvent) {
                    if (!databaseEvent.loaded) {

                        activityErrorHandler.error(TRY_DOWNLOAD_DATABASE, getString(R.string.err_database_corrupted));
                    }
                }
            });
        }


        strMyLocation = getString(R.string.my_location);
        strFromStopOrAddress = getString(R.string.from_stop_or_address);
        strToStopOrAddress = getString(R.string.to_stop_or_address);
        strDepartAt = getString(R.string.depart_at);
        strArriveBy = getString(R.string.arrive_by);
        strToday = getString(R.string.today);
        strTomorrow = getString(R.string.tomorrow);
        strYesterday = getString(R.string.yesterday);

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        txtFrom = (TextView) findViewById(R.id.txt_from);
        txtTo = (TextView) findViewById(R.id.txt_to);
        txtOptions = (TextView) findViewById(R.id.txt_options);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        cntInput = (ViewGroup) findViewById(R.id.cnt_input);
        imgFrom = (ImageView) findViewById(R.id.img_from);
        imgTo = (ImageView) findViewById(R.id.img_to);
        final ImageView imgSwap = (ImageView) findViewById(R.id.img_swap);
        final ImageView imgDots = (ImageView) findViewById(R.id.img_stop_dots);
        final View viewLine = findViewById(R.id.view_line);
        final ViewGroup cntSwap = (ViewGroup) findViewById(R.id.cnt_swap);
        cntFragment = (ViewGroup) findViewById(R.id.cnt_fragment);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewFabReveal = findViewById(R.id.view_fab_reveal);
        final ViewGroup.MarginLayoutParams lineViewLayoutParams = (ViewGroup.MarginLayoutParams) viewLine.getLayoutParams();
        final ViewGroup.LayoutParams inputLayoutParams = cntInput.getLayoutParams();
        pager = (ViewPager) findViewById(R.id.view_pager);
        rootLayout = (InsetsRelativeLayout) findViewById(R.id.cnt_root);
        pager.setOffscreenPageLimit(2);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.post(new Runnable() {
            @Override
            public void run() {
                fab.setVisibility(View.GONE);
            }
        });



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtFrom.getError() == null && txtTo.getError() == null) {

                    int[] fabLocation = new int[2];
                    v.getLocationOnScreen(fabLocation);
                    fab.hide();


                    if (journeyHistoryFragment == null) {
                        journeyHistoryFragment = (JourneyHistoryFragment) adapter.getFragmentsWithClass(JourneyHistoryFragment.class)[0];
                    }


                    journeyHistoryFragment.addHistory(origin, destination);


                    new LUtils.CircularRevealBuilder(viewFabReveal)
                            .cx(fabLocation[0] + (v.getWidth() / 2)).cy(fabLocation[1] + (v.getHeight() / 2)).withLayer().duration(ANIMATION_DURATION * 2)
                            .interpolator(fastOutSlowInInterpolator).listener(new LUtils.CircularRevealListener() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            JourneyActivity.startActivity(MainActivity.this, origin, destination, optionTime, !optionDeparture);
                        }

                        @Override
                        public void onAnimationStart(Animator animation) {
                            if (animation != null) {
                                viewFabReveal.setVisibility(View.VISIBLE);
                            }
                        }
                    }).createAndStart();

                } else {
            /*ExtendedSnackbar.extend(Snackbar
                    .make(rootLayout, Html.fromHtml(String.format("<font color=\"#ffffff\">%s</font>",getString(R.string.err_same_stops))), Snackbar.LENGTH_INDEFINITE)
                    ,Utils.getNavigationBarHeight(MainActivity.this)).show();*/
                }

            }
        });

        rootAnimationListener = new LayerEnablingAnimatorListener(cntInput);

        txtOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.setDate(optionTime);
                timePickerDialog.show(getSupportFragmentManager(), TAG_TIME_PICKER_DIALOG);
            }
        });







        //toggleFab(false);

        txtInput = (EditText) findViewById(R.id.txt_input);


        RxUtils.createEditTextChangeObservable(txtInput)
                .throttleLast(200, TimeUnit.MILLISECONDS, Schedulers.io())
                .concatMap(new Func1<String, Observable<Pair<String, List<Stop>>>>() {
                    @Override
                    public Observable<Pair<String, List<Stop>>> call(final String query) {

                        if (query.isEmpty()) {
                            return Observable.just(null);
                        }

                        return StopProvider.getStopResultObservable(getContentResolver(), query)
                                .map(new Func1<List<Stop>, Pair<String, List<Stop>>>() {
                                    @Override
                                    public Pair<String, List<Stop>> call(List<Stop> stops) {
                                        return new Pair(query, stops);
                                    }
                                });
                    }
                }).concatMap(new Func1<Pair<String, List<Stop>>, Observable<List<Stop>>>() {

                                 @Override
                                 public Observable<List<Stop>> call(Pair<String, List<Stop>> queryAndStops) {
                                     if (queryAndStops == null) {
                                         return Observable.just(null);
                                     }
                                     if (queryAndStops.second.size() == 0) {
                                         return RestClient.service().locationName(queryAndStops.first)
                                                 .map(new Func1<LocationResponse, List<Stop>>() {
                                                     @Override
                                                     public List<Stop> call(LocationResponse locationNameResponse) {
                                                         return locationNameResponse.getAddresses();
                                                     }
                                                 });
                                     } else {
                                         return Observable.just(queryAndStops.second);
                                     }
                                 }
                             }
        )
                .onErrorResumeNext(Observable.<List<Stop>>empty())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<List<Stop>>bindToLifecycle())
                .subscribe(new Action1<List<Stop>>() {
                               @Override
                               public void call(List<Stop> stops) {
                                   if (stops != null) {
                                       searchStopResultFragment.showStops(stops);
                                   } else {
                                       searchStopResultFragment.showStartItems();
                                   }
                               }
                           },
                        new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                showError(throwable);
                            }
                        });


        if (savedInstanceState != null) {
            inputOpen = savedInstanceState.getBoolean(STATE_INPUT_OPEN, false);
            resolvingError = savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);


            origin = (Stop) savedInstanceState.getSerializable(STATE_ORIGIN);
            destination = (Stop) savedInstanceState.getSerializable(STATE_DESTINATION);
            optionTime = (Date)savedInstanceState.getSerializable(STATE_TIME);
            optionDeparture = savedInstanceState.getBoolean(STATE_SEARCH_FOR_DEPARTURE);

            if (origin != null) {
                txtFrom.setText(origin.getName());
                txtFrom.setTextColor(textColorSelected);
            }
            if (destination != null) {
                txtTo.setText(destination.getName());
                txtTo.setTextColor(textColorSelected);
            }

            if (inputOpen) {
                toolbar.setVisibility(View.VISIBLE);
            }
        } else {
            //AppObservable.


            searchStopResultFragment = SearchResultFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.cnt_fragment, searchStopResultFragment, TAG_SEARCH_RESULT_FRAGMENT).commit();
        }


        statusBarHeight = Utils.getStatusBarHeight(this);

        tabLayout.setPadding(0, statusBarHeight, 0, 0);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(txtInput.getWindowToken(), 0);
                onBackPressed();

            }
        });
        toolbar.setPadding(0, statusBarHeight, 0, 0);
        toolbar.inflateMenu(R.menu.search_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                txtInput.setText("");
                return true;
            }
        });

        int actionBarSize = Utils.getActionBarHeight(this);
        ((ViewGroup.MarginLayoutParams) cntFragment.getLayoutParams()).setMargins(0, statusBarHeight + actionBarSize, 0, 0);


        final int dp56 = Utils.dpToPx(this, 56);
        final int dp48 = Utils.dpToPx(this, 48);
        final int dp8 = Utils.dpToPx(this, 8);
        final int dp208 = Utils.dpToPx(this, 208);


        iconSearch = BitmapFactory.decodeResource(getResources(), R.drawable.ic_search);
        iconAddress = BitmapFactory.decodeResource(getResources(), R.drawable.ic_address);
        iconStop = BitmapFactory.decodeResource(getResources(), R.drawable.ic_stop);
        iconFrom = iconStop;
        iconLocation = BitmapFactory.decodeResource(getResources(), R.drawable.ic_location);

        final String hintNextTrip = getString(R.string.search_stop_or_address);
        final String hintSearchTripFrom = getString(R.string.from_stop_or_address);
        final String hintSearchTripTo = getString(R.string.to_stop_or_address);


        optionsAnimation.addListener(new LayerEnablingAnimatorListener(cntInput));
        optionsAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {

                int height = statusBarHeight + heightFracton((dp56 * 2) + dp48, dp208, animation.getAnimatedFraction());

                if (journeyHistoryFragment == null) {
                    journeyHistoryFragment = (JourneyHistoryFragment) adapter.getFragmentsWithClass(JourneyHistoryFragment.class)[0];
                }

                journeyHistoryFragment.setContainerRelativeTopPadding((int) (dp48 * animation.getAnimatedFraction()));

                inputLayoutParams.height = height;
                cntInput.setLayoutParams(inputLayoutParams);
            }
        });
        optionsAnimation.setInterpolator(fastOutSlowInInterpolator);
        optionsAnimation.setDuration(ANIMATION_DURATION);


        searchResultFragmentAnimation = new AlphaAnimation(0, 1);
        searchResultFragmentAnimation.setDuration(ANIMATION_DURATION);




        final RotateAnimation rotateAnimation = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(ANIMATION_DURATION);
        rotateAnimation.setAnimationListener(new LayerEnablingAnimationListener(imgSwap));
        rotateAnimation.setInterpolator(fastOutSlowInInterpolator);


        final TranslateAnimation animSwapTo = new TranslateAnimation(0, 0, 0, -dp56);
        animSwapTo.setDuration(ANIMATION_DURATION);
        animSwapTo.setInterpolator(fastOutSlowInInterpolator);
        animSwapTo.setFillEnabled(true);
        animSwapTo.setFillAfter(true);

        final TranslateAnimation animSwapFrom = new TranslateAnimation(0, 0, 0, dp56);
        animSwapFrom.setDuration(ANIMATION_DURATION);
        animSwapFrom.setInterpolator(fastOutSlowInInterpolator);
        animSwapFrom.setFillAfter(true);
        animSwapFrom.setFillEnabled(true);
        animSwapFrom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                String textFrom = txtFrom.getText().toString();
                String textTo = txtTo.getText().toString();
                Bitmap temp = iconFrom;
                iconFrom = ((BitmapDrawable) imgTo.getDrawable()).getBitmap();
                txtFrom.setText(textTo);
                imgFrom.setImageBitmap(iconFrom);
                imgTo.setImageBitmap(temp);
                txtTo.setText(textFrom);
                animSwapFrom.setFillAfter(false);
                animSwapTo.setFillAfter(false);
                Stop tempStop = origin;
                origin =  destination;
                destination = tempStop;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        cntSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgSwap.startAnimation(rotateAnimation);
                txtFrom.startAnimation(animSwapFrom);
                imgFrom.startAnimation(animSwapFrom);
                txtTo.startAnimation(animSwapTo);
                imgTo.startAnimation(animSwapTo);

            }
        });

        txtFrom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    requestFrom = true;
                    rootOffset = dp48;
                    showInput(event, txtFrom, origin, strFromStopOrAddress);
                }
                return false;
            }
        });


        txtTo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {



                if (event.getAction() == MotionEvent.ACTION_UP) {
                    rootOffset = dp48 + dp56;
                    requestFrom = false;
                    showInput(event, txtTo, destination, strToStopOrAddress);
                }
                return false;
            }
        });


        final ValueAnimator containerAnimation = ValueAnimator.ofFloat(0, 1f);
        containerAnimation.setInterpolator(new LinearInterpolator());
        containerAnimation.addListener(new LayerEnablingAnimatorListener(cntInput));
        containerAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            private float targetHeight(int step) {



                switch (step) {
                    case 0:
                        return statusBarHeight + (dp56 + dp48);
                    case 1:
                        return statusBarHeight + dp48 + (dp56 * 2) + (optionsExpanded ? dp48 : 0);
                }
                return statusBarHeight + dp48;

            }

            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {


                int step = (int) Math.floor(valueAnimator.getAnimatedFraction() * 3f);
                float playtime = valueAnimator.getCurrentPlayTime();

                float fromHeight = targetHeight(step);
                float toHeight = targetHeight(step + 1);
                float percentage = valueAnimator.getAnimatedFraction() * 3f - (float) step;

                /*
                Log.d(TAG,"---------------------------");
                Log.d(TAG,"step="+step);
                Log.d(TAG,"toHeight="+toHeight);
                Log.d(TAG,"fromHeight="+fromHeight);
                Log.d(TAG,"percentage="+percentage);
                */


                inputLayoutParams.height = (int) (fromHeight + ((toHeight - fromHeight) * percentage));
                lineViewLayoutParams.topMargin = -dp8 + (int) Math.min(((float) dp8) * playtime / 1000f, (float) dp8);

                if (step == 0) {
                    float alpha = (percentage * 2f - 1f);
                    txtTo.setAlpha(alpha);
                    imgSwap.setAlpha(alpha);
                    imgTo.setAlpha(alpha);
                    imgDots.setAlpha(alpha);
                    txtOptions.setAlpha(alpha);

                    if (optionsExpanded) {
                        fab.setAlpha(alpha);
                    }
                }
                if (step == 1) {

                    float alpha = ((1 - percentage) * 2f - 1f);
                    txtTo.setAlpha(alpha);
                    txtFrom.setAlpha(alpha);
                    imgSwap.setAlpha(alpha);
                    txtOptions.setAlpha(alpha);
                    imgFrom.setAlpha(alpha);
                    imgTo.setAlpha(alpha);
                    imgDots.setAlpha(alpha);
                    if (optionsExpanded) {
                        fab.setAlpha(alpha);
                    }
                }

                cntInput.setLayoutParams(inputLayoutParams);
            }
        });
        containerAnimation.setDuration(SLIDE_ANIMATION_DURATION);
        containerAnimation.setCurrentPlayTime(0);

        adapter = new ViewPagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return NextTripFragment.newInstance();
                    case 1:
                        return JourneyHistoryFragment.newInstance();
                    case 2:
                        return PlaceHolderFragment.newInstance(getString(R.string.more));
                }
                return null;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.next_trip);
                    case 1:
                        return getString(R.string.search_trip);
                    case 2:
                        return getString(R.string.more);
                }
                return "";
            }
        };


        pager.setAdapter(adapter);

        tabLayout.setupWithViewPager(pager);


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                containerAnimation.setCurrentPlayTime((int) ((positionOffset + position) * 1000f));
            }

            @Override
            public void onPageSelected(int position) {
                Fragment fragment = adapter.getFragment(position);
                if(fragment instanceof ViewPagerAdapter.OnPageSelectedListener){
                    ((ViewPagerAdapter.OnPageSelectedListener)fragment).onPageSelected();
                }
                if (position == 0) {
                    showInputLocation = false;
                    if (origin != null && origin.getStopType() == Stop.TYPE_ADDRESS) {
                        txtFrom.setText(strFromStopOrAddress);
                        txtFrom.setTextColor(textColorUnselected);
                    }

                    txtFrom.setError(null);
                    imgFrom.setImageBitmap(iconSearch);
                } else {
                    showInputLocation = true;
                }
                if (position == 1) {
                    imgFrom.setImageBitmap(iconFrom);
                    if (origin != null && origin.getStopType() == Stop.TYPE_ADDRESS) {
                        txtFrom.setText(origin.getName());
                        txtFrom.setTextColor(textColorSelected);
                    }
                    if(hasErrors){
                        txtFrom.setError("ERROR");
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private int heightFracton(int fromHeight, int toHeight, float fraction) {
        return (int) (fromHeight - ((fromHeight - toHeight) * fraction));
    }


    public static Bitmap loadBitmapFromView(View v) {
        Bitmap bitmap;
        v.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        return bitmap;
    }

    //FIXME: remove
    private void showDataBaseErrorDialog(boolean downloadError) {
        databaseDownloaded = false;
        prefs.edit().putBoolean(PREF_KEY_DATABASE_DOWNLOADED, databaseDownloaded);

        String message = downloadError ? databaseDownloadErrorCount < 3 ? getString(R.string.err_download_database) : getString(R.string.err_download_database_close) : getString(R.string.err_database_corrupted);

        new AlertDialog.Builder(this).cancelable(false).message(message).tag(TAG_DATABASE_ERROR_DIALOG).show();
    }


    private void showError(Throwable throwable) {
        throwable.printStackTrace();
        Toast.makeText(this, R.string.err_common, Toast.LENGTH_SHORT).show();
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    private void showInput(MotionEvent event, TextView target, Stop stop, String hint) {


        rootLayout.setZeroInset(false);


        fab.hide();
        txtInput.setText("");
        /*if (stop == null) {
            txtInput.setText("");
        } else {
            txtInput.setText("");
            if (stop.getId() != null && !stop.getId().equals(MY_LOCATION_ID)) {
                txtInput.append(target.getText());
            }
        }*/
        txtInput.setHint(hint);

        txtInput.requestFocus();
        inputMethodManager.showSoftInput(txtInput, InputMethodManager.SHOW_IMPLICIT);

        searchResultFragmentAnimation.setAnimationListener(new HideViewAnimationListener(cntFragment, true));
        searchResultFragmentAnimation.setInterpolator(fastOutSlowInInterpolator);
        searchResultFragmentAnimation.setStartOffset(ANIMATION_DURATION);
        cntFragment.startAnimation(searchResultFragmentAnimation);

        animateAppbarTranslation(-rootOffset);

        inputOpen = true;
        revealXY[0] = (int) event.getX();
        revealXY[1] = (int) event.getY() + statusBarHeight;


        new LUtils.CircularRevealBuilder(toolbar).cx(revealXY[0]).cy(revealXY[1]).delay(ANIMATION_DURATION).withLayer()
                .interpolator(fastOutSlowInInterpolator).duration(ANIMATION_DURATION).listener(new LUtils.CircularRevealListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                toolbar.setVisibility(View.VISIBLE);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Window w = getWindow(); // in Activity's onCreate() for instance
                    //w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    //w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    /*w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
                    w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);*/
                }
            }
        }).createAndStart();

    }


    @Override
    public void onBackPressed() {
        if (inputOpen) {
            inputOpen = false;
            searchResultFragmentAnimation.setStartOffset(0);
            searchResultFragmentAnimation.setInterpolator(reverseFastOutSlowInInterpolator);
            searchResultFragmentAnimation.setAnimationListener(new HideViewAnimationListener(cntFragment, false));
            cntFragment.startAnimation(searchResultFragmentAnimation);

            rootLayout.setZeroInset(true);


            new LUtils.CircularRevealBuilder(toolbar)
                    .interpolator(reverseFastOutSlowInInterpolator).duration(ANIMATION_DURATION).withLayer().listener(new LUtils.CircularRevealListener() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    animateAppbarTranslation(0);
                    toolbar.setVisibility(View.INVISIBLE);
                    showFabIfStopsSelected(ANIMATION_DURATION);
                }
            }).cx(revealXY[0]).cy(revealXY[1]).createAndStart();

        } else {
            super.onBackPressed();
        }

    }

    public void showFabIfStopsSelected(int delay) {
        if (origin != null && destination != null && pager.getCurrentItem() == 1 && txtFrom.getError()==null && txtTo.getError()==null) {
            fab.postDelayed(new Runnable() {
                @Override
                public void run() {
                    fab.show();
                }
            }, delay);
        }
    }

    private void animateAppbarTranslation(int value) {
        cntInput.animate().translationY(value).setListener(rootAnimationListener).setInterpolator(fastOutSlowInInterpolator).setDuration(ANIMATION_DURATION).start();
    }

    @Override
    protected void onResume() {

        super.onResume();
        viewFabReveal.setVisibility(View.INVISIBLE);
        showFabIfStopsSelected(ANIMATION_DURATION);
        searchStopResultFragment = (SearchResultFragment) getSupportFragmentManager().findFragmentByTag(TAG_SEARCH_RESULT_FRAGMENT);


        timePickerDialog = (TimePickerDialogFragment) getSupportFragmentManager().findFragmentByTag(TAG_TIME_PICKER_DIALOG);
        if (timePickerDialog == null) {
            timePickerDialog = TimePickerDialogFragment.newInstance();
        }

        if (!databaseDownloaded) {


            //downloadDatabase();

            activityErrorHandler.tryRun(TRY_DOWNLOAD_DATABASE, new ActivityErrorHandler.Action() {
                @Override
                public void call() {
                    downloadDatabase();
                }
            });


        }

        if(origin != null && destination != null){
            onTimeSelected(new Date(), optionDeparture);
        }

    }

    private void downloadDatabase() {
        progressDialog = (ProgressDialogFragment) ProgressDialogFragment.getInstance(getSupportFragmentManager(), TAG_DOWNLOAD_DATABASE_PROGRESS_DIALOG);
        if (progressDialog == null) {
            progressDialog = (ProgressDialogFragment) new ProgressDialogFragment.Builder(MainActivity.this).max(100).message(getString(R.string.downloading_database)).show();
        }

        RestClient.getInstance().downloadFile(Constants.URL_DB, getCacheDir(), Constants.DB_NAME).subscribeOn(Schedulers.io()).onBackpressureLatest()
                .observeOn(AndroidSchedulers.mainThread()).compose(this.<Integer>bindToLifecycle()).subscribe(new Observer<Integer>() {

            private int progress = 0;

            @Override
            public void onCompleted() {
                if (progress == 100) {
                    databaseDownloaded = true;
                    prefs.edit().putBoolean(PREF_KEY_DATABASE_DOWNLOADED, true).commit();
                    getContentResolver().call(StopProvider.CONTENT_URI, StopProvider.METHOD_ENSURE_LOADED, null, null);
                }
                if (progressDialog != null) {
                    progressDialog.dismissAllowingStateLoss();
                }
            }

            @Override
            public void onError(Throwable e) {

                Utils.removeDatabase(getBaseContext());
                if (progressDialog != null) {
                    progressDialog.dismissAllowingStateLoss();
                }

                activityErrorHandler.error(TRY_DOWNLOAD_DATABASE, getString(R.string.err_download_database));

                //showDataBaseErrorDialog(true);

                databaseDownloadErrorCount++;

                e.printStackTrace();
            }

            @Override
            public void onNext(Integer integer) {
                progress = integer;
                if (progressDialog != null) {

                    progressDialog.setProgress((float) integer / 100f);
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onDialogCallback(BaseDialogFragment.DialogEvent event) {
        if (event.tag.equals(TAG_DATABASE_ERROR_DIALOG)) {
            if (databaseDownloadErrorCount < 4) {
                downloadDatabase();
            } else {
                finish();
            }
        }
    }

    public boolean showInputLocation() {
        return showInputLocation;
    }

    @Override
    public void onTimeSelected(Date date, boolean departure) {
        optionTime = date;
        optionDeparture = departure;
        String optionText = departure ? strDepartAt : strArriveBy;

        String relative = Utils.dateToRelativeString(date,strTomorrow,strYesterday);

        if(!relative.isEmpty()){
            relative += " ";
        }

        txtOptions.setText(optionText + " " + relative + Constants.DATE_FORMAT_TIME.format(optionTime));

    }

    @Override
    public void onError(int errorCode) {
        if (errorCode == TRY_DOWNLOAD_DATABASE) {
            downloadDatabase();
        }
    }

    public static class PlayServicesErrorDialogFragment extends DialogFragment {
        public PlayServicesErrorDialogFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the errorText code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(EXTRA_PLAY_SERVICES_ERROR_CODE);
            return GoogleApiAvailability.getInstance().getErrorDialog(
                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MainActivity) getActivity()).onDialogDismissed();
        }
    }


}
