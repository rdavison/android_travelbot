package se.davison.travelbot.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.smartrecycleradapter.OnItemClickListener;
import se.davison.smartrecycleradapter.SectionAdapter;
import se.davison.smartrecycleradapter.SectionAdapterItem;
import se.davison.travelbot.R;
import se.davison.travelbot.adapters.adapteritems.JourneyFavoriteAdapterItem;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.ui.MainActivity;
import se.davison.travelbot.util.ExtendedSharedPreferences;
import se.davison.travelbot.util.Utils;


/**
 * Created by richard on 04/11/14.
 */
public class JourneyHistoryFragment extends RecyclerViewFragment<LinkedTreeMap<String,List<JourneyHistoryFragment.JourneyFavorite>>> {

    private View container;
    private int basePadding;

    public static class JourneyFavorite{

        public Stop origin;
        public Stop destination;

        public JourneyFavorite(Stop origin, Stop destination) {
            this.origin = origin;
            this.destination = destination;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JourneyFavorite that = (JourneyFavorite) o;

            if (origin != null ? !origin.equals(that.origin) : that.origin != null) return false;
            return !(destination != null ? !destination.equals(that.destination) : that.destination != null);

        }

        @Override
        public int hashCode() {
            int result = origin != null ? origin.hashCode() : 0;
            result = 31 * result + (destination != null ? destination.hashCode() : 0);
            return result;
        }
    }


    private ExtendedSharedPreferences prefs;
    private String strStop;
    private String strAddress;
    private SectionAdapter adapter;
    private ArrayList<JourneyHistoryFragment.JourneyFavorite> history;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }



    private static final String TAG = JourneyHistoryFragment.class.getSimpleName();



    public static JourneyHistoryFragment newInstance(){
        return new JourneyHistoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = ExtendedSharedPreferences.from(getContext());
        strStop = getString(R.string.stop);
        strAddress = getString(R.string.address);
        setRetainInstance(true);
    }


    @Override
    protected void setup(View container, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout) {

        int horizontalMargin = getResources().getDimensionPixelSize(R.dimen.card_wrap_horizontal_margin);

        adapter = new SectionAdapter(getContext());

        this.container = container;
        this.basePadding = Utils.dpToPx(getContext(),((56*2) + 48)) + Utils.getStatusBarHeight(getContext());
        container.setPadding(0, basePadding , 0, 0);
        recyclerView.setBackgroundResource(R.drawable.card_inset);


        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup viewGroup, RecyclerView.ViewHolder viewHolder, View view, int position) {
                Activity activity = getActivity();
                if(activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity)activity;
                    JourneyFavoriteAdapterItem item = (JourneyFavoriteAdapterItem) adapter.getItem(position);
                    mainActivity.addStop(MainActivity.RESULT_STOP, item.getOrigin(),true,false,null);
                    mainActivity.addStop(MainActivity.RESULT_STOP,item.getDestination(),false,false,null);
                    mainActivity.showFabIfStopsSelected(200);
                }
            }
        });




        int dp8 = Utils.dpToPx(getContext(), 8);

        recyclerView.setPadding(horizontalMargin, dp8, horizontalMargin, Utils.getNavigationBarHeight(getContext())+dp8);


        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter.attachTo(recyclerView);
    }

    public void setContainerRelativeTopPadding(int padding){

        container.setPadding(container.getPaddingLeft(),basePadding+padding,container.getPaddingRight(),container.getPaddingBottom());
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public boolean onError(Throwable ex) {

        return true;
    }

    @Override
    public String getErrorString(Throwable ex) {
        return "Error";
    }

    @Override
    public String getEmptyString() {
        return "Empty string";
    }

    @Override
    protected void onComplete(LinkedTreeMap<String, List<JourneyHistoryFragment.JourneyFavorite>> result) {


        ArrayList<AdapterItem> items = new ArrayList<>(100);

        for(String title : result.keySet()){

            List<JourneyHistoryFragment.JourneyFavorite> journeys = result.get(title);
            if(journeys.size()>0) {
                items.add(new SectionAdapterItem(title));
            }
            for(JourneyHistoryFragment.JourneyFavorite fav : journeys){
                items.add(new JourneyFavoriteAdapterItem(fav.origin,fav.destination,strStop,strAddress));
            }

        }

        adapter.setItems(items);
        adapter.notifyDataSetChanged();

    }

    @Override
    protected boolean scrollRefreshEnabled() {
        return false;
    }

    @Override
    public Observable<LinkedTreeMap<String, List<JourneyHistoryFragment.JourneyFavorite>>> mainObservable() {

        return Observable.create(new Observable.OnSubscribe<LinkedTreeMap<String, List<JourneyHistoryFragment.JourneyFavorite>>>() {
            @Override
            public void call(Subscriber<? super LinkedTreeMap<String, List<JourneyHistoryFragment.JourneyFavorite>>> subscriber) {

                ArrayList<JourneyHistoryFragment.JourneyFavorite> favorites = new ArrayList<>(prefs.getList(MainActivity.PREF_KEY_FAVORITE_JOURNEYS, new ArrayList<JourneyHistoryFragment.JourneyFavorite>(20), JourneyHistoryFragment.JourneyFavorite.class));
                history = new ArrayList<>(prefs.getList(MainActivity.PREF_KEY_HISTORY_JOURNEYS, new ArrayList<JourneyHistoryFragment.JourneyFavorite>(20),JourneyHistoryFragment.JourneyFavorite.class));

                LinkedTreeMap<String, List<JourneyHistoryFragment.JourneyFavorite>> favoritesHistoryMap = new LinkedTreeMap<>();
                favoritesHistoryMap.put(getString(R.string.favorites),favorites);
                favoritesHistoryMap.put(getString(R.string.history),history);
                subscriber.onNext(favoritesHistoryMap);
                subscriber.onCompleted();
            }
        });
    }

    public void addHistory(Stop origin, Stop destination) {
        JourneyFavorite fav = new JourneyFavorite(origin, destination);
        history.remove(fav);
        history.add(0,fav);
        prefs.edit().putList(MainActivity.PREF_KEY_HISTORY_JOURNEYS,history).apply();
    }
}
