package se.davison.travelbot.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlaceHolderFragment extends Fragment {


	private static final String EXTRA_TEXT = "extra_text";

	public static final PlaceHolderFragment newInstance(String text){
		PlaceHolderFragment instance = new PlaceHolderFragment();
		Bundle args = new Bundle(1);
		args.putString(EXTRA_TEXT, text);
		instance.setArguments(args);	
		return instance;
	}

	public static final PlaceHolderFragment newInstance(){
        return newInstance("Dummy text, empty constructor");
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {	
		TextView textView = new TextView(getActivity());
		textView.setGravity(Gravity.CENTER);



		textView.setText(getArguments().getString(EXTRA_TEXT));
		return textView;
	}
	
	
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}

}
