package se.davison.travelbot.ui.fragments;

import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.TreeSet;

import rx.Observable;
import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.smartrecycleradapter.OnItemClickListener;
import se.davison.smartrecycleradapter.ProgressAdapterItem;
import se.davison.smartrecycleradapter.SectionAdapter;
import se.davison.smartrecycleradapter.SectionAdapterItem;
import se.davison.travelbot.R;
import se.davison.travelbot.adapters.adapteritems.JourneyAdapterItem;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.net.JourneyResponse;
import se.davison.travelbot.net.RestClient;
import se.davison.travelbot.ui.MapActivity;
import se.davison.travelbot.util.Constants;
import se.davison.travelbot.util.RxUtils;
import se.davison.travelbot.util.Utils;

/**
 * Created by richard on 19/09/15.
 */
public class JourneyFragment extends RecyclerViewFragment<JourneyResponse> {

    private String TAG = JourneyFragment.class.getSimpleName();

    public static final String EXTRA_ORIGIN = "extra_origin";
    public static final String EXTRA_DESTINATION = "extra_destination";
    public static final String EXTRA_DATE = "extra_date";
    public static final String EXTRA_SEARCH_FOR_ARRIVAL = "extra_search_for_arrival";

    private Stop origin;
    private Stop destination;
    private Date date;
    private Boolean searchForArrival;
    private SectionAdapter adapter;
    private String strFrom;
    private String strLoadEarlier;
    private String strLoadLater;
    private ProgressAdapterItem progressEarlier;
    private ProgressAdapterItem progressLater;
    //private List<Journey> journeys;
    private RxUtils.OperatorCountDownLatch delayedOperator = new RxUtils.OperatorCountDownLatch(1);

    private ArrayMap<Long, TreeSet<Journey>> journeyMap = new ArrayMap<>(10);

    ArrayList<AdapterItem> items = new ArrayList<>(1000);
    private LinearLayoutManager layoutManager;


    @Override
    public boolean refreshOnScroll() {
        return false;
    }


    public static JourneyFragment newInstance(Stop origin, Stop destination, Date date, Boolean searchForArrival) {
        JourneyFragment fragment = new JourneyFragment();

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_ORIGIN, origin);
        args.putSerializable(EXTRA_DESTINATION, destination);
        args.putSerializable(EXTRA_DATE, date);
        args.putBoolean(EXTRA_SEARCH_FOR_ARRIVAL, searchForArrival);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected boolean scrollRefreshEnabled() {
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getArguments();

        int navbarHeight = Utils.getNavigationBarHeight(getContext());

        origin = (Stop) extras.getSerializable(EXTRA_ORIGIN);
        destination = (Stop) extras.getSerializable(EXTRA_DESTINATION);
        date = (Date) extras.getSerializable(EXTRA_DATE);
        searchForArrival = extras.getBoolean(EXTRA_SEARCH_FOR_ARRIVAL);

        strFrom = getString(R.string.from);
        strLoadEarlier = getString(R.string.load_earlier);
        strLoadLater = getString(R.string.load_later);

        progressEarlier = new ProgressAdapterItem(getContext(),strLoadEarlier,0x8a000000);
        progressLater = new ProgressAdapterItem(getContext(),strLoadLater,0x8a000000, 64,new int[]{0,0,0,navbarHeight});





    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }


    @Override
    public void setup(View container, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout) {

        layoutManager = new LinearLayoutManager(getContext());

        int horizontalMargin = getContext().getResources().getDimensionPixelSize(R.dimen.activity_horizontal_margin);
        int dp8 = Utils.dpToPx(getContext(), 8);



        recyclerView.setPadding(horizontalMargin, dp8, horizontalMargin, Utils.getNavigationBarHeight(getContext())+dp8);
        recyclerView.setBackgroundResource(R.drawable.card_inset);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setClipToPadding(false);

        adapter = new SectionAdapter(getContext());
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup viewGroup, RecyclerView.ViewHolder viewHolder, View view, int position) {
                AdapterItem item = adapter.getItem(position);
                if(item == progressEarlier && progressEarlier.isEnabled()){

                    if(adapter.getItem(position+1) instanceof SectionAdapterItem){
                        position++;
                    }
                    JourneyAdapterItem journeyAdapterItem = (JourneyAdapterItem) adapter.getItem(position+1);
                    progressEarlier.setLoading(true);
                    date = new Date(journeyAdapterItem.getJourney().getLegs().get(0).getOrigin().getDate().getTime()-60000);
                    Log.d(TAG, "SEARCH DATE"+ Constants.DATE_FORMAT_FULL.format(date));
                    searchForArrival = true;
                    loadData();
                }else if(item == progressLater && progressLater.isEnabled()) {
                    progressLater.setLoading(true);
                    JourneyAdapterItem journeyAdapterItem = (JourneyAdapterItem) adapter.getItem(position-1);
                    date = new Date(journeyAdapterItem.getJourney().getLegs().get(0).getOrigin().getDate().getTime()+60000);
                    Log.d(TAG, "SEARCH DATE"+Constants.DATE_FORMAT_FULL.format(date));
                    searchForArrival = false;
                    loadData();
                }else if(item instanceof JourneyAdapterItem){

                    MapActivity.startActivity(getContext(), ((JourneyAdapterItem)item).getJourney());
                }
            }
        });

        adapter.setSeparator(0x1f000000, 0,0);
        adapter.attachTo(recyclerView);
    }





    @Override
    public String getErrorString(Throwable ex) {
        if(ex instanceof RestClient.ServiceException){
            return ex.getMessage();
        }
        return getString(R.string.err_fetch_journeys);
    }

    public void showData(){
        delayedOperator.countDown();
    }

    @Override
    public String getEmptyString() {
        return getString(R.string.no_journeyes);
    }

    @Override
    public Observable<JourneyResponse> mainObservable() {
        return RestClient.getInstance().journey(origin, destination, date, searchForArrival).lift(delayedOperator);
    }

    @Override
    protected void onCompleteBackground(JourneyResponse journeyResponse) {

        Date now = Utils.removeTime(new Date());

        if(journeyResponse.getJourneys()!=null) {
            for (Journey j : journeyResponse.getJourneys()) {
                j.calculate();

                long key = Utils.removeTime(j.getLegs().get(0).getOrigin().getDate()).getTime();

                if(!journeyMap.containsKey(key)){
                    journeyMap.put(key, new TreeSet<Journey>());
                }
                journeyMap.get(key).add(j);
            }
        }

        items.clear();
        items.add(progressEarlier);



        for(int i = 0; i < journeyMap.size(); i++){

            Date date = new Date(journeyMap.keyAt(i));

            if(journeyMap.size()>1){
                items.add(new SectionAdapterItem(Constants.DATE_FORMAT_DATE.format(date)));
            }

            TreeSet<Journey> journeys = journeyMap.get(date.getTime());
            for (Journey j : journeys){
                items.add(new JourneyAdapterItem(j,strFrom));
            }

        }
        items.add(progressLater);

        super.onCompleteBackground(journeyResponse);
    }

    @Override
    protected boolean onError(Throwable ex) {


        return super.onError(ex);

    }

    @Override
    protected void toggleList(boolean show) {
            super.toggleList(show);


    }

    @Override
    protected void onComplete(JourneyResponse journeyResponse) {

        progressEarlier.setLoading(false);
        progressEarlier.setEnabled(true);
        progressLater.setLoading(false);
        progressLater.setEnabled(true);

        adapter.setItems(items);
        adapter.notifyDataSetChanged();


    }

    public void reload() {
        int firstPos = layoutManager.findFirstVisibleItemPosition();
        AdapterItem item = null;
        while(!(item instanceof JourneyAdapterItem)){
            item = items.get(firstPos);
            firstPos++;
        }
        date = new Date(((JourneyAdapterItem) item).getJourney().getLegs().get(0).getOrigin().getDate().getTime()-60000);
        searchForArrival = false;
        loadData();
    }
}
