package se.davison.travelbot.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;

import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.TransportMode;

/**
 * Created by richard on 01/08/16.
 */
public class JourneyPathView extends JourneyView {


    private static final int TEXT_COLOR = 0xDE000000;
    private static final int ICON_COLOR = 0x8A000000;


    private static final int WIDTH = 16;
    private static final int STROKE_WIDTH = 2;
    private static final int STATE_START = 1;
    private static final int STATE_END = 1 << 1;
    private static final int STATE_CONNECTION = 1 << 2;
    private static final int STATE_FINAL = 1 << 3;


    private Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int dp2;
    private int dp4;
    private int dp8;
    private int dp16;
    private Leg previousLeg;
    private Leg leg;
    private int state;


    public JourneyPathView(Context context) {
        this(context,null);
    }

    public JourneyPathView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public JourneyPathView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context,attrs,defStyleAttr);
        init(context, attrs, defStyleAttr,0);
    }



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public JourneyPathView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context,attrs,defStyleAttr,defStyleRes);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {

        dp16 = (int) (dp*16);
        dp2 = (int)(dp*2);
        dp4 = (int)(dp*4);
        dp8 = (int)(dp*8);

        //leg.setFgColor(0xFFFF0000);
        state = STATE_START;

    }

    public Leg getPreviousLeg() {
        return previousLeg;
    }

    public void setPreviousLeg(Leg previousLeg) {
        this.previousLeg = previousLeg;
        if(previousLeg!=null && state != (STATE_END | STATE_FINAL)) {
            if (leg.getTransportMode() != TransportMode.WALK && previousLeg.getTransportMode() != TransportMode.WALK) {
                state = STATE_CONNECTION;
            }
            if (leg.getTransportMode() == TransportMode.WALK && previousLeg.getTransportMode() != TransportMode.WALK) {
                state = STATE_END;
            }
        }
    }





    public Leg getLeg() {
        return leg;
    }


    public void setLeg(Leg leg, boolean finalLeg){
        this.leg = leg;
        state = STATE_END | STATE_FINAL;
    }

    public void setLeg(Leg leg) {
        this.leg = leg;
    }

    Path path = new Path();

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(leg!=null) {

            int radius = (int) (WIDTH / 2 * dp);
            int width = (int) (WIDTH * dp);

            paint.setStrokeWidth((int) dp * STROKE_WIDTH);
            paint.setStyle(Paint.Style.FILL);


            paint.setColor(leg.getFgColor());

            if (leg.getTransportMode() != TransportMode.WALK || previousLeg!=null) {

                for (int i = 0; i < (state == STATE_CONNECTION ? 2 : 1); i++) {

                    boolean rotate = i > 0 || state == STATE_END || state == (STATE_END | STATE_FINAL);



                    if (rotate) {
                        canvas.save();
                        canvas.translate(width/2, width/2);
                        canvas.rotate(180);
                        canvas.translate(-width/2, -width/2);
                        paint.setColor(previousLeg.getFgColor());
                    }


                    path.moveTo(0, radius);
                    path.lineTo(radius / 2, width - (2 * dp));
                    path.lineTo(radius, width);
                    path.lineTo(width - (width / 4), width - (2 * dp));
                    path.lineTo(width, radius);
                    path.lineTo(width, width);
                    path.lineTo(0, width);
                    canvas.drawPath(path, paint);

                    if(rotate){
                        canvas.restore();
                    }

                }
            }

            paint.setColor(state != STATE_END ? leg.getFgColor(): previousLeg.getFgColor());


            paint.setStyle(Paint.Style.STROKE);





            canvas.drawCircle(radius, radius, radius - (paint.getStrokeWidth() / 2), paint);
            paint.setStyle(Paint.Style.FILL);

            if(state != (STATE_END | STATE_FINAL)) {


                if (leg.getTransportMode() != TransportMode.WALK) {
                    canvas.drawRect(0, WIDTH * dp, WIDTH * dp, getHeight(), paint);
                } else {
                    paint.setColor(0x61000000);
                    int dots = (int) Math.floor(getHeight()/ (WIDTH * dp));
                    for (int i = 0; i < dots; i++) {
                        canvas.drawCircle(WIDTH / 2 * dp, (WIDTH * dp) + 12 * dp + (WIDTH * dp) * i, WIDTH / 4 * dp, paint);
                    }
                }
            }

        }

    }



    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = dp16+dp4;
        int desiredHeight = dp16+dp4;


        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }
}
