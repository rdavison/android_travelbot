package se.davison.travelbot.ui.widget;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by richard on 20/02/16.
 */
public class ExtendedSnackbar{


    public static Snackbar extend(Snackbar snackbar, int height) {

        View view = (Snackbar.SnackbarLayout) snackbar.getView();
        view.setPadding(view.getPaddingLeft(),view.getPaddingTop(),view.getPaddingRight(),view.getPaddingBottom()+height);
        return snackbar;
    }
}
