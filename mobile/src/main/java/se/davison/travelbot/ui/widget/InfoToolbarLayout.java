package se.davison.travelbot.ui.widget;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

/**
 * Created by richard on 01/11/15.
 */
public class InfoToolbarLayout extends ViewGroup {

    private static final String TAG = InfoToolbarLayout.class.getSimpleName();
    private AppBarLayout.OnOffsetChangedListener onOffsetChangedListener;
    private boolean layoutCalled;

    public interface Callback{
        void hideShowFab(boolean visible);
    }

    private WindowInsetsCompat lastInsets;
    private int currentOffset;
    private View lastChild;
    private View backgroundChild;
    private View overlayChild;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private TextView toolbarSubtitle;
    private ValueAnimator titleAnimator;
    private float titleAlpha = 1f;
    private TimeInterpolator fastOutSlowInInterpolator;
    private boolean titleIsShown = true;
    private int lastChildY;
    private float paralaxFactor = 0.7f;
    private int dp8;
    private GradientDrawable toolbarFade;
    private Paint toolbarBackgroundPaint = new Paint();
    private Callback callback;

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public InfoToolbarLayout(Context context) {
        this(context, null);
    }

    public InfoToolbarLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InfoToolbarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        dp8 = (int) (context.getResources().getDisplayMetrics().density*8);

        fastOutSlowInInterpolator = android.view.animation.AnimationUtils.loadInterpolator(getContext(),android.R.interpolator.fast_out_slow_in);

        setWillNotDraw(false);

        ViewCompat.setOnApplyWindowInsetsListener(this,
                new android.support.v4.view.OnApplyWindowInsetsListener() {
                    @Override
                    public WindowInsetsCompat onApplyWindowInsets(View v,
                                                                  WindowInsetsCompat insets) {
                        lastInsets = insets;
                        requestLayout();
                        return insets.consumeSystemWindowInsets();
                    }
                });


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int width = MeasureSpec.getSize(widthMeasureSpec);

        //recalculate(width);

        final int desiredHeight = getDesiredHeight();
        int height;
        switch (MeasureSpec.getMode(heightMeasureSpec)) {
            case MeasureSpec.EXACTLY:
                height = MeasureSpec.getSize(heightMeasureSpec);
                break;
            case MeasureSpec.AT_MOST:
                height = Math.min(desiredHeight, MeasureSpec.getSize(heightMeasureSpec));
                break;
            default: // MeasureSpec.UNSPECIFIED
                height = desiredHeight;
                break;
        }
        setMeasuredDimension(width, height);
        measureChildren(widthMeasureSpec, MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST));
    }

    private int getDesiredHeight() {
        int totalHeight  = 0;
        for (int i = 0, z = getChildCount(); i < z; i++) {
            final View child = getChildAt(i);
            measureChild(child, View.MeasureSpec.EXACTLY,
                    View.MeasureSpec.EXACTLY);
            //child.measure( View.MeasureSpec.EXACTLY, View.MeasureSpec.EXACTLY);
            int childHeight = child.getMeasuredHeight();
            totalHeight += childHeight;
            if(child instanceof Toolbar){
                totalHeight-=childHeight;
            }
            if(i == 1 && z > 2){
                totalHeight-=childHeight;
            }

        }

        return totalHeight;
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int insets = t;
        if(lastInsets!=null){
            insets+=lastInsets.getSystemWindowInsetTop();
        }
        int offset = t;
        int lastLayoutY = 0;



        if(changed) {

            layoutCalled = true;

            for (int i = 0, z = getChildCount(); i < z; i++) {
                final View child = getChildAt(i);

                int layoutY = offset;
                boolean skipMeasure = false;
                int childHeight = child.getMeasuredHeight();

                if (child instanceof Toolbar) {

                    toolbar = (Toolbar) child;

                    for (int ii = 0; ii < toolbar.getChildCount(); ii++) {
                        View toolbarChild = toolbar.getChildAt(ii);
                        if (toolbarChild instanceof TextView) {
                            if (toolbarTitle == null) {
                                toolbarTitle = (TextView) toolbarChild;

                            } else {
                                toolbarSubtitle = (TextView) toolbarChild;
                            }
                        }
                    }

                    layoutY = insets;
                    skipMeasure = true;
                }
                if (i == 0) {
                    backgroundChild = child;
                }

                if (i == 1) {
                    overlayChild = child;
                    layoutY = layoutY - child.getMeasuredHeight();
                    skipMeasure = true;
                }
                if (i == z - 1) {
                    lastChild = child;
                    if (lastChild == toolbar) {
                        lastChild = getChildAt(z - 2);
                        lastChildY = lastLayoutY;
                    }
                }

                child.layout(l, layoutY, r, layoutY + child.getMeasuredHeight());
                lastLayoutY = layoutY;

                if (!skipMeasure) {
                    offset += childHeight;
                }
            }
            try {
                ColorDrawable drawable = (ColorDrawable) lastChild.getBackground();
                toolbarBackgroundPaint.setColor(drawable.getColor());
                toolbarFade = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{drawable.getColor(), Color.TRANSPARENT});
                toolbarFade.setBounds(0, 0, getWidth(), dp8);

            } catch (Exception ex) {
                ex.printStackTrace();
                throw new IllegalStateException("Child before toolbar needs to have a color background");
            }
            setMinimumHeight(toolbar.getHeight());
        }
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        // Add an OnOffsetChangedListener if possible
        final ViewParent parent = getParent();
        if (parent instanceof AppBarLayout) {
            if (onOffsetChangedListener == null) {
                onOffsetChangedListener = new OffsetUpdateListener();
            }
            ((AppBarLayout) parent).addOnOffsetChangedListener(onOffsetChangedListener);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        // Remove our OnOffsetChangedListener if possible and it exists
        final ViewParent parent = getParent();
        if (onOffsetChangedListener != null && parent instanceof AppBarLayout) {
            ((AppBarLayout) parent).removeOnOffsetChangedListener(onOffsetChangedListener);
        }

        super.onDetachedFromWindow();
    }

    private void animateTitle(float targetAlpha) {
        if (titleAnimator == null) {
            titleAnimator = new ValueAnimator();
            titleAnimator.setDuration(400);
            titleAnimator.setInterpolator(fastOutSlowInInterpolator);
            titleAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animator) {
                    setTitleAlpha((float) animator.getAnimatedValue());
                }
            });
        } else if (titleAnimator.isRunning()) {
            titleAnimator.cancel();
        }

        titleAnimator.setFloatValues(titleAlpha,targetAlpha);
        titleAnimator.start();
    }

    public void setTitleShown(boolean shown) {
        setTitleShown(shown, ViewCompat.isLaidOut(this) && !isInEditMode());
    }

    public void setTitleShown(boolean shown, boolean animate) {
        if (titleIsShown != shown) {
            if (animate) {
                animateTitle(shown ? 1f : 0);
            } else {
                setTitleAlpha(shown ?1f : 0);
            }
            titleIsShown = shown;
        }
    }

    private void setTitleAlpha(float alpha) {
        if (alpha != titleAlpha && toolbar!=null) {
            toolbarTitle.setAlpha(alpha);
            toolbarSubtitle.setAlpha(alpha);
            titleAlpha = alpha;
            ViewCompat.postInvalidateOnAnimation(toolbar);
        }
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        // This is a little weird. Our scrim needs to be behind the Toolbar (if it is present),
        // but in front of any other children which are behind it. To do this we intercept the
        // drawChild() call, and draw our scrim first when drawing the toolbar



        final int insetTop = lastInsets != null ? lastInsets.getSystemWindowInsetTop() : 0;

        if (child == toolbar && getHeight() + currentOffset < insetTop + toolbar.getHeight() + lastChild.getHeight()) {
            int offset = toolbar.getHeight()+lastInsets.getSystemWindowInsetTop()-currentOffset;
            toolbarFade.setBounds(0, offset, getWidth(), offset + dp8);
            toolbarFade.draw(canvas);
            canvas.drawRect(0,getHeight()-lastChild.getHeight(),getWidth(),offset,toolbarBackgroundPaint);
        }

        // Carry on drawing the child...
        return super.drawChild(canvas, child, drawingTime);
    }



    private class OffsetUpdateListener implements AppBarLayout.OnOffsetChangedListener {
        private float lastAlpha = -1;

        @Override
        public void onOffsetChanged(AppBarLayout layout, int verticalOffset) {
            currentOffset = verticalOffset;

            final int insetTop = lastInsets != null ? lastInsets.getSystemWindowInsetTop() : 0;
            final int scrollRange = layout.getTotalScrollRange();
            final int scrollY = -currentOffset;

            boolean showTitle = getHeight() + verticalOffset <= insetTop + toolbar.getHeight();


            for (int i = 0, z = getChildCount(); i < z; i++) {
                final View child = getChildAt(i);
                final LayoutParams lp = (LayoutParams) child.getLayoutParams();

                if(child instanceof Toolbar){
                    toolbar.layout(0,insetTop-verticalOffset,child.getWidth(),insetTop+child.getHeight()-verticalOffset);

                }
                if(child == backgroundChild){
                    child.layout(0,(int)(-(((float)verticalOffset)*paralaxFactor)),getWidth(),backgroundChild.getHeight()-(int)(((float)verticalOffset)*paralaxFactor));
                }

                if(child == lastChild && showTitle){

                }
            }

            int distance = currentOffset+backgroundChild.getHeight()- overlayChild.getHeight()-toolbar.getHeight();
            float alpha = Math.min(Math.max((distance + toolbar.getHeight() / 4) / (float) (toolbar.getHeight() / 2), 0), 1);
            overlayChild.setAlpha(alpha);

            if(layoutCalled && alpha != lastAlpha && callback!=null && (alpha==0 || alpha == 1)){

                callback.hideShowFab(alpha==1);
            }

            lastAlpha = alpha;

            setTitleShown(showTitle);

            if (Math.abs(verticalOffset) == scrollRange) {
                // If we have some pinned children, and we're offset to only show those views,
                // we want to be elevate
                ViewCompat.setElevation(layout, layout.getTargetElevation());
            } else {
                // Otherwise, we're inline with the content
                ViewCompat.setElevation(layout, 0f);
            }
        }
    }




}
