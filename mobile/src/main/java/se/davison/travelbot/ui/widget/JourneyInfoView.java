package se.davison.travelbot.ui.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.util.SparseIntArray;

import se.davison.travelbot.R;
import se.davison.travelbot.model.Journey;
import se.davison.travelbot.model.Leg;
import se.davison.travelbot.model.TransportMode;

/**
 * Created by richard on 19/09/15.
 */
public class JourneyInfoView extends JourneyView {

    private static final BitmapFactory.Options BITMAP_OPTIONS;

    static {
        BITMAP_OPTIONS = new BitmapFactory.Options();
        BITMAP_OPTIONS.inPreferredConfig = Bitmap.Config.ARGB_8888;
    }

    private Bitmap icArrow = BitmapFactory.decodeResource(getResources(), R.drawable.ic_arrow_right_24dp, BITMAP_OPTIONS);

    private static final String TAG = JourneyInfoView.class.getSimpleName();
    private Journey journey;
    private int textSize;
    private SparseIntArray legOffsetX = new SparseIntArray(20);
    private SparseIntArray legOffsetY = new SparseIntArray(20);
    private SparseArray<Bitmap> icons;



    //private int dp24;
    //private int dp8;
    private int dp2;

    private int textBgHeight;
    private int textPadding;
    private int iconSize;
    private RectF bgRect = new RectF(0,0,0,0);

    public void setJourney(Journey journey) {
        this.journey = journey;
        requestLayout();
        invalidate();
    }

    private Paint textPaint;
    private Paint textBgPaint;


    public JourneyInfoView(Context context) {
        super(context);
        init(context);
    }

    public JourneyInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public JourneyInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public JourneyInfoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        float sp = displayMetrics.scaledDensity;

        icons = TransportMode.bitmaps(context);


        textBgHeight = (int) (18*dp);
        textPadding = (int) (4*dp);
        iconSize = (int) (24*dp);



        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setTextSize(14*sp);

        textBgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textBgPaint.setStyle(Paint.Style.FILL);

        //dp24 = (int) (24 * dp);
        //dp8 = (int) (8 * dp);
        dp2 = (int) (2 * dp);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        if (journey != null) {

            int index = 0;
            int x = 0;
            int y = 0;
            int arrowOffset = 0;
            int rowIndex = 0;
            Bitmap icon;
            for (Leg leg : journey.getLegs()) {
                icon = icons.get(leg.getTransportMode().toResourceId());

                if(legOffsetY.get(index) > y){
                    rowIndex++;
                }

                x = legOffsetX.get(rowIndex+index)+getPaddingLeft();
                y = legOffsetY.get(index)+getPaddingTop();
                arrowOffset = legOffsetX.get(rowIndex+index + 1) - iconSize;


                canvas.drawBitmap(icon, x, y, null);

                if(arrowOffset>x + iconSize) {
                    textBgPaint.setColor(leg.getFgColor());

                    bgRect.left = x + iconSize;
                    bgRect.top = y + (dp2 * 2);
                    bgRect.right = arrowOffset;
                    bgRect.bottom = y + (dp2 * 2) + textBgHeight;

                    if (Build.VERSION.SDK_INT > 21) {
                        canvas.drawRoundRect(bgRect, dp2, dp2, textBgPaint);
                    } else {
                        canvas.drawRect(bgRect, textBgPaint);
                    }
                    textPaint.setColor(leg.getBgColor());

                    float textY = y + dp2 + (iconSize - (dp2)) / 2 - (textPaint.descent() + textPaint.ascent()) / 2;

                    canvas.drawText(leg.getLine(), x + iconSize + textPadding, textY, textPaint);
                }
                if(index < journey.getLegs().size()-1) {
                    canvas.drawBitmap(icArrow, arrowOffset, y, null);
                }

                index++;
            }
        }
    }




    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {


        super.onMeasure(widthMeasureSpec, heightMeasureSpec);


        int paddingW = getPaddingLeft() + getPaddingRight();
        int minW = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();

        int w = Math.max(minW, MeasureSpec.getSize(widthMeasureSpec));

        int rowW = -dp2;
        int index = 0;
        int newRowW;
        int rowH = 0;
        int rowIndex = 0;
        if (journey != null) {
            for (Leg leg : journey.getLegs()) {
                //24 x 2 + 8 x 2

                //int textWidth = 0;
                if(leg.getTransportMode()== TransportMode.WALK){
                    newRowW = (iconSize*2);
                }else{
                    newRowW = (int) (((iconSize +textPadding)*2)  + textPaint.measureText(leg.getLine()));
                }


                if (rowW + newRowW + paddingW > w) {

                    rowH += iconSize + (int) (4 * dp);
                    ; //image height 24dp + 4 in margin

                    legOffsetX.put(rowIndex+index, rowW);
                    rowW = -dp2;
                    rowIndex++;
                }
                legOffsetY.put(index, rowH);
                legOffsetX.put(rowIndex+index, rowW);
                rowW += newRowW;

                index++;
            }
            //for last offset
            legOffsetX.put(rowIndex + index, rowW);
        }


        int minH = iconSize + rowH +getPaddingTop()+ getPaddingBottom();
        int heightSpec = MeasureSpec.getSize(heightMeasureSpec);
        int h = Math.max(heightSpec, minH);
        if(heightSpec>0){
            h = Math.min(heightSpec, minH);
        }


        setMeasuredDimension(w, h);

    }

    @Override
    public void setPadding(int left, int top, int right, int bottom) {
        super.setPadding(left, top, right, bottom);
        //measure(widthMeasureSpec,heightMeasureSpec);
        invalidate();
    }

    @Override
    public void setPaddingRelative(int start, int top, int end, int bottom) {
        super.setPaddingRelative(start, top, end, bottom);
        //measure(widthMeasureSpec,heightMeasureSpec);
        invalidate();
    }
}
