package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import se.davison.travelbot.R;

/**
 * Created by richard on 10/09/15.
 */
public class MarqueeView extends View {
    private long start;

    private static final int FPS = 60;
    private static final int SPEED = 10;

    private Drawable image;

    public MarqueeView(Context context) {
        super(context);
    }

    public MarqueeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.MarqueeView,
                0, 0);

        image = a.getDrawable(R.styleable.MarqueeView_src);

        a.recycle();

        start = System.currentTimeMillis();
        this.postInvalidate();
    }

    int offset=0;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(image!=null) {

            long elapsedTime = System.currentTimeMillis()-start;





            image.setBounds(offset, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            image.draw(canvas);

            image.setBounds(offset + image.getIntrinsicWidth(), 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            image.draw(canvas);

            offset -= 1;
            if(offset<-image.getIntrinsicWidth()){
                offset = 0;
            }

            if(elapsedTime<SPEED){
                postInvalidateDelayed(1000/FPS);
            }

            //this.invalidate();
        }
    }
}
