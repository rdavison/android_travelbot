package se.davison.travelbot.ui.fragments.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import se.davison.travelbot.R;
import se.davison.travelbot.adapters.RecyclingPagerAdapter;
import se.davison.travelbot.util.Constants;
import se.davison.travelbot.util.Utils;

/**
 * Created by admin on 2015-08-24.
 */
public class TimePickerDialogFragment extends AppCompatDialogFragment {

    private static final String EXTRA_DATE = "extra_date";
    private ViewPager pager;
    private NumberPicker pkrHour;
    private NumberPicker pkrMinute;
    private boolean departure = true;
    private Date date;

    public interface Callback{
        void onTimeSelected(Date date, boolean departure);
    }

    public static TimePickerDialogFragment newInstance(){
        TimePickerDialogFragment fragment = new TimePickerDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);



        Context context = getContext();
        View root = LayoutInflater.from(context).inflate(R.layout.dialog_time_picker, null);



        pkrHour = (NumberPicker) root.findViewById(R.id.pkr_hour);
        pkrMinute = (NumberPicker) root.findViewById(R.id.pkr_minute);
        pager = (ViewPager)root.findViewById(R.id.view_pager);
        final TabLayout tabLayout = (TabLayout)root.findViewById(R.id.tab_layout);
        final ImageView imgNow = (ImageView)root.findViewById(R.id.img_now);
        final ImageView imgNext = (ImageView)root.findViewById(R.id.img_next);
        final ImageView imgPrevious = (ImageView)root.findViewById(R.id.img_previous);

        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{R.attr.colorPrimary});
        int primaryColor = a.getColor(0, 0);

        a.recycle();


        String[] hours = new String[24];
        String[] minutes = new String[60];

        for(int i = 0; i < 24;i++){
            hours[i] = String.format("%02d", i);
        }

        for(int i = 0; i < 60;i++){
            minutes[i] = String.format("%02d", i);
        }


        setupPicker(pkrHour, hours, primaryColor);
        setupPicker(pkrMinute,minutes,primaryColor);



        final TabLayout.Tab arriveTab = tabLayout.newTab();
        arriveTab.setText(R.string.arrive_by);

        final TabLayout.Tab departTab = tabLayout.newTab();
        departTab.setText(R.string.depart_at);

        tabLayout.addTab(departTab, true);
        tabLayout.addTab(arriveTab);

        final Adapter adapter = new Adapter(context);

        pager.setAdapter(adapter);
        setDate(date);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.equals(departTab)){
                    departure = true;
                }else{
                    departure = false;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        imgNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(null);
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem() + 1, true);
            }
        });

        imgPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pager.setCurrentItem(pager.getCurrentItem()-1,true);
            }
        });


        return new AlertDialog.Builder(context).setView(root).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                Activity activity = getActivity();
                if(activity instanceof Callback){


                    int diff =  (pager.getCurrentItem()-Adapter.START_VALUE);
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DAY_OF_MONTH,diff);
                    cal.set(Calendar.HOUR_OF_DAY,pkrHour.getValue());
                    cal.set(Calendar.MINUTE,pkrMinute.getValue());
                    ((Callback) activity).onTimeSelected(cal.getTime(), departure);
                }
            }
        }).setNegativeButton(R.string.cancel, null).create();
    }

    private void setupPicker(NumberPicker picker, String[] displayedValues, int color) {

        setDividerColor(picker, color);
        setNumberPickerTextColor(picker, Color.BLACK);
        picker.setDisplayedValues(displayedValues);
        picker.setMaxValue(-1 + displayedValues.length);
    }

    public static void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException | Resources.NotFoundException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {
        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText){
                try{
                    Field selectorWheelPaintField = numberPicker.getClass()
                            .getDeclaredField("mSelectorWheelPaint");
                    selectorWheelPaintField.setAccessible(true);
                    ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
                    ((EditText)child).setTextColor(color);
                    //((EditText)child).setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
                    numberPicker.invalidate();
                    return;
                }
                catch(NoSuchFieldException | IllegalAccessException | IllegalArgumentException e){

                }
            }
        }
    }

    public void setDate(Date date) {

        this.date = date;


        if(pager!=null) {

            Calendar cal = Calendar.getInstance();
            int diff = 0;
            if(date!=null) {
                cal.setTime(date);

                long today = new Date().getTime();
                today -= today%86400000;

                long due = date.getTime();
                due -= due%86400000;

                diff = (int)((due-today)/86400000);


            }

            pkrHour.setValue(cal.get(Calendar.HOUR_OF_DAY));
            pkrMinute.setValue(cal.get(Calendar.MINUTE));
            pager.setCurrentItem(Adapter.START_VALUE+diff,true);



        }

    }

    private static class Adapter extends RecyclingPagerAdapter {

        public static final int START_VALUE = 10;

        private final Context context;
        private final DateFormat dateformat;

        private final String strToday;
        private final String strTomorrow;
        private final String strYesterday;




        public Adapter(Context context) {
            this.context = context;
            strToday = context.getString(R.string.today);
            strTomorrow = context.getString(R.string.tomorrow);
            strYesterday = context.getString(R.string.yesterday);
            dateformat = android.text.format.DateFormat.getDateFormat(context);
        }




        @Override
        public int getCount() {
            return 365+START_VALUE;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup container) {

            if(convertView==null){

                TextView textView = new TextView(context);
                textView.setGravity(Gravity.CENTER);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                convertView = textView;
            }

            Calendar cal = Calendar.getInstance();

            cal.add(Calendar.DATE, position-START_VALUE);

            String relative = Utils.dateToRelativeString(cal.getTime(),strTomorrow,strYesterday);

            if(relative.isEmpty()){
                relative = strToday;
            }

            ((TextView)convertView).setText(relative);


            return convertView;
        }
    }


}
