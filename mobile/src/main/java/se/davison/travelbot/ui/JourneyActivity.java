package se.davison.travelbot.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;
import java.util.Random;

import se.davison.travelbot.R;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.ui.fragments.DepartureBoardFragment;
import se.davison.travelbot.ui.fragments.JourneyFragment;
import se.davison.travelbot.ui.widget.FromToView;
import se.davison.travelbot.ui.widget.InfoToolbarLayout;
import se.davison.travelbot.util.LUtils;
import se.davison.travelbot.util.LayerEnablingAnimatorListener;


/**
 * Created by richard on 08/09/15.
 */
public class JourneyActivity extends ToolbarActivity  implements AppBarLayout.OnOffsetChangedListener, Toolbar.OnMenuItemClickListener {

    public static final String EXTRA_ORIGIN = "extra_origin";
    public static final String EXTRA_DESTINATION = "extra_destination";
    public static final String EXTRA_DATE = "extra_date";
    public static final String EXTRA_SEARCH_FOR_ARRIVAL = "extra_search_for_arrival";

    private static final String STATE_HERO_ID = "state_hero_id";
    private static final int[] HERO_IMAGES = new int[] {R.drawable.hero_journey_1,R.drawable.hero_journey_2,R.drawable.hero_journey_3};


    private static final String TAG_FRAGMENT = "tag_fragment";

    private static final int ANIMATION_DURATION = 200;
    private View reveal;
    private JourneyFragment fragment;
    private int index = 0;
    private String strStop;
    private String strAddress;
    private int heroId;
    private boolean enableFab = false;

    public static void startActivity(Activity context, Stop origin, Stop destination, Date date, boolean searchForArrival){

        Intent intent = new Intent(context, JourneyActivity.class);
        intent.putExtra(EXTRA_ORIGIN,origin);
        intent.putExtra(EXTRA_DESTINATION,destination);
        intent.putExtra(EXTRA_DATE,date);
        intent.putExtra(EXTRA_SEARCH_FOR_ARRIVAL,searchForArrival);
        context.startActivity(intent);
        context.overridePendingTransition(0,0);
    }

    private Stop origin;
    private Stop destination;
    private Date date;
    private Boolean searchForArrival;

    ValueAnimator revealAnimator = ValueAnimator.ofInt(0,1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        origin = (Stop) extras.getSerializable(EXTRA_ORIGIN);
        destination = (Stop) extras.getSerializable(EXTRA_DESTINATION);
        date = (Date) extras.getSerializable(EXTRA_DATE);
        searchForArrival = extras.getBoolean(EXTRA_SEARCH_FOR_ARRIVAL);

        strStop = getString(R.string.stop);
        strAddress = getString(R.string.address);


        setContentView(R.layout.activty_journey);
        fixToolbarRipple();

        getSupportActionBar().setTitle(getString(R.string.journey));
        getToolbar().setSubtitle(Html.fromHtml(origin.getName() + " &#10145; " + destination.getName()));

        InfoToolbarLayout infoToolbarLayout = (InfoToolbarLayout) findViewById(R.id.cnt_info_toolbar);
        final AppBarLayout appBarLayout = (AppBarLayout)findViewById(R.id.cnt_app_bar);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        infoToolbarLayout.setCallback(new InfoToolbarLayout.Callback() {
            @Override
            public void hideShowFab(boolean visible) {
                if(enableFab) {
                    if (visible) {
                        fab.show();
                    } else {
                        fab.hide();
                    }
                }
            }
        });

        ImageView imgHero = (ImageView)findViewById(R.id.img_hero);
        ((FromToView)findViewById(R.id.cnt_info)).setStops(origin,destination);





        reveal = findViewById(R.id.view_reveal);

        getToolbar().setOnMenuItemClickListener(this);





        FragmentManager fm = getSupportFragmentManager();

        if(savedInstanceState==null){

            reveal.setVisibility(View.VISIBLE);
            reveal.setPivotY(0);
            reveal.post(new Runnable() {
                @Override
                public void run() {
                    reveal.animate().alpha(0).setDuration(ANIMATION_DURATION).setInterpolator(new DecelerateInterpolator()).setListener(new LayerEnablingAnimatorListener(reveal, new LayerEnablingAnimatorListener.Callback() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            enableFab = true;
                        }
                    })).start();
                }
            });







            if (fm.findFragmentByTag(TAG_FRAGMENT) == null) {
                fragment = JourneyFragment.newInstance(origin, destination, date, searchForArrival);
                fm.beginTransaction().add(R.id.cnt_fragment, fragment, TAG_FRAGMENT).commit();
            }

            if(LUtils.isLollopop()){

                appBarLayout.setVisibility(View.GONE);

                new LUtils.CircularRevealBuilder(appBarLayout).withLayer().interpolator(new DecelerateInterpolator()).duration(ANIMATION_DURATION).delay(ANIMATION_DURATION).listener(new LUtils.CircularRevealListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        appBarLayout.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        fragment.showData();
                        fab.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fab.show();
                            }
                        }, 0);
                    }
                }).createAndStart();
            }else{
                fab.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fab.show();
                    }
                }, ANIMATION_DURATION);
                fragment.showData();
            }

            heroId = HERO_IMAGES[new Random().nextInt(HERO_IMAGES.length-1)];
        }else{
            fragment = (JourneyFragment) fm.findFragmentByTag(TAG_FRAGMENT);
            heroId = savedInstanceState.getInt(STATE_HERO_ID);
            fragment.showData();
        }

        imgHero.setImageResource(heroId);


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_HERO_ID, heroId);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.journey, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        reveal.clearAnimation();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        index = i;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_CANCEL:

                //Log.d(TAG,"index="+index);

                if (index == 0) {
                    fragment.setRefreshEnabled(fragment.refreshOnScroll());
                } else {
                    fragment.setRefreshEnabled(false);
                }
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_refresh:
                fragment.reload();
                break;
        }
        return false;
    }
}
