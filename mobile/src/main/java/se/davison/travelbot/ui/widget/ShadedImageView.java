package se.davison.travelbot.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import se.davison.travelbot.R;

/**
 * Created by admin on 2015-09-01.
 */
public class ShadedImageView extends ImageView {
    private float ratio;
    private boolean top;
    private boolean bottom;
    private Paint[] paints;
    private float[] positions;
    private int[] colors;

    public ShadedImageView(Context context) {
        this(context,null);
    }

    public ShadedImageView(Context context, AttributeSet attrs) {
        super(context,attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,
                R.styleable.ShadedImageView,
                0, 0);

        ratio = a.getFloat(R.styleable.ShadedImageView_opacity, 0.6f);

        top = a.getInt(R.styleable.ShadedImageView_location, -1)==0;
        bottom = a.getInt(R.styleable.ShadedImageView_location, -1)==1;


        final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int actionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();


        a.recycle();
        positions = new float[3];
        positions[0] = 0;
        positions[1] = 0.3f;
       //positions[2] = 0.6f;
        positions[2] = 1;

        colors = new int[3];
        colors[0] = toAlpha(Color.BLACK,ratio);
        colors[1] = toAlpha(Color.BLACK,ratio/2);
        //colors[2] = toAlpha(Color.BLACK,ratio/6);
        colors[2] = Color.TRANSPARENT;

    }

    private int toAlpha(int color, float ratio) {
        int factor = (int) (255 * ratio);
        return ( factor << 24 ) | ( color & 0x00ffffff );
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(paints==null){

            List<LinearGradient> gradients = new ArrayList<>(4);
            if(top){
                gradients.add(createGradient(0, (int) (getHeight()*0.7)));
            }
            if(bottom){
                gradients.add(createGradient((int) (getHeight()*0.7),0));
            }
            paints = new Paint[gradients.size()];
            int i = 0;
            for(LinearGradient gradient : gradients){
                paints[i] = new Paint();
                paints[i].setShader(gradient);
                i++;
            }

        }

        for (Paint p :
                paints) {
            canvas.drawPaint(p);
        }
    }

    private LinearGradient createGradient(int from, int to) {
        return new LinearGradient(0,from,0,to,colors,positions, Shader.TileMode.CLAMP);
    }
}
