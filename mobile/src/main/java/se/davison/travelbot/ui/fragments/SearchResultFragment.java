package se.davison.travelbot.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.Subscription;
import se.davison.smartrecycleradapter.AdapterItem;
import se.davison.smartrecycleradapter.OnItemClickListener;
import se.davison.smartrecycleradapter.SectionAdapter;
import se.davison.travelbot.R;
import se.davison.travelbot.adapters.adapteritems.SearchResultStopAdapterItem;
import se.davison.travelbot.adapters.adapteritems.SearchResultHeaderAdapterItem;
import se.davison.travelbot.adapters.adapteritems.SearchResultLocationAdapterItem;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.ui.MainActivity;
import se.davison.travelbot.util.ExtendedSharedPreferences;
import se.davison.travelbot.util.Utils;

/**
 * Created by admin on 2015-08-07.
 */
public class SearchResultFragment extends Fragment {
    private static final String PREF_KEY_RECENT_SEARCHES = "pref_key_recent_searches";
    private ExtendedSharedPreferences prefs;
    private LinkedList<Stop> historyItems;
    private ArrayList<Stop> favorites;
    private SectionAdapter adapter;
    private Gson gson = new Gson();
    private Type listType = new TypeToken<LinkedList<Stop>>(){}.getType();
    private String lastQuery;
    private Observable<List<Stop>> resultObservable;
    private Subscription subscription;
    private String strHistory;
    private String strFavorites;
    private String strMyLocation;
    private String strChooseFromMap;
    private boolean showLocation = true;
    private RecyclerView recyclerView;

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);

        strFavorites = getString(R.string.favorites);
        strHistory = getString(R.string.history);
        strMyLocation = getString(R.string.my_location);
        strChooseFromMap = getString(R.string.choose_from_map);

        prefs = ExtendedSharedPreferences.from(getContext()); //PreferenceManager.getDefaultSharedPreferences(getActivity());



        historyItems = new LinkedList<Stop>(prefs.getList(PREF_KEY_RECENT_SEARCHES, new LinkedList<Stop>(), Stop.class)); //PreferenceHelper.get(prefs, PREF_KEY_RECENT_SEARCHES, new LinkedList<Stop>());

        favorites =  new ArrayList<Stop>(prefs.getList(MainActivity.PREF_KEY_FAVORITE_STOPS, new ArrayList<Stop>(20),Stop.class));
    }



    public static SearchResultFragment newInstance(){
        return new SearchResultFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        Context context = getActivity();

        recyclerView = new RecyclerView(context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        //recyclerView.setPadding(0,0,0,Utils.getNavigationBarHeight(getContext())+Utils.dpToPx(context,8));

        adapter = new SectionAdapter(context);
        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(ViewGroup viewGroup, RecyclerView.ViewHolder viewHolder, View view, int position) {
                AdapterItem item = adapter.getItem(position);
                Stop data = null;
                int resultCode = MainActivity.RESULT_STOP;

                View sharedView = null;
                if (item instanceof SearchResultLocationAdapterItem) {
                    int itemType = ((SearchResultLocationAdapterItem) item).getType();
                    resultCode = itemType == SearchResultLocationAdapterItem.CHOOSE_FROM_MAP ? MainActivity.RESULT_CHOSE_FROM_MAP : MainActivity.RESULT_MY_LOCATION;
                } else if (item instanceof SearchResultStopAdapterItem) {
                    data = ((SearchResultStopAdapterItem) item).getStop();
                    sharedView = ((SearchResultStopAdapterItem.ViewHolder)viewHolder).txtName;
                    historyItems.remove(data);
                    historyItems.addFirst(data);
                    if (historyItems.size() > 10) {
                        historyItems.removeLast();
                    }
                    prefs.edit().putList(PREF_KEY_RECENT_SEARCHES, historyItems).apply();
                }
                if (getActivity() instanceof MainActivity) {

                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.setResult(resultCode, data,sharedView);
                }
            }
        });


        adapter.setSeparator(0x1f000000, Utils.dpToPx(context, 56f),0);
        //recyclerView.setAdapter(adapter);
        adapter.attachTo(recyclerView);
        return recyclerView;
    }





    public void showStartItems() {
        ArrayList<AdapterItem> items = new ArrayList<AdapterItem>(historyItems.size()+20);

        showLocation = ((MainActivity)getActivity()).showInputLocation();

        if(showLocation) {
            items.add(new SearchResultLocationAdapterItem(strMyLocation, SearchResultLocationAdapterItem.MY_LOCATION));
            items.add(new SearchResultLocationAdapterItem(strChooseFromMap, SearchResultLocationAdapterItem.CHOOSE_FROM_MAP));
        }


        if(historyItems.size()>0) {
            items.add(new SearchResultHeaderAdapterItem(strHistory));
            for (Stop stop : historyItems) {
                items.add(new SearchResultStopAdapterItem(stop, SearchResultStopAdapterItem.HISTORY));
            }
        }

        if(favorites.size()>0) {
            items.add(new SearchResultHeaderAdapterItem(strFavorites));
            for (Stop stop : favorites) {
                items.add(new SearchResultStopAdapterItem(stop, SearchResultStopAdapterItem.FAVORITE));
            }
        }
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    public void setLastQuery(String lastQuery) {
        this.lastQuery = lastQuery;
    }

    public void showStops(List<Stop> stops){
        //if(!lastQuery.equals("")) {

            if(stops.size()>0) {

                ArrayList<AdapterItem> items = adapter.getItems();
                items.clear();

                for (Stop stop : stops) {
                    SearchResultStopAdapterItem item = new SearchResultStopAdapterItem(stop, SearchResultStopAdapterItem.STOP);
                    items.add(item);

                }

                adapter.setItems(items);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(0);

            }else{
                //TODO: show empty results
            }
        /*}else{
            showStartItems();
        }*/
    }
}
