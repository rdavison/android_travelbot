package se.davison.travelbot.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * Created by admin on 2015-08-15.
 */
public class Leg extends Depature implements Serializable {


    @SerializedName("Origin")
    private TimedStop origin;

    @SerializedName("Destination")
    private TimedStop destination;

    private long duration;
    private int durationM;
    private int durationH;


    public long getDuration(){
        return duration;
    }

    public int[] getDurationHoursMinutes(){
        return new int[]{durationH,durationM};
    }


    public TimedStop getOrigin() {
        return origin;
    }

    public TimedStop getDestination() {
        return destination;
    }

    public void setOrigin(TimedStop origin) {
        this.origin = origin;
    }

    public void setDestination(TimedStop destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Leg leg = (Leg) o;

        if (!origin.equals(leg.origin)) return false;
        return destination.equals(leg.destination);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + origin.hashCode();
        result = 31 * result + destination.hashCode();
        return result;
    }

    public void setDuration(long duration) {
        this.duration = duration;

        durationM = (int) TimeUnit.MILLISECONDS.toMinutes(duration)%60;
        durationH = (int) TimeUnit.MILLISECONDS.toHours(duration);
    }
}
