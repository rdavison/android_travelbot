package se.davison.travelbot.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import se.davison.travelbot.net.JourneyDetailResponse;
import se.davison.travelbot.util.Utils;

/**
 * Created by richard on 28/02/16.
 */
public abstract class Depature implements Serializable {

    @SerializedName("sname")
    private String line = "";
    @SerializedName("name")
    private String fullName = "";
    private String fullDirection = "";
    private String direction = "";
    private String accessibility;

    @SerializedName("fgColor")
    private String strFgColor;
    @SerializedName("bgColor")
    private String strBgColor;

    private int intFgColor = 0xffffffff;
    private int intBgColor = 0xff000000;
    @SerializedName("type")
    private TransportMode transportMode;
    private String subDirection;


    private String journeyDetailReference;
    private JourneyDetailResponse journeyDetail;

    public String getJourneyDetailReference() {
        return journeyDetailReference;
    }

    public void setJourneyDetailReference(String journeyDetailReference) {
        this.journeyDetailReference = journeyDetailReference;
    }

    public void fix(Date serverTime) {
        this.fullDirection = direction;
        if (direction != null && direction.contains(" via ")) {
            String[] directions = direction.split(" via ");
            this.direction = directions[0];
            this.subDirection = "via " + directions[1];
        }

        intFgColor = Utils.parseColor(strFgColor);
        intBgColor = Utils.parseColor(strBgColor);

        if (transportMode != null && transportMode.equals(TransportMode.WALK)) {
            this.line = "Walk";
            this.fullName = "Walk";
            intBgColor = 0xFF000000;
            intFgColor = 0xFFCCCCCC;
        }
    }

    public boolean isNumeric() {
        try {
            Integer.parseInt(line);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullDirection() {
        return fullDirection;
    }

    public void setFullDirection(String direction) {
        this.fullDirection = direction;
    }

    public int getFgColor() {
        return intFgColor;
    }

    public void setFgColor(int fgColor) {
        this.intFgColor = fgColor;
    }

    public int getBgColor() {
        return intBgColor;
    }

    public void setBgColor(int bgColor) {
        this.intBgColor = bgColor;
    }

    public TransportMode getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(TransportMode transportMode) {
        this.transportMode = transportMode;
    }

    public String getSubDirection() {
        return subDirection;
    }

    public String getDirection() {
        return direction == null ? fullDirection : direction;
    }



    @Override
    public int hashCode() {
        return (line + direction).hashCode();
    }





    public void fix() {
        fix(null);
    }

    public void setJourneyDetail(JourneyDetailResponse journeyDetail) {
        this.journeyDetail = journeyDetail;
    }

    public JourneyDetailResponse getJourneyDetail() {
        return journeyDetail;
    }
}
