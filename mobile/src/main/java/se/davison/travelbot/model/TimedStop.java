package se.davison.travelbot.model;

import android.location.Location;

import java.text.ParseException;
import java.util.Date;

import se.davison.travelbot.util.Constants;

/**
 * Created by richard on 28/02/16.
 */
public class TimedStop extends Stop {

    private String rtTime;
    private String rtDate;
    private String time;
    private String date;
    private Date datetime;
    private Date dateTimeReal;

    public TimedStop(TimedStop stop) {
        super(stop);
        this.rtTime = stop.rtTime;
        this.rtDate = stop.rtDate;
        this.time = stop.time;
        this.date = stop.date;
        this.datetime = stop.datetime;
        this.dateTimeReal = stop.dateTimeReal;
    }



    public Date getDate() {

        if(datetime==null) {
            try {
                datetime = Constants.DATE_FORMAT_FULL.parse(String.format("%s %s", this.date, this.time));
                dateTimeReal = datetime;
                if(rtDate!=null && rtTime!=null) {
                    dateTimeReal = Constants.DATE_FORMAT_FULL.parse(String.format("%s %s", this.rtDate, this.rtTime));
                }
            } catch (ParseException e) {
                datetime = new Date();
                dateTimeReal = datetime;
                e.printStackTrace();
            }
        }
        return datetime;
    }

    public int getTimeDiff(){

        getDate();

        long diff = dateTimeReal.getTime()-datetime.getTime();

        return (int) (diff/1000l);
    }

    public TimedStop(String id, String name, String district, String latitude, String longitude) {
        super(id, name, district, latitude, longitude);
    }

    public TimedStop(int stopType, String name, String district, String latitude, String longitude) {
        super(stopType, name, district, latitude, longitude);
    }

    public TimedStop(int stopType, String name, String district, Location location) {
        super(stopType, name, district, location);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TimedStop timedStop = (TimedStop) o;

        if (!time.equals(timedStop.time)) return false;
        return date.equals(timedStop.date);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}
