package se.davison.travelbot.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by richard on 13/01/15.
 */
public class Departure2 {

    public class Line{
        @SerializedName("BackgroundColor")
        private String backgroundColor;

        @SerializedName("ForegroundColor")
        private String foregroundColor;
    }

}
