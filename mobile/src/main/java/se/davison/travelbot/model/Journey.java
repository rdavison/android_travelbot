package se.davison.travelbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by admin on 2015-08-15.
 */
public class Journey implements Comparable<Journey>, Serializable {

    @SerializedName("Leg")
    private List<Leg> legs;


    private long duration;
    private int durationM;
    private int durationH;

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public void calculate(){
        duration = legs.get(legs.size()-1).getDestination().getDate().getTime()-legs.get(0).getOrigin().getDate().getTime();

        durationM = (int) TimeUnit.MILLISECONDS.toMinutes(duration)%60;
        durationH = (int) TimeUnit.MILLISECONDS.toHours(duration);

        for(Leg leg : legs){
            leg.setDuration(leg.getDestination().getDate().getTime()-leg.getOrigin().getDate().getTime());
        }
    }

    public long getDuration(){
        return duration;
    }

    public int[] getDurationHoursMinutes(){
        return new int[]{durationH,durationM};
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Journey journey = (Journey) o;

        return legs.equals(journey.legs);

    }

    @Override
    public int hashCode() {
        return legs.hashCode();
    }

    @Override
    public int compareTo(Journey another) {

        return getLegs().get(0).getOrigin().getDate().compareTo(another.getLegs().get(0).getOrigin().getDate());
    }
}
