package se.davison.travelbot.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.SparseArray;

import se.davison.travelbot.R;

public enum TransportMode{
	WALK, BUS, TRAM, TRAIN, BOAT, REG, VAS, TAXI, LOC,ST, LDT, UNKNOWN;


	private static final BitmapFactory.Options BITMAP_OPTIONS;
	private static final int[] ICONS =  new int[]{R.drawable.ic_bus, R.drawable.ic_tram,R.drawable.ic_boat,R.drawable.ic_railway, R.drawable.ic_walk};

	static {
		BITMAP_OPTIONS = new BitmapFactory.Options();
		BITMAP_OPTIONS.inPreferredConfig = Bitmap.Config.ARGB_8888;
	}


	public static SparseArray<Bitmap> bitmaps(Context context){
		SparseArray<Bitmap> bitmaps = new SparseArray<>(ICONS.length);

		for (int icon: ICONS) {
			bitmaps.put(icon, BitmapFactory.decodeResource(context.getResources(), icon, BITMAP_OPTIONS));
		}
		return bitmaps;
	}



	public int toResourceId(){
		switch (this) {
			case TRAIN:
			case VAS:
			case REG:
			case LDT:
				return R.drawable.ic_railway;
			case BUS:
			case UNKNOWN:
				return R.drawable.ic_bus;
			case BOAT:
				return R.drawable.ic_boat;
			case TRAM:
				return R.drawable.ic_tram;
			case WALK:
				return R.drawable.ic_walk;
		}

		return R.drawable.ic_bus;
	}


}
