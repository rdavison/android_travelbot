package se.davison.travelbot.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import se.davison.travelbot.util.Constants;

/**
 * Created by richard on 04/11/14.
 */
public class Stop implements Serializable{

    public static final int TYPE_STOP = 0;
    public static final int TYPE_ADDRESS = 1;
    public static final int TYPE_OTHER = 2;


    private String name;
    private String district;
    private String id;
    private String track;


    private int stopType = TYPE_STOP;



    @SerializedName("lat")
    private String latitude;

    @SerializedName("lon")
    private String longitude;


    //for view on map
    private boolean included;

    public Stop(Stop stop) {
        this(stop.id,stop.getName(),stop.district,stop.latitude,stop.longitude);
        this.stopType = stop.stopType;
        this.track = stop.track;
    }


    public Stop(String id, String name, String district, String latitude, String longitude) {
        this.name = name;
        this.district = district;
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Stop(int stopType, String name, String district, String latitude, String longitude) {
        this(null,name,district,latitude,longitude);
        this.stopType = stopType;
    }

    public Stop(int stopType, String name, String district, Location location) {
        this(stopType,name,district,null,null);
        setLocation(location);
    }



    public boolean hasLocation(){
        return latitude!=null&&!latitude.isEmpty()&&longitude!=null&&!longitude.isEmpty();
    }

    public void setLocation(Location location){
        this.latitude = String.valueOf(location.getLatitude());
        this.longitude = String.valueOf(location.getLatitude());
    }

    public LatLng getLocation(){
        return new LatLng(Double.valueOf(latitude),Double.valueOf(longitude));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void fixName(){
        if(name.contains(", ")){
            String[] temp = name.split(", ");
            name = temp[0];
            district = temp[1];
        }
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public String getTrack() {
        return track;
    }

    public void setStopType(int stopType) {
        this.stopType = stopType;
    }

    public int getStopType() {
        return stopType;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stop)) return false;

        Stop stop = (Stop) o;

        if (!name.equals(stop.name)) return false;
        if (!district.equals(stop.district)) return false;
        return id != null ? id.equals(stop.id) : stop.id == null;

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + district.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    public void setIncluded(boolean included) {
        this.included = included;
    }

    public boolean isIncluded() {
        return included;
    }
}
