package se.davison.travelbot.model;

import android.annotation.SuppressLint;
import android.support.v4.util.ArrayMap;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;


import se.davison.travelbot.util.Constants;
import se.davison.travelbot.util.Utils;

public class TimedDeparture extends Depature implements Serializable, Comparable<TimedDeparture> {

    private static final long serialVersionUID = 4196741678862770813L;

    private static final boolean USE_UNTIL_TIME = true;

    private String rtTime;
    private String rtDate;
    private String time;
    private String date;

    private Date untilNext;
    private Date untilThereAfter;
    private Date nextTrip;
    private Date thereAfterTrip;

    private String track;

    private boolean showTrack;



    public void showTrack(boolean showTrack) {
        this.showTrack= showTrack;
    }


    public boolean showTrack() {
        return showTrack;
    }


    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public Date getDate() {
        String parsedDate = this.rtDate;
        if (parsedDate == null) {
            parsedDate = this.date;
        }
        String parsedTime = this.rtTime;
        if (parsedTime == null) {
            parsedTime = this.time;
        }
        Date datetime;
        try {
            datetime = Constants.DATE_FORMAT_FULL.parse(String.format("%s %s", parsedDate, parsedTime));
        } catch (ParseException e) {
            datetime = new Date();
            e.printStackTrace();
        }
        return datetime;
    }

    public static ArrayMap<String,TreeSet<TimedDeparture>> group(List<TimedDeparture> departures){
        ArrayMap<String,TreeSet<TimedDeparture>> map = new ArrayMap<>(20);
        for(TimedDeparture departure : departures){
            String track = departure.getTrack();
            if(!map.containsKey(track)){
                map.put(track,new TreeSet<TimedDeparture>());
            }
            map.get(track).add(departure);
            departure.showTrack(false);
        }
        return map;
    }

    public String getTimeNext(String stringNow) {
        return USE_UNTIL_TIME ? getUntilNextAsString(stringNow) : getNextTripAsString(stringNow);
    }

    public String getTimeThereAfter(String stringNow) {
        return USE_UNTIL_TIME ? getUntilThereAfterAsString(stringNow) : getThereAfterTripAsString(stringNow);
    }

    public boolean timeSet() {
        return !(untilNext == null || untilThereAfter == null);
    }

    @Override
    public int compareTo(TimedDeparture another) {
        return untilNext.compareTo(another.untilNext);
    }

    public Date getUntilNext() {
        return untilNext;
    }

    public String getUntilNextAsString(String stringNow) {
        return (untilNext == null) ? "" : isNow(untilNext) ? stringNow : Utils.millisToString(untilNext.getTime());
    }

    public void setUntilNext(Date untilNext) {
        this.untilNext = untilNext;
    }

    public Date getUntilThereAfter() {
        return untilThereAfter;
    }

    public String getUntilThereAfterAsString(String stringNow) {
        return (untilThereAfter == null) ? "" : isNow(untilThereAfter) ? stringNow : Utils.millisToString(untilThereAfter.getTime());
    }

    private boolean isNow(Date date){
        return date.getTime() < 60000;
    }

    public void setUntilThereAfter(Date untilThereAfter) {
        this.untilThereAfter = untilThereAfter;
    }

    public Date getNextTrip() {
        return nextTrip;
    }

    public String getNextTripAsString(String stringNow) {
        return (nextTrip == null) ? "" : isNow(untilNext) ? stringNow : Constants.DATE_FORMAT_TIME.format(nextTrip);
    }

    public void setNextTrip(Date nextTrip) {
        this.nextTrip = nextTrip;
    }

    public Date getThereAfterTrip() {
        return thereAfterTrip;
    }

    public String getThereAfterTripAsString(String stringNow) {
        return (thereAfterTrip == null) ? "" : isNow(untilThereAfter) ? stringNow : Constants.DATE_FORMAT_TIME.format(thereAfterTrip);
    }

    public void setThereAfterTrip(Date thereAfterTrip) {
        this.thereAfterTrip = thereAfterTrip;
    }

    private void calculateTime(Date serverTime) {
        untilNext = new Date(nextTrip.getTime()+1 - serverTime.getTime());
        if (thereAfterTrip != null) {
            untilThereAfter = new Date(thereAfterTrip.getTime() - serverTime.getTime());
        }
    }

    @Override
    public void fix(Date serverTime) {
        super.fix(serverTime);
        if(serverTime!=null) {
            calculateTime(serverTime);
        }
    }

    public static List<TimedDeparture> merge(List<TimedDeparture> allDepartures) {
        @SuppressLint("UseSparseArrays") //sparse array not needed, just causes an extra loop because we want to return an arrayList
                HashMap<Integer, TimedDeparture> newMap = new HashMap<Integer, TimedDeparture>(allDepartures.size());
        for (TimedDeparture dep : allDepartures) {
            int hash = dep.hashCode();
            if (!newMap.containsKey(hash)) {
                dep.nextTrip = dep.getDate();
                newMap.put(hash, dep);
            } else {
                TimedDeparture first = newMap.get(hash);
                if(first.thereAfterTrip==null) {
                    first.thereAfterTrip = dep.getDate();
                }
            }
        }
        return new ArrayList<TimedDeparture>(newMap.values());
    }
}