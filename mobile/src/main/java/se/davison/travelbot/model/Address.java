package se.davison.travelbot.model;

/**
 * Created by admin on 2015-08-15.
 */
public class Address extends Stop {
    public Address(String name, String district, String latitude, String longitude) {
        super(null, name, district, latitude, longitude);
    }
}
