package se.davison.travelbot.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.opengl.Visibility;
import android.os.Build;
import android.support.annotation.Nullable;
import android.transition.CircularPropagation;
import android.transition.Explode;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by richard on 28/10/15.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class SlideUpDown extends android.transition.Visibility {


    private static final String PROPNAME_SCREEN_BOUNDS = "android:explode:screenBounds";
    private static final String TAG = SlideUpDown.class.getSimpleName();

    private static final int SLIDE_DP = 96;
    private int range = -1;

    public SlideUpDown() {
        setPropagation(new CircularPropagation());
    }

    public SlideUpDown(Context context, AttributeSet attrs) {
        super(context, attrs);
        setPropagation(new CircularPropagation());
    }

    private void captureValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        view.getLocationOnScreen(mTempLoc);
        int left = mTempLoc[0];
        int top = mTempLoc[1];
        int right = left + view.getWidth();
        int bottom = top + view.getHeight();
        transitionValues.values.put(PROPNAME_SCREEN_BOUNDS, new Rect(left, top, right, bottom));
    }

    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        super.captureStartValues(transitionValues);
        captureValues(transitionValues);
    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        super.captureEndValues(transitionValues);
        captureValues(transitionValues);
    }

    private static final TimeInterpolator sDecelerate = new DecelerateInterpolator();
    private static final TimeInterpolator sAccelerate = new AccelerateInterpolator();

    private int[] mTempLoc = new int[2];

    @Override
    public Animator onAppear(ViewGroup sceneRoot, View view,
                             TransitionValues startValues, TransitionValues endValues) {
        if (endValues == null) {
            return null;
        }



        Rect bounds = (Rect) endValues.values.get(PROPNAME_SCREEN_BOUNDS);



        float endY = view.getTranslationY();
        //float startY = endY + ;



        ObjectAnimator anim = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y,
                endY,0);
        return anim;
    }


    @Override
    public Animator onDisappear(ViewGroup sceneRoot, View view,
                                TransitionValues startValues, TransitionValues endValues) {
        if (startValues == null) {
            return null;
        }

        Rect bounds = (Rect) startValues.values.get(PROPNAME_SCREEN_BOUNDS);

        Log.d(TAG,"BOUNDS = "+bounds.toShortString());

        float startY = view.getTranslationY();
        float endY = startY + calculateOut(bounds,sceneRoot,false);
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y,
                startY,endY);
        return anim;
    }


    private int calculateOut(Rect bounds,View sceneRoot,boolean appear) {

        if(range == -1){
            range = (int)sceneRoot.getResources().getDisplayMetrics().density*SLIDE_DP;
        }

        sceneRoot.getLocationOnScreen(mTempLoc);
        int sceneRootY = mTempLoc[1];
        int focalY;
        Rect epicenter = getEpicenter();
        if (epicenter == null) {
            focalY = sceneRootY + (sceneRoot.getHeight() / 2)
                    + Math.round(sceneRoot.getTranslationY());
        } else {
            focalY = epicenter.centerY();
        }
        if(appear){
            focalY += range;
        }
        Log.d(TAG,"focalY = "+focalY);
        Log.d(TAG,"sceenRootY = "+sceneRootY);


        if(bounds.top<(epicenter.top)){
            return -range;
        }else{
            return +range;
        }
    }

}
