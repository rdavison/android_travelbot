package se.davison.travelbot.transition;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.transition.ChangeBounds;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

import se.davison.travelbot.R;

/**
 * Created by richard on 24/10/15.
 */
@SuppressLint("NewApi")
public class TintText extends Transition {


    private static final String PROPNAME_TEXTCOLOR= "android:tint:textcolor";
    private int fromColor;
    private int toColor;

    @Override
    public void captureStartValues(TransitionValues transitionValues) {
        captureValues(transitionValues,"start");
    }

    private void captureValues(TransitionValues transitionValues, String log) {
        View view = transitionValues.view;
        Log.d("asdas",view.getContext().getClass().getSimpleName());
        if (view.getVisibility() == View.GONE) {
            return;
        }
        if(view instanceof TextView){

            TextView textView = (TextView)view;
            transitionValues.values.put(PROPNAME_TEXTCOLOR, textView.getTextColors().getDefaultColor());
        }
    }

    public TintText() {
        init();
    }


    public TintText(Context context, AttributeSet attrs) {

        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TintText);

        /*if(!a.hasValue(R.styleable.TintTextView_fromColor)||a.hasValue(R.styleable.TintTextView_toColor)){
            throw new IllegalArgumentException("missing fromColor and/or toColor attribute");
        }*/



        fromColor = a.getColor(R.styleable.TintText_fromColor, -1);
        toColor = a.getColor(R.styleable.TintText_toColor, -1);
        a.recycle();



        init();
    }

    private void init() {
        addTarget(TextView.class);
    }

    @Override
    public Animator createAnimator(ViewGroup sceneRoot, TransitionValues startValues, TransitionValues endValues) {

        final TextView textView = ((TextView)endValues.view);
        ValueAnimator animator = ValueAnimator.ofObject(new ArgbEvaluator(),
                fromColor, toColor);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Object value = animation.getAnimatedValue();
                // Each time the ValueAnimator produces a new frame in the animation, change
                // the background color of the target. Ensure that the value isn't null.
                if (null != value) {
                    textView.setTextColor((Integer) value);
                }
            }
        });
        return animator;

    }

    @Override
    public void captureEndValues(TransitionValues transitionValues) {
        captureValues(transitionValues,"end");
    }
}
