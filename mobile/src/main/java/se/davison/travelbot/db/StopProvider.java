package se.davison.travelbot.db;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.util.Constants;

/**
 * Created by richard on 05/01/15.
 */
public class StopProvider extends ContentProvider {


    public static final String METHOD_ENSURE_LOADED = "method_ensure_loaded";
    public static String AUTHORITY = "provider";


    private static final String PROVIDER_NAME = "se.davison.provider.Stop";
    private static final String TABLE_NAME = "stops";
    private static final String URL = "content://" + PROVIDER_NAME + "/"+TABLE_NAME;

    public static final Uri CONTENT_URI = Uri.parse(URL);

    private  static final int STOP = 1;
    private static final int STOP_ID = 2;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, TABLE_NAME, STOP);
        uriMatcher.addURI(PROVIDER_NAME, TABLE_NAME+"/#", STOP_ID);
    }

    private static HashMap<String, String> STOP_PROJECTION_MAP;

    private Database database;


    @Override
    public boolean onCreate() {
        database = new Database(getContext().getCacheDir().getAbsolutePath(), Constants.DB_NAME);
        database.ensureLoaded(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(Database.TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case STOP:
                qb.setProjectionMap(STOP_PROJECTION_MAP);
                break;
            case STOP_ID:
                qb.appendWhere( Database._ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == ""){
            sortOrder = Database.NAME;
        }

        if(database.getDatabase()==null){
            throw new IllegalStateException("database is null");
        }

        Cursor c = qb.query(database.getDatabase(),	projection,	selection, selectionArgs,
                null, null, sortOrder);
        /**
         * register to watch a content URI for changes
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            case STOP:
                return "vnd.android.cursor.dir/vnd.travelbot.stop";
            case STOP_ID:
                return "vnd.android.cursor.item/vnd.travelbot.stop";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }

    public static Observable<List<Stop>> getStopResultObservable(final ContentResolver contentResolver, final String filter){
        return Observable.create(new Observable.OnSubscribe<List<Stop>>() {
            @Override
            public void call(Subscriber<? super List<Stop>> subscriber) {
                subscriber.onNext(StopProvider.getStopResult(contentResolver,filter));
                subscriber.onCompleted();
            }
        });
    }


    public static List<Stop> getStopResult(ContentResolver contentResolver, String filter){
        Cursor cursor = getStopCursor(contentResolver,filter);
        List<Stop> result = new ArrayList<Stop>();
        if (cursor!=null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                result.add(new Stop(cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5)));
            }
        }
        cursor.close();
        return result;
    }

    @Override
    public Bundle call(String method, String arg, Bundle extras) {
        if(method.equals(METHOD_ENSURE_LOADED)) {
            database.ensureLoaded(getContext());
        }
        return null;
    }

    public static Cursor getStopCursor(ContentResolver contentResolver, String filter) {
        String whereClause = String.format("LOWER(%s) LIKE '%s%%' AND %s IS NULL", Database.NAME, filter, Database.TRACK);
        return contentResolver.query(CONTENT_URI, new String[]{Database._ID, Database.STOP_ID, Database.NAME, Database.DISTRICT, Database.LATITUDE, Database.LONGITUDE},whereClause,null,Database.WEIGHT + " DESC LIMIT 25");
    }
}
