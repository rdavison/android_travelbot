package se.davison.travelbot.db;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import rx.Observable;
import se.davison.travelbot.model.Stop;
import se.davison.travelbot.util.EventBus;

/**
 * Created by richard on 05/01/15.
 */
public class Database {


    public static final String TABLE_NAME = "Stops";
    public static final String _ID = "_id";
    public static final String STOP_ID = "StopID";
    public static final String NAME = "Name";
    public static final String DISTRICT = "District";
    public static final String LATITUDE = "Lat";
    public static final String LONGITUDE = "Lon";
    public static final String TRACK = "Track";
    public static final String WEIGHT = "Weight";



    private static final String TAG = Database.class.getSimpleName();

    private boolean mLoaded = false;
    private SQLiteDatabase db;

    private String path;
    private String name;
    private boolean loadRunning = false;

    public static class DatabaseEvent{
        public boolean loaded = false;

        public DatabaseEvent(boolean loaded) {
            this.loaded = loaded;
        }
    }

    public Database(String path, String name) {
        this.path = path;
        this.name = name;

    }

    public SQLiteDatabase getDatabase() {
        return db;
    }

    @SuppressWarnings("unchecked")
    private List<Stop> queryResult(String whereQuery) {
        if (whereQuery.equals("")) {
            return Collections.EMPTY_LIST;
        }
        List<Stop> list = new ArrayList<Stop>();
        Cursor cur = db
                .rawQuery("SELECT StopID, Name, District, Lat, Lon FROM Stops WHERE " + whereQuery, new String[]{});
        cur.moveToFirst();
        while (cur.isAfterLast() == false) {
            list.add(new Stop(cur.getString(0), cur.getString(1), cur.getString(2), cur.getString(3), cur.getString(4)));
            cur.moveToNext();
        }
        cur.close();
        return list.isEmpty() ? Collections.EMPTY_LIST : list;
    }

    public List<Stop> getMatches(String query) {
        if (query.length() > 1) {
            query = query.toString().substring(0, 1).toUpperCase()
                    + query.toString().substring(1, query.length()).toLowerCase();
        }
        return queryResult("name LIKE '" + query + "%' AND Track IS NULL ORDER BY Weight DESC LIMIT 25");
    }

    public List<Stop> getAllMatches(String query) {
        if (query.length() > 1) {
            query = query.toString().substring(0, 1).toUpperCase()
                    + query.toString().substring(1, query.length()).toLowerCase();
        }
        return queryResult("name LIKE '" + query + "%' AND Track IS NULL ORDER BY Weight DESC");
    }

    public Stop getStop(String query) {
        return queryResult("StopID = '" + query + "'").get(0);
    }

    public synchronized Cursor getStopsCursor(String filter) {
        if (db == null) {
            return null;
        }
        Cursor cur = db.rawQuery("SELECT _id, StopID, Name, District, Lat, Lon FROM Stops WHERE Name LIKE '"
                + filter + "%' AND Track IS NULL ORDER BY Weight DESC LIMIT 25", new String[]{});
        cur.moveToFirst();

        return cur;
    }

    public double[] getStopCoordinates(Stop stop) {
        double[] coordinates = new double[2];
        Cursor cur = db.rawQuery("SELECT Lon,Lat FROM Stops WHERE StopID = " + stop.getId(),
                new String[]{});
        cur.moveToFirst();
        coordinates[0] = cur.getDouble(1);
        coordinates[1] = cur.getDouble(0);
        cur.close();
        return coordinates;
    }

    public void setStopCoordinates(Stop... stops) {
        String whereQuery = "";
        int i = 0;
        Map<String, Stop> stopMap = new HashMap<String, Stop>(stops.length);
        for (Stop stop : stops) {
            if (i > 0) {
                whereQuery += " OR ";
            }
            whereQuery += "StopID = " + stop.getId();
            i++;
            stopMap.put(stop.getId(), stop);
        }
        Cursor cur = db.rawQuery("SELECT StopID, Lat,Lon FROM Stops WHERE " + whereQuery, new String[]{});
        cur.moveToFirst();
        while (!cur.isAfterLast()) {
            Stop stop = stopMap.get(cur.getString(0));
            if (stop != null) {
                stop.setLatitude(cur.getString(1));
                stop.setLongitude(cur.getString(2));
            }
            cur.moveToNext();
        }
        cur.close();
    }

    public synchronized void ensureLoaded(final Context context) {
        if (mLoaded)
            return;

        if(!loadRunning) {
            loadRunning = true;

            Thread thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(10);
                        openData(context);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, e.toString());
                    }

                    EventBus.getInstance().post(new DatabaseEvent(mLoaded));
                    loadRunning = false;
                }
            });
            thread.setPriority(Thread.NORM_PRIORITY - 1);
            thread.start();
        }
    }

    public void close() {
        if (db != null) {
            db.close();
        }
    }

    public boolean isLoaded() {
        return mLoaded;
    }

    public synchronized void forceReload(Context context) {
        close();
        mLoaded = false;
        ensureLoaded(context);
    }




    private synchronized void openData(Context context) throws Exception {
        if (mLoaded)
            return;

        File dbFile = new File(path + "/" + name);

        if (!dbFile.exists())
            return;

        db = SQLiteDatabase.openDatabase(dbFile.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY);
        if(db==null){
            return;
        }
        Cursor cur = db.rawQuery("PRAGMA quick_check",null);
        cur.moveToFirst();
        if (cur.getCount() > 1 || !cur.getString(0).equals("ok")){
            cur.close();
            return;
        }
        cur.close();
        mLoaded = true;
        return;
    }


}
